<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransportorRootsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transportor_roots', function (Blueprint $table) {
            $table->id();
            $table->string('transportor_id');
            $table->string('ststate');
            $table->string('stdistrict');
            $table->string('edstate');
            $table->string('eddistrict');
            $table->string('freight');
            $table->string('truckload');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transportor_roots');
    }
}
