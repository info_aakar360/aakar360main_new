<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMorefiledsToTransportorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transportor', function (Blueprint $table) {
            $table->string('email')->nullable();
            $table->string('district');
            $table->string('contact_no')->nullable();
            $table->string('contact_designation')->nullable();
            $table->string('landline')->nullable();
            $table->string('refered_by')->nullable();
            $table->integer('verified')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transportor', function (Blueprint $table) {
            $table->dropColumn('email');
            $table->dropColumn('district');
            $table->dropColumn('contact_no');
            $table->dropColumn('contact_designation');
            $table->dropColumn('landline');
            $table->dropColumn('refered_by');
            $table->dropColumn('verified');
        });
    }
}
