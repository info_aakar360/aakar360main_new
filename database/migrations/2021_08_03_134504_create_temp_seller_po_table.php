<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempSellerPoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_seller_po', function (Blueprint $table) {
            $table->id();
            $table->string('order_id');
            $table->string('po_no');
            $table->integer('customer_id');
            $table->text('products');
            $table->string('order_status');
            $table->timestamp('created_date');
            $table->text('order_session_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_seller_po');
    }
}
