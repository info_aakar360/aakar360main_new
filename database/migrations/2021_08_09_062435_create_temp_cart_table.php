<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_cart', function (Blueprint $table) {
            $table->id();
            $table->text('cart')->nullable();
            $table->text('cart_options')->nullable();
            $table->text('cart_variants')->nullable();
            $table->text('cart_session_id')->nullable();
            $table->text('ip_address')->nullable();
            $table->string('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_cart');
    }
}
