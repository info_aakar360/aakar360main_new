<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_orders', function (Blueprint $table) {
            $table->id();
            $table->integer('customer');
            $table->text('name');
            $table->text('email');
            $table->text('city');
            $table->text('address');
            $table->string('mobile');
            $table->text('products');
            $table->string('seller_id');
            $table->string('dealer_id')->nullable();
            $table->decimal('shipping',11,2)->default('0.00');
            $table->decimal('summ',11,2)->default('0.00');
            $table->text('date');
            $table->text('coupon');
            $table->integer('time');
            $table->integer('stat')->default('1');
            $table->text('country');
            $table->text('payment');
            $table->text('payment_id')->nullable();
            $table->text('order_session_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_orders');
    }
}
