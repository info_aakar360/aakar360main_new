<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempMultisellerPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_multiseller_payments', function (Blueprint $table) {
            $table->id();
            $table->integer('seller_id')->nullable();
            $table->string('order_id')->nullable();
            $table->integer('dealer_id')->nullable();
            $table->string('cr_id')->nullable();
            $table->string('dr_id')->nullable();
            $table->string('payment_type')->nullable();
            $table->string('payment_status')->nullable();
            $table->string('payment_method')->nullable();
            $table->text('payment_data')->nullable();
            $table->string('amount')->nullable();
            $table->integer('paid_amount')->nullable();
            $table->integer('balance_amount')->nullable();
            $table->text('description')->nullable();
            $table->text('remark')->nullable();
            $table->text('adjustments')->nullable();
            $table->dateTime('date_created')->nullable();
            $table->text('date_paid')->nullable();
            $table->text('order_session_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_multiseller_payments');
    }
}
