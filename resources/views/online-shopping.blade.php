<?php echo $header?>
    <section class="at-contact-sec" style="background: white;">
        <div class="container">
            <div class="row">
                <section class="at-property-sec at-property-right-sidebar" id="faq">
                    <div class="">
                        <div class="row">
                            <div class="col-md-12">
                                <ul>
                                    <li>
                                        <input type="checkbox" checked>
                                        <i></i>
                                        <h5>Q : Why do I see different prices for the same product?<br><small>
                                        <p>A : We are a market place and give multiple brand/sellers a platform to list their products. This is why you will see different prices for the same product. To see the best price available, click the Buy Now button.</p>
                                    </li>
                                    <li>
                                        <input type="checkbox" checked>
                                        <i></i>
                                        <h5>Q : What does the term pre-order mean?<br><small>
                                        <p>A : At times, we're able to give you the chance to buy a much-wanted product before it is available in the maket. This is when we list the product for pre-order—so you can pre-booked and pay for it in advance. The product will be shipped by the brand/seller/brand soon after the official launch.</p>
                                    </li>
                                    <li>
                                        <input type="checkbox" checked>
                                        <i></i>
                                        <h5>Q : Can I cancel a pre-ordered item?<br><small>
                                        <p>A : You can cancel your order until the seller process it. To cancel your order, visit My Orders in the&nbsp;My Account section. However, please check for specific terms and conditions at the time of pre-ordering.</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </section>
<?php echo $footer?>