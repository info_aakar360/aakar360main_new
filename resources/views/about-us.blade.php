<?php echo $header?>
    <section class="at-contact-sec" style="background: white;">
        <div class="container">
            <div class="row">
                <div class="_2dcm6DxB235FpJeLVGoJPP">
                    At XYZ, everything you see is hand-picked and 100% authentic – sourced straight from the best brands and their authorised resellers from India and across the world, just for you.  <br> <br> We bring you the <strong>newest</strong> – it’s in-season and on-trend; if it’s on the racks, it’s online. And it’s <strong>nowest</strong> – have it delivered QUiQly, from a store near you, when you use our Phygital services.<br>  It’s New and Now <br> A little over a year from launch, XYZ is already in the Top 10, and one of the fastest growing, e-commerce companies in the country. <br> Offering flexibility in the way you shop, XYZ is a first-of-its-kind Phygital marketplace that serves over one million customers. <br>  The journey begins online. Engage with us in a seamless shopping experience across desktop, tablet and smartphone. Browse the best Indian and international brands across clothes and accessories (watches, bags, shoes, jewellery and more), gadgets and home appliances.  <br> Then go offline with us. Tapping into our strong brand partner network, we have created a Phygital experience that combines the ease of shopping online with the reassurance of buying from a brick-and-mortar store. Use our QUiQ PiQ, QUiQ Exchange and QUiQ Finder services to shop from across 2,000 stores and 100 partner brands to pick up or return what you’ve bought, in-store.<br>   Also, engage with our influencers on Que Magazine
                    <link to="" que="">
                    , which is the place for real advice from real people. Get their take on fashion, tech and life. <br> Join the Luxury XYZ <br> XYZ Luxury
                    <link to="" luxury="" hp="">
                    is the country’s first and only online luxury destination. Stocking apparel and accessories - including shoes, bags, belts and watches - from the best international luxury labels, as well as designs from India’s top designers, the luxury experience extends to our Luxe Boxes, which make for indulgent gifting options, no matter what the occasion. <br> We aim to leverage the legacy of the Tata Group, which has set the standard for luxury and sterling service with marquee names such as the Taj Hotels Resorts and Palaces, Tanishq and Jaguar-Land Rover, to create a gateway to high-end fashion, exquisite products and a great experience. We’ll be the first to tell you about new launches and collections and, of course, bring you exclusives. <br>  Get Help <br> From queries on refunds and exchanges, to how to PiQ up your purchases, connect with the crew at XYZ Care
                    <link to="" page="">
                    . One member of our crew will be assigned to help you every step of the way. <br>  All this because we want you to be absolutely, completely and totally happy about what you buy. <br> Be #NewandNow
                </div>
            </div>
        </div>
    </section>
<?php echo $footer?>