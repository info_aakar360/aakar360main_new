<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title><?=$title; ?></title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/animate-css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/chartist-js/chartist.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/chartist-js/chartist-plugin-tooltip.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/dashboard-modern.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/intro.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->

    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">

    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />
    <style>
        .modal-window {
            position: fixed;
            background-color: rgba(200, 200, 200, 0.75);
            top: -70px;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 999;
            opacity: 0;
            pointer-events: none;
            -webkit-transition: all 0.3s;
            -moz-transition: all 0.3s;
            transition: all 0.3s;
        }
        .modal-window:target {
            opacity: 1;
            pointer-events: auto;
        }
        .modal-window>div {
            width: 800px;
            position: relative;
            margin: 10% auto;
            padding: 2rem;
            background: #fff;
            color: #444;
        }
        @media only screen and (max-width: 600px) {
            .modal-window>div {
                width: 340px;
                position: relative;
                margin: 10% auto;
                padding: 2rem;
                background: #fff;
                color: #444;
            }
        }

        .modal-window header {
            font-weight: bold;
        }

        .modal-close {
            color: #aaa;
            line-height: 50px;
            font-size: 80%;
            position: absolute;
            right: 0;
            text-align: center;
            top: 0;
            width: 70px;
            text-decoration: none;
        }

        .modal-close:hover {
            color: #000;
        }

        .modal-window h1 {
            font-size: 150%;
            margin: 0 0 15px;
        }
        table th, td{
            border:1px solid #d8d3d8;
            padding-left: 15px;
        }
        .tableFixHead {
            overflow: auto;
            height: 560px;
            width:100%
        }
    </style>
    <!-- END: Custom CSS-->
</head>

<!-- END: Head-->
<?=$header;?>
<div class="row">
    <div class="col s12">
        <div class="container">
            <div class="section">
                <div class="card">
                    <div class="card-content">
                        <h4 class="card-title" style="color: #0d1baa; margin-left: 5px">Campaign SMS</h4>
                        <form action="sms" method="post">
                            <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">

                            <div class="col s12 m6">
                                <div class="col s1 m1"style="text-align: center;margin-top: 30px;margin-left: -32px;"> <i class="material-icons prefix">flag</i></div>
                                <div class="col s11 m11">
                                    Select State<label><input id="state_check_all" name="state_check_all_name"  type="checkbox">
                                        <span class="state_all" style="text-decoration: none;font-size: 12px;margin-left: 23px;padding-left: 20px;">Select All</span></label>
                                    <select class="state_list browser-default" name="state[]"  multiple style="width: 100%" required>
                                        <?php
                                        $sid_val = explode(',',$sid);
                                        foreach ($states as $state){
                                        $s_id = $state->id;
                                        ?>
                                        <option  <?php if (in_array($s_id, $sid_val)) {
                                            echo "selected";
                                        } else {
                                            echo '';
                                        }?> value="<?=$s_id;?>"><?=$state->name?></option>
                                        <?php
                                        } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col s12 m6">
                                <div class="col s1 m1" style="text-align: center;margin-top: 30px;margin-left: -32px;"> <i class="material-icons prefix">adjust</i></div>
                                <div class="col s11 m11">
                                    Select District<label><input id="district_check_all" name="district_check_all_name" type="checkbox">
                                        <span class="district_all" style="text-decoration: none;font-size: 12px;margin-left: 13px;padding-left: 20px;">Select All</span></label>

                                    <select class="district_list browser-default" name="district[]" multiple style="width: 100%" >
                                        <?php $did_val = explode(',',$did); $s='';
                                        if($did==''){
                                            $s = 'selected';
                                        }
                                        foreach ($district as $dis){
                                        $dis_id = $dis->id;
                                        ?>
                                        <option <?php if (in_array($dis_id, $did_val)) {
                                            echo "selected";
                                        } else {
                                            echo '';
                                        } echo $s ?> value="<?php echo $dis_id;?>"><?=$dis->name?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class=" col s12 m12 btn-margin-sms">
                                <center> <button class="btn" type="submit" name="find_district">FIND</button> </center>
                            </div>
                            <div class="col s12 m6">
                                <div class="col s1 m1" style="text-align: center; margin-top: 30px;margin-left: -32px;"> <i class="material-icons prefix">location_on</i></div>
                                <div class="col s11 m11">
                                    Select Locality
                                    <select class="locality_list browser-default" name="locality[]" multiple style="width: 100%" >
                                        <?php $lid_val = explode(',',$lid); $s='';

                                        foreach ($locality as $local){
                                        $local_id = $local->id;
                                        ?>
                                        <option <?php if (in_array($local_id, $lid_val)) {
                                            echo "selected";
                                        } else {
                                            echo '';
                                        } echo $s ?> value="<?php echo $local_id;?>"><?=$local->locality?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class=" col s12 m6">
                                <div class="col s1 m1" style="text-align: center; margin-top: 30px;margin-left: -32px;">
                                    <i class="material-icons prefix">account_circle</i>
                                </div>
                                <div class="col s11 m11" style="margin-top: 4px">
                                    Account Manager
                                    <select class=" browser-default acc_manager" multiple name="acc_manager[]"  style="width: 100%" >
                                        <?php $acc_val = explode(',',$acc_manager); $s='';
                                        if($acc_manager==''){
                                            $s = 'selected';
                                        }
                                        foreach ($users as $user){
                                        if(in_array($user->id, $acc_val)){
                                            $s = 'selected';
                                        }else{
                                            $s = '';
                                        }
                                        ?>
                                        <option <?=$s?> value="<?=$user->id?>"><?=$user->name?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class=" col s12 m6">
                                <div class="col s1 m1" style="text-align: center;  margin-top: 20px;margin-left: -32px;"> <i class="material-icons prefix">list</i></div>
                                <div class="col s11 m11">
                                    Category
                                    <select class=" browser-default cust_category" multiple name="category[]"  style="width: 100%" >
                                        <?php
                                        $cid_val = explode(',',$category_set); $s='';
                                        if($did==''){
                                            $s = 'selected';
                                        }
                                        foreach ($custcategory as $category){
                                        $cat_id = $category->id;
                                        ?>
                                        <option <?php if (in_array($cat_id, $cid_val)) {
                                            echo "selected";
                                        } else {
                                            echo '';
                                        } ?> value="<?=$category->id?>"><?=$category->type?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col s12 m6">
                                <div class="col s1 m1" style="text-align: center;margin-top: 30px;margin-left: -32px;"> <i class="material-icons prefix">dns</i></div>
                                <div class="col s11 m11">
                                    Select Price Group
                                    <select class="pgroup_list browser-default" name="price_group[]" multiple style="width: 100%" >
                                        <option value="all">All</option>
                                        <?php
                                        foreach($cat_groups as $grp){
                                        ?>
                                        <option value="<?=$grp->id?>"><?=$grp->name.' - '.$grp->group_name?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col s12 m4">
                                <div class="col s1 m1" style="text-align: center; margin-top: 30px; margin-left: -32px"> <i class="material-icons prefix">dns</i></div>
                                <div class="col s11 m11">
                                    Type
                                    <select class=" browser-default ctype "  name="type[]" multiple  style="width: 100%">
                                        <?php  $c_type_val = explode(',',$type_set); $s='';
                                        if($did==''){
                                            $s = 'selected';
                                        }
                                        foreach ($custtype as $type){
                                        $c_type = $type->id;
                                        ?>
                                        <option <?php if (in_array($c_type, $c_type_val)) {
                                            echo "selected";
                                        } else {
                                            echo '';
                                        } ?>  value="<?=$type->id?>"><?=$type->type?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col s12 m4">
                                <div class="col s1 m1" style="text-align: center;margin-top: 30px;margin-left: -32px;"> <i class="material-icons prefix">dns</i></div>
                                <div class="col s11 m11">
                                    Project Type
                                    <select name="ptype" id="ptype" class="ptype"> type
                                        <option value="" disabled selected></option>
                                        <option value="1">Private</option>
                                        <option value="2">Government</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col s12 m4">
                                <div class="col s1 m1" style="text-align: center;margin-top: 30px;margin-left: -32px;"> <i class="material-icons prefix">dns</i></div>
                                <div class="col s11 m11">
                                    Project Sub Type
                                    <select class="browser-default pstype" name="pstype[]" id="pstype" multiple tabindex="-1" style="width: 100% !important;" > type
                                    </select>
                                </div>
                            </div>


                            <div class="col s12 m6">
                                <div class="col s1 m1" style="text-align: center; margin-top: 30px;margin-left: -32px;"> <i class="material-icons prefix">dehaze</i></div>
                                <div class="col s11 m11">
                                    Class
                                    <select class="browser-default class" multiple name="class[]" style="width: 100%">
                                        <?php  $c_class_val = explode(',',$class_set); $s='';
                                        if($did==''){
                                            $s = 'selected';
                                        }
                                        foreach ($customer_class as $class){
                                        $cl_id= $class->id;
                                        ?>
                                        <option <?php if (in_array($cl_id, $c_class_val)) {
                                            echo "selected";
                                        } else {
                                            echo '';
                                        } ?>  value="<?=$class->id?>"><?=$class->type?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col s12 m6">
                                <div class="col s1 m1" style="text-align: center;margin-top: 30px;margin-left: -32px;"> <i class="material-icons prefix">transfer_within_a_station</i></div>
                                <div class="col s11 m11">
                                    Visited
                                    <select class="visited browser-default" name="visited" style="width: 100%" >
                                        <option <?=($visited == 'all') ? 'selected' : '' ?> value="all">All</option>
                                        <option <?=($visited == 'yes') ? 'selected' : '' ?> value="yes">Yes</option>
                                        <option <?=($visited == 'no') ? 'selected' : '' ?> value="no">No</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col s12 m12 btn-margin-sms">
                                <center><button class="btn" type="submit" name="find">FIND</button></center>
                            </div>

                        </form>

                        <form method="post">
                            <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                            <input type="hidden" name="temp_id" id="temp_id" value="">

                            <div class="col s12 m11"><i class="material-icons prefix" style=" margin-left: -13px; margin-top: 26px;">group</i>
                                <div class="col s12 m12" style="margin-top: -51px;">
                                    Select Customers<label><input id="customer_check_all" name="customer_check_all_name"  type="checkbox">
                                        <span class="customer_all" style="text-decoration: none;margin-left: 15px;font-size: 12px;padding-left: 20px;">Select All</span>
                                    </label>
                                    <select class="customer_list browser-default" name="cust[]" id="contid" multiple style="width: 100%" required>
                                        <?php foreach ($customers as $customer){
                                        ?>
                                        <option selected value="<?php echo trim($customer->contact_no); if(!empty($customer->sec_contact)){ echo ',',trim($customer->sec_contact); } ?>"><?=$customer->name?> (  <?php echo  $add = $customer->postal_address; ?>) </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col s12 m11">
                                Select Template
                                @if(\Illuminate\Support\Facades\Session::get('smssendfrom') == 'smshop')
                                    <select class="customer_list browser-default" name="template" id="template"  style="width: 100%" required>
                                        <option value="">Select Template</option>
                                        <option value="1207162141012065937">RAIGARH TMT AVAILABLE WITH US AT {#var#} PLZ CONTACT : {#var#} SM SHOP RAIPUR</option>
                                        <option value="1207162141027211499">{#var#} RAIPUR ISI TMT BASIC- {#var#} EXPLANT TAXPAID RATE 8&32MM = {#var#} 10MM TO 25MM = {#var#} CONTACT- {#var#} SM SHOP RAIPUR</option>
                                        <option value="1207162141019945751">RDSO APPROVED TMT AVAILABLE WITH US {#var#} PLEASE CONTACT {#var#} SM SHOP RAIPUR</option>
                                        <option value="1207162140994149310">WE DEAL IN:- TMT, ANGLE, CHANNEL, BEAM, PIPE, FLAT, SQUARE, ROUND, PLATE, SHEET, B. WIRE, GI WIRE CONTACT :- {#var#} SM SHOP RAIPUR</option>
                                        <option value="1207162141001939638">UNDER WEIGHT TMT AVAILABLE WITH US AT {#var#} PLZ CONTACT : {#var#} SM SHOP RAIPUR</option>
                                        <option value="1707161727877347843">SM SHOP {#var#} RANDOM TMT {#var#} SHORT FOLD {#var#} SINGLE LENGTH {#var#} AVAILABLE @ SPECIAL DISCOUNT PLZ CONTACT {#var#}</option>
                                        <option value="1707161656790532270">Dear S M SHOP User, {#var#} is the OTP Generated for Your Transaction.</option>
                                    </select>
                                @else
                                    <select class="customer_list browser-default" name="template" id="template"  style="width: 100%" required>
                                        <option value="">Select Template</option>
                                        <option value="1707162194839163822">{#var#} Your Aakar360 OTP is {#var#} {#var#}</option>
                                        <option value="1707161684573368493">Your Aakar360 Project OTP is {#var#} {#var#}</option>
                                    </select>
                                @endif
                            </div>
                            <div class="col s12 m1 input-field col s1 btn-margin-sms">
                                Total : <span id="tot_cust"></span>
                            </div>

                            <div class="input-field col s12 m11">
                                <i class="material-icons prefix" style="margin-left: -16px; margin-top: 22px;">mail</i>
                                <div class="col s12 m12">
                                    <label for="ccomment" style="text-decoration: none;color: #9e9e9e;font-size: 12px;">Your Message</label>
                                    <textarea id="msg_input" name="msg" style="font-size: small" class="materialize-textarea " aria-invalid="msgid"></textarea>
                                </div>
                            </div>
                            <div class="input-field col s12 m1 ">
                                Text : [<span id="tot_msg_show"></span>]  msg : [<span id="tot_msg_count">1</span>]
                            </div>

                            <div class="input-field col s12 m11">
                                <i class="material-icons prefix" style="margin-top: 5px;margin-left: -17px;">contact_phone</i>
                                <div class="col s12 m12">
                                    <input type="text" name="new_no" class="new_no"<?php $nos = array(); $secnos = array(); if(!empty($customers)){foreach ($customers as $customer) { $nos[] = $customer->contact_no;
                                        if(!empty($customer->sec_contact)){
                                            $secnos[] = $customer->sec_contact;
                                        }
                                    }}?>
                                    value="<?php  print_r(implode(',',$nos)); if(!empty($secnos)){ echo ','; print_r(implode(',',$secnos));}?>"  placeholder="Enter Number">
                                </div>
                            </div>

                            <div class="input-field btn-margin-sms col s12 m12" style="margin-top: 12px;margin-bottom: 160px">
                                <center><a href="#open-modal1" class="btn myblue" >Send Sms From</a>
                                    <button class="btn" type="button" name="msg_send">Send
                                        <i class="material-icons top">send</i>
                                    </button></center>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="open-modal1" class="modal-window">
    <div>
        <a href="#modal-close" title="Close" class="modal-close">close &times;</a>
        <h1>SMS From</h1>
        <div>
            <table border="0">
                <form action="" method="post">
                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                    <tr>
                        <td>
                            <input type="radio" id="aakar360" name="smsfrom" value="aakar360" style="opacity: inherit;">
                        </td>
                        <td>
                            <label for="aakar360">Aakar360</label><br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="radio" id="crm" name="smsfrom" value="smshop"  style="opacity: inherit;">
                        </td>
                        <td>
                            <label for="crm">Sm Shop</label>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button type="submit" id="smsfromsave" name="smsfromsave" class="btn myblue">Save</td>
                    </tr>
                </form>
            </table>
        </div>
    </div>
</div>
<!-- END: Footer-->

<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<!--<script src="<?/*=$tp; */?>/js/scripts/form-elements.js"></script>-->
<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
</body>

</html>
<script>
    $(document).ready(function(){
        $('.state_list').select2();
        $('.district_list').select2();
        $('.pgroup_list').select2();
        $('.locality_list').select2();
        $('.customer_list').select2();
        $('.cust_category').select2();
        $('.acc_manager').select2();
        $('.ctype').select2();
        $('.pstype').select2();
        $('.class').select2();
        $('.visited').select2();

        var count = $("#contid :selected").length;
        $('#tot_cust').html(count);

        $('#msg_input').keyup(function() {
            var tot_msg_1 = this.value.length;
            var tot_msg = (parseInt(tot_msg_1)-1)/140;
            $('#tot_msg_show').html(parseInt(tot_msg_1));
            $('#tot_msg_count').html(parseInt(tot_msg)+1);
        });
        $('#contid').change(function () {
            var count = $("#contid :selected").length;
            $('#tot_cust').html(count);
        });

        $('#state_check_all').click(function () {
            var isChecked = $("#state_check_all").is(":checked");
            if (isChecked) {
                $(".state_list option").attr("selected", true);

            } else {
                $(".state_list option").attr("selected", false);
            }
        });
        $('#district_check_all').click(function () {
            var isChecked = $("#district_check_all").is(":checked");
            if (isChecked) {
                $(".district_list option").attr("selected", true);

            } else {
                $(".district_list option").attr('selected', false);
            }
        });
        $('#customer_check_all').click(function () {

            var isChecked = $("#customer_check_all").is(":checked");
            if (isChecked) {
                $(".customer_list option").attr("selected", true);
                var count = $("#contid :selected").length;
                $('#tot_cust').html(count);

            } else {
                $(".customer_list option").attr('selected', false);
                var count = $("#contid :selected").length;
                $('#tot_cust').html(count);
            }
        });

        $('#template').change(function () {
            var id = $(this).val();
            var text = $("#template option:selected").text();
            $("#temp_id").val(id);
        });

        $('button[name=msg_send]').click(function () {
            $('button[name=msg_send]').html('Wait...');
            var mob = $("#contid").val();
            var cname = encodeURIComponent($("#msg_input").val());
            var temp_id = $("#temp_id").val();
            var mob2 = $(".new_no").val();

            if(mob==''){
                mob = mob2;
            }
            else{
                if(mob2==''){
                    mob = mob;
                }else{
                    mob = mob +','+ mob2;
                }
            }
            mob = mob.toString().replace(/\s/g,'');
            var url = "https://merasandesh.com/api/sendsms";
            $.ajax({
                type: "POST",
                url: url,
                @if(\Illuminate\Support\Facades\Session::get('smssendfrom') == 'smshop')
                data:{username:'smshop',password:'Smshop@123',senderid:'SMSHOP',message:cname,numbers:mob, unicode:0,template_id:temp_id},
                @else
                data:{username:'Aakar_360',password:'Ak@12345',senderid:'AKAARR',message:cname,numbers:mob, unicode:0,template_id:temp_id},
                @endif
                crossOrigin:true,
                beforeSend: function(){
                    $('button[name=msg_send]').html('Wait...');
                    $.ajax({
                        type: "POST",
                        url: "msg_save",
                        data:{message:cname, numbers:mob,template_id:temp_id, _token:'<?=csrf_token();?>'},
                        success: function(data){
                        }
                    });
                },
                success: function(data){
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                },
                complete: function(data){
                    $('button[name=msg_send]').html('Send');
                    alert('SMS sent succesfully !');
                    location.reload();
                }
            });



        });

        $('.new_no').blur(function () {
            var a = $(this).val();
            var b = $(this).val().length;
            var c = a.indexOf(",");
            if(a==''){
                $('.send_msg').prop("disabled", false);
            }
            else{
                if(c==-1){
                    if(b==10){
                        $('.send_msg').prop("disabled", false);
                    }
                    else{
                        alert("Please Enter 10 digit no");
                        $('.send_msg').prop("disabled", true);
                    }
                }else{
                    var str = a;
                    var str_array = str.split(',');
                    for(var i = 0; i < str_array.length; i++) {
                        str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
                        var tot = str_array[i].length;
                        if(tot!=10){
                            alert("Please Enter 10 digit number before and after comma");
                            $('.send_msg').prop("disabled", true);
                        }
                        else{
                            $('.send_msg').prop("disabled", false);
                        }
                    }
                }
            }

        });

        $("input[name=new_no]").keypress(function (e) {

            if (/\d+|,/i.test(e.key) ){

                console.log("character accepted: " + e.key)
            } else {
                console.log("illegal character detected: "+ e.key)
                return false;
            }

        });
        $('.ptype').change(function () {
            var val = $(this).val();

            $.ajax({
                type: "POST",
                url: "get-subtype",
                data:'tid='+val+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $('.pstype').html(data);
                }
            });
        });

    });

</script>