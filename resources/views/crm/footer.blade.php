<script>
    function showSwitchTab(){
        $('.modal_p1').css('display', 'block');
    }
</script>
<div class="modal_p1">
    <div class="modal_p1_body">
        <form action="" method="post">
            <?=csrf_field()?>
            <input type="hidden" id="crm_type" name="crm_type" value=""/>
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Choose Crm Type?</h5>
                </div>
                <div class="modal-body">
                    <div class="row" style="display:flex">
                        <div class="col-md-4" style="margin: 0 5px;width: 100%">
                            <form method="post" action="{{url('crm/index')}}" style="height: 160%; !important;">
                                @csrf
                                <button type="submit" name="submitCookie"  class="card">
                                    <input type="hidden" name="crm_type" value="1">
                                    <div class="column">
                                        <img src="../assets/images/background/business.jpg" alt="Business" class="imagefit">
                                    </div>
                                    <div class="column1">
                                        <h5 style="margin-top: 30%;margin-left: 5%;">Business</h5>
                                    </div>
                                </button>
                            </form>
                        </div>
                        <div class="col-md-4" style="margin: 0 5px;width: 100%">
                            <form method="post" action="{{url('crm/index')}}">
                                @csrf
                                <button type="submit" name="submitCookie" class="card" style="height: 160%; !important;">
                                    <input type="hidden" name="crm_type" value="2">
                                    <div class="column">
                                        <img src="../assets/images/background/retail.jpg" alt="Retail" class="imagefit" >
                                    </div>
                                    <div class="column1">
                                        <h5 style="margin-top: 30%;margin-left: 5%;">Retail</h5>
                                    </div>
                                </button>
                            </form>
                        </div>
                        <div class="col-md-4" style="margin: 0 5px;width: 100%">
                            <form method="post" action="{{url('crm/index')}}">
                                @csrf
                                <button type="submit" name="submitCookie"  class="card" value="3" style="height: 160%; !important;">
                                    <input type="hidden" name="crm_type" value="3">
                                    <div class="column">
                                        <img src="../assets/images/background/software.jpg" alt="Software" class="imagefit">
                                    </div>
                                    <div class="column1">
                                        <h5 style="margin-top: 30%;margin-left: 5%;">Software</h5>
                                    </div>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
{{--<footer class="page-footer footer footer-static footer-dark gradient-45deg-indigo-purple gradient-shadow navbar-border navbar-shadow">
    <div class="footer-copyright">
        <div class="container"><span>&copy; 2019          <a href="" target="_blank">Adroweb</a> All rights reserved.</span><span class="right hide-on-small-only">Design and Developed by <a href="">Adroweb</a></span></div>
    </div>
</footer>--}}

