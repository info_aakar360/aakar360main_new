<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="ThemeSelect">
    <title><?=$title; ?></title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?=$tp; ?>/vendors/noUiSlider/nouislider.min.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">

    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />



    <!-- END: Custom CSS-->
</head>
<!-- END: Head-->
<?=$header;?>
<div class="row">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" >
                <h5>Edit Truck Load</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="{{route('truckload.index')}}" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
                <form method="post" action="{{route('truckload.update',[$truckload->id])}}" class="col s12 m12">
                    {{ method_field('PUT') }}
                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                    <div class="row">
                        <div class="input-field col s12 m12">
                            <i class="material-icons prefix">list</i>
                            <input id="load" type="text" class="validate" name="load" value="{{$truckload->load}}">
                            <label for="load">Load</label>
                        </div>

                        <div class="col s12 m12 " style="text-align:center;margin-top: 20px;margin-bottom: 20px;" >
                            <button class="btn myblue waves-light add_submit"  style="padding:0 5px;" type="submit" name="add">SAVE
                                <i class="material-icons right">save</i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- END: Footer-->
<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>

<script src="<?=$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script>
<?php echo $footer ?>
</body>
</html>