<?php echo $header?>
<div class="b-hero1">
    <figure style="background-image: url('assets/design/<?=$cat->image; ?>')" class="b-post__hero-image">
        <div data-w-id="403de785-9462-5110-ab3f-b13328bf2c04" class="b-post__hero-inner">
            <div class="b-container cc-read cc-hero">
                <div class="b-read__wrap cc-hero">
                    <h1 class="b-post__title"> <?=translate($cat->name) ?></h1>
                    <p class="b-post__subhead"><?=$cat->description;?></p>

                </div>

            </div>

        </div>

    </figure>

</div>
<div class="row" style="text-align: center;">
    <div class="col-xs-12 col-sm-offset-1 col-sm-10 col-lg-offset-2 col-lg-8" style="margin-top: -105px; z-index: 1">
        <div class="header-bottom">
            <div class="header-navigation">
                <ul class="navigation navigation--horizontal">
                    <li class="navigation-item navigation-architecture">
                        <?php $totvisit = 0;
                        $btotal = DB::select("SELECT * FROM design_category WHERE id = '".$cat->id."'");
                        foreach($btotal as $btv){
                            $pla = DB::select("SELECT * FROM layout_plans WHERE category = '".$btv->id."'");
                            foreach ($pla as $pl) {
                                $pla = DB::select("SELECT COUNT(*) as count FROM layout_reviews WHERE active = 1 AND review <> '' AND layout = '".$pl->id."'")[0]->count;
                                $totvisit = $totvisit + $pla;
                            }
                        }
                        ?>
                        <a href="javascript:void(0)" title="Total Visits"><span><?=$totvisit; ?> Total Review</span><i class="fa fa-pencil-square-o"> <?=$totvisit; ?></i> </a>
                    </li>
                    <li class="navigation-item navigation-art">
                        <?php $totvisits = 0;
                        foreach($btotal as $btvs){
                            $plas = DB::select("SELECT * FROM layout_plans WHERE category = '".$btvs->id."'");
                            foreach ($plas as $pls) {
                                $totvisits = $totvisits + $pls->visits;
                            }
                        }
                        ?>
                        <a href="javascript:void(0)" title="Liked"><span><?=$totvisits; ?> Total Visits</span><i class="fa fa-eye"> <?=$totvisits; ?></i></a>
                    </li>
                    <li class="navigation-item navigation-art">
                        <?php
                        $totli = 0;
                        foreach ($btotal as $btl){
                            $plal = DB::select("SELECT * FROM layout_plans WHERE category = '".$btl->id."'");
                            foreach ($plal as $pll) {
                                $li = DB::select("SELECT COUNT(*) as count FROM plan_like WHERE plan_id = " . $pll->id)[0]->count;
                                $totli = $totli + $li;
                            }
                        }
                        ?>

                        <a href="javascript:void(0)" title="Liked"><span><?=$totli; ?> Total Likes</span><i class="fa fa-thumbs-up"> <?=$totli; ?></i></a>
                    </li>
                </ul>
            </div>
        </div>


    </div>

</div>
<section class="pdtopbtm-50">
    <div class="container">
        <div class="row">
            <button class="btn btn-default filter-button"><i class="fa fa-filter"></i> Filter</button>
            <aside class="sidebar-shop col-md-3 order-md-first">
                <div class="filter-div-close"></div>
                <div class="sidebar-wrapper at-categories" style="padding: 10px;">
                    <div class="widget">
                        <h3 class="widget-title">
                            <a data-toggle="collapse" href="#widget-body-c" role="button" aria-expanded="true" aria-controls="widget-body-c" class="cat_accord">Categories</a>
                        </h3>
                        <div class="show collapse" id="widget-body-c">
                            <div class="widget-body" style="height: 250px; overflow-y: auto;">
                                <ul class="cat-list">
                                    <?php foreach($cats as $cat){
                                        echo '<li><a href="./'.$cat->layout.'/'.$cat->slug.'">'.translate($cat->name).'</a></li>';
                                        $childs = DB::select("SELECT * FROM design_category WHERE parent = ".$cat->id." ORDER BY id DESC");
                                        foreach ($childs as $child){
                                            echo '<li><a href="./'.$child->layout.'/'.$child->slug.'">- '.$child->name.'</a></li>';
                                        }
                                    }
                                    ?>
                                </ul>
                            </div><!-- End .widget-body -->
                        </div><!-- End .collapse -->
                    </div><!-- End .widget -->
                </div>
            </aside>
            <div class="filter-wrapper"></div>
            <div class="col-md-9 col-lg-9">
                <div class="grid">
                    <?php foreach($layout_plans as $layout){  ?>
                        <div class="grid-item">
                            <div class="tiles1">
                                <a href="<?php echo url('/layout_plan/'.$layout->slug); ?>">
                                    <img src="<?php echo url('/assets/layout_plans/'.image_order($layout->images)); ?>"/>
                                    <div class="icon-holder">
                                        <?php $amm = json_decode($layout->interior_ammenities);
                                        $i = 1;
                                        //dd($amm);
                                        foreach($amm as $key=>$value){
                                            $a = DB::select("SELECT * FROM amminities WHERE id = ".$key)[0];
                                            //dd($a);
                                            ?>
                                            <?php if($i <= 3){
                                                if($i%3 == 0){ ?>
                                                    <div class="icons mrgntop borderRight0"><b><?=$a->name.' : '.$value; ?></b></div>
                                                <?php }else{ ?>
                                                    <div class="icons mrgntop"><b><?=$a->name.' : '.$value; ?></b></div>
                                                <?php }
                                            }else{
                                                if($i%3 == 0){ ?>
                                                    <div class="icons mrgnbot borderRight0"><b><?=$a->name .' : '.$value; ?></b></div>
                                                <?php }else{ ?>
                                                    <div class="icons mrgnbot"><b><?=$a->name .' : '.$value; ?></b></div>
                                                <?php } } ?>
                                            <?php $i++; } ?>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php if(!empty($link)){?>
                    <div class="">
                        <div class="blockquote blockquote--style-1">
                            <div class="row inner-div">
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <img src="<?=url('/assets/products/'.image_order($link->image))?>" style="width:75%;height:auto;"/>
                                    </div>
                                    <div class="col-md-7" style="padding-top: 25px;">
                                        <h3><?=$link->content; ?></h3>
                                    </div>
                                    <div class="col-md-2" style="padding:25px 0 0 0">
                                        <a class="btn btn-primary" href="<?=$link->link; ?>" type="submit" style="padding: 10px auto !important;">
                                            GET STARTED
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
<!-- Inner page heading end -->


<!-- Services Popup End -->
<?php echo $footer?>

<script>
    jQuery(window).on('load', function(){
        var $ = jQuery;
        var $container = $('.grid');
        $container.masonry({
            columnWidth:10,
            gutterWidth: 15,
            itemSelector: '.grid-item'
        });
    });
    $(document).ready(function(){
        $(document).on('click', '.filter-button', function(){
            $('aside.sidebar-shop').addClass('open');
        });
        $(document).on('click', '.filter-div-close', function(){
            $('aside.sidebar-shop').removeClass('open');
        });
    });
</script>