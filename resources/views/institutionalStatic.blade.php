<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Title-->
    <title>Aakar360 Mentors Pvt. Ltd.</title>
    <!-- Favicon-->
    <link rel="icon" href="img/core-img/favicon.ico">
    <!-- Core Stylesheet-->
    <link rel="stylesheet" href="{{ url('businessstatic/style.css') }}">
</head>
<body>
<!-- Preloader-->
<!-- Header Area-->
<header class="header-area header2" style="background-color: #fff !important;">
    <div class="container">
        <div class="classy-nav-container breakpoint-off">
            <nav class="classy-navbar navbar2 justify-content-between" id="saasboxNav">
                <!-- Logo--><a class="nav-brand me-5" href="#"><img src="{{ url('businessstatic/img/logo.png') }}" alt="" style="height: 26px !important;"></a>
                <!-- Navbar Toggler-->
                <div class="classy-navbar-toggler"><span class="navbarToggler"><span></span><span></span><span></span><span></span></span></div>
                <!-- Menu-->
                <div class="classy-menu">
                    <!-- close btn-->
                    <div class="classycloseIcon">
                        <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                    </div>
                    <!-- Nav Start-->
                    <div class="classynav">
                        {{--<ul id="corenav">--}}
                            {{--<li><a href="#">Home</a></li>--}}
                            {{--<li><a href="#">About</a></li>--}}
                            {{--<li><a href="#">Contact</a></li>--}}
                        {{--</ul>--}}
                        <!-- Login Button-->
                        <div class="login-btn-area ms-4 mt-4 mt-lg-0"><a class="btn saasbox-btn btn-sm" href="{{ route('business.institutionalInstitutionalLogin') }}">Login/Register</a></div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</header>
<!-- Welcome Area-->
<section class="welcome-area hero2 bg-img bg-overlay-banner" id="home" style="background-image: url('{{ url('businessstatic/img/testimonial.jpg') }}') !important;background-repeat: no-repeat;background-size: 100%;height: 625px !important;">
    <!-- Background Shape
    <div class="hero-background-shape"><img src="img/core-img/hero-2.png" alt=""></div>
    <!-- Background Animation
    <div class="background-animation">
      <div class="star-ani"></div>
      <div class="circle-ani"></div>
      <div class="box-ani"></div>
    </div>
    <!-- Hero Circle
    <div class="hero2-big-circle"></div>-->
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-between">
            <div class="col-12 col-md-12">
                <!-- Content-->
                <div class="welcome-content pe-3" style="text-align: center;">
                    <h2 class="wow fadeInUp" data-wow-delay="100ms" data-wow-duration="1000ms">Build powerful responsive fastest website using it.</h2>
                    <!-- Button Group-->
                    <!--div class="btn-group-one wow fadeInUp" data-wow-delay="500ms" data-wow-duration="1000ms"><a class="btn saasbox-btn white-btn mt-3" href="#">More About Us</a><a class="btn saasbox-btn-2 mt-3 ms-3 ms-sm-4 video-play-btn" href="https://www.youtube.com/watch?v=lFGvqvPh5jI" data-effect="mfp-zoom-in"><i class="me-1 lni-play"></i>Play Video</a></div-->
                </div>
            </div>
            <!--div class="col-10 col-md-6">
              <div class="welcome-thumb wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1000ms"><img src="img/bg-img/hero-6.png" alt=""></div>
            </div-->
        </div>
    </div>
</section>
<div class="container">
    <div class="border-dashed"></div>
</div>
<!-- About Area-->
<section class="about-area about2">
    <div class="container">
        <div class="row align-items-center justify-content-md-center justify-content-lg-between">
            <!-- About Thumbnail Area-->
            <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                <div class="aboutUs-thumbnail mt-100 wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1000ms"><img class="w-85" src="{{ url('businessstatic/img/about.jpg') }}" alt=""></div>
            </div>
            <!-- About Us Content Area-->
            <div class="col-12 col-md-8 col-lg-6">
                <div class="aboutUs-content mt-120 wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1000ms">
                    <div class="section-heading mb-5">
                        <h2>About Us</h2>
                        <!--h2>We focus on the growth of your business.</h2-->
                        <p>We are a growing team, joining together from different industries for a common purpose to transform construction through technology and innovation. We provide you wide range of construction material across the construction life cycle at unbeatable price point by aggregating demand at a large scale.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container" style="padding-top: 60px;">
        <!-- About Content Area-->
        <div class="about-content">
            <div class="row justify-content-center">
                <div class="col-12 col-md-9 col-lg-7 col-xxl-6">
                    <div class="section-heading text-center">
                        <h2>Statistics</h2>
                        <!--p>It's crafted with the latest trend of design &amp; coded with all modern approaches. It's a robust &amp; multi-dimensional usable template.</p-->
                    </div>
                </div>
            </div>
            <div class="row">
                <!--div class="container">
                    <div class="row">
                      <div class="col-12 col-sm-3">
                        <div class="single-cool-fact mb-50 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="100ms">
                          <h2>+<span class="rs-counter"> 800</span></h2>
                          <p>Client's</p>
                        </div>
                      </div>
                      <div class="col-12 col-sm-3">
                        <div class="single-cool-fact mb-50 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
                          <h2>+<span class="rs-counter"> 500</span></h2>
                          <p>Project's</p>
                        </div>
                      </div>
                      <div class="col-12 col-sm-3">
                        <div class="single-cool-fact mb-50 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="500ms">
                          <h2>+<span class="rs-counter"> 25</span></h2>
                          <p>Manufecturer's</p>
                        </div>
                      </div>
                      <div class="col-12 col-sm-3">
                        <div class="single-cool-fact mb-50 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="500ms">
                          <h2>+<span class="rs-counter"> 1000</span> cr</h2>
                          <p>Material Supplied</p>
                        </div>
                      </div>
                    </div>
                  </div-->
                <!-- Single About Area-->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-about-area text-center wow fadeInUp" data-wow-delay="100ms" data-wow-duration="1000ms">
                        <div class="icon mx-auto"><i class="lni-sun"></i></div>
                        <h4>+800 Client's</h4>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-about-area text-center wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1000ms">
                        <div class="icon mx-auto"><i class="lni-heart"></i></div>
                        <h4>+500 Project's</h4>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-about-area text-center wow fadeInUp" data-wow-delay="500ms" data-wow-duration="1000ms">
                        <div class="icon mx-auto"><i class="lni-infinite"></i></div>
                        <h4>+25 Manufecturer's</h4>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-about-area text-center wow fadeInUp" data-wow-delay="700ms" data-wow-duration="1000ms">
                        <div class="icon mx-auto"><i class="lni-cart"></i></div>
                        <h4>+1000 cr. Material Supplied</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<!-- Service Area-->
<section class="service-area section-padding-120" id="service">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-9 col-lg-7 col-xxl-6">
                <div class="section-heading text-center">
                    <h2>Sector <span>We  </span> Serve</h2>
                    <!--p>It's crafted with the latest trend of design &amp; coded with all modern approaches. It's a robust &amp; multi-dimensional usable template.</p-->
                </div>
            </div>
        </div>
        <div class="row justify-content-center g-5">
            <!-- Single Service Area-->
            <div class="col-12 col-sm-8 col-md-6 col-lg-5 col-xl-4">
                <div class="card service-card wow fadeInUp" data-wow-delay="100ms" data-wow-duration="1000ms">
                    <div class="card-body">
                        <div class="icon"><i class="lni-android"></i></div>
                        <h4>Real Estate</h4>
                        <p>It's crafted with the latest trend of design.</p>
                    </div>
                </div>
            </div>
            <!-- Single Service Area-->
            <div class="col-12 col-sm-8 col-md-6 col-lg-5 col-xl-4">
                <div class="card service-card active wow fadeInUp" data-wow-delay="200ms" data-wow-duration="1000ms">
                    <div class="card-body">
                        <div class="icon"><i class="lni-layout"></i></div>
                        <h4>Infrastructure</h4>
                        <p>It's crafted with the latest trend of design.</p>
                    </div>
                </div>
            </div>
            <!-- Single Service Area-->
            <div class="col-12 col-sm-8 col-md-6 col-lg-5 col-xl-4">
                <div class="card service-card wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1000ms">
                    <div class="card-body">
                        <div class="icon"><i class="lni-pie-chart"></i></div>
                        <h4>Industries</h4>
                        <p>It's crafted with the latest trend of design.</p>
                    </div>
                </div>
            </div>
            <!-- Single Service Area
            <div class="col-12 col-sm-8 col-md-6 col-lg-5 col-xl-4">
              <div class="card service-card wow fadeInUp" data-wow-delay="400ms" data-wow-duration="1000ms">
                <div class="card-body">
                  <div class="icon"><i class="lni-wordpress"></i></div>
                  <h4>WordPress 5.0 Ultimate Solution</h4>
                  <p>It's crafted with the latest trend of design.</p>
                </div>
              </div>
            </div>
            <div class="col-12 col-sm-8 col-md-6 col-lg-5 col-xl-4">
              <div class="card service-card wow fadeInUp" data-wow-delay="500ms" data-wow-duration="1000ms">
                <div class="card-body">
                  <div class="icon"><i class="lni-sun"></i></div>
                  <h4>Business Idea &amp; Creative Leads</h4>
                  <p>It's crafted with the latest trend of design.</p>
                </div>
              </div>
            </div>
            <div class="col-12 col-sm-8 col-md-6 col-lg-5 col-xl-4">
              <div class="card service-card wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1000ms">
                <div class="card-body">
                  <div class="icon"><i class="lni-play"></i></div>
                  <h4>Tutorial: Learning for future</h4>
                  <p>It's crafted with the latest trend of design.</p>
                </div>
              </div>
            </div-->
        </div>
    </div>
</section>
<!-- Features Area-->
<section class="saasbox-features-area feature2 section-padding-120-90">
    <!-- Background Shape-->
    <div class="background-shape wow fadeInLeftBig" data-wow-duration="4000ms"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-9 col-lg-7 col-xxl-6">
                <div class="section-heading text-center">
                    <h2>Why <span>Choose </span> Us</h2>
                    <!--p>It's crafted with the latest trend of design & coded with all modern approaches. It's a robust & multi-dimensional usable template.</p-->
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-12 col-lg-8 col-xxl-9">
                <div class="row g-4">
                    <!-- Single Feature Area-->
                    <div class="col-12 col-md-6">
                        <div class="card feature-card wow fadeInUp" data-wow-delay="100ms" data-wow-duration="1000ms">
                            <div class="card-body d-flex align-items-center"><i class="lni-wordpress"></i>
                                <div class="fea-text">
                                    <h6>WordPress Solution</h6><span>Ultimate Solution for WP</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Single Feature Area-->
                    <div class="col-12 col-md-6">
                        <div class="card feature-card wow fadeInUp" data-wow-delay="200ms" data-wow-duration="1000ms">
                            <div class="card-body d-flex align-items-center"><i class="lni-brush"></i>
                                <div class="fea-text">
                                    <h6>Frontend Solution</h6><span>Solution for Webs</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Single Feature Area           -->
                    <div class="col-12 col-md-6">
                        <div class="card feature-card wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1000ms">
                            <div class="card-body d-flex align-items-center"><i class="lni-bar-chart"></i>
                                <div class="fea-text">
                                    <h6>Digital Branding</h6><span>Boot your sales</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Single Feature Area           -->
                    <div class="col-12 col-md-6">
                        <div class="card feature-card wow fadeInUp" data-wow-delay="400ms" data-wow-duration="1000ms">
                            <div class="card-body d-flex align-items-center"><i class="lni-wechat"></i>
                                <div class="fea-text">
                                    <h6>Live Chat Help</h6><span>Support 24h a day</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Single Feature Area        -->
                    <div class="col-12 col-md-6">
                        <div class="card feature-card wow fadeInUp" data-wow-delay="500ms" data-wow-duration="1000ms">
                            <div class="card-body d-flex align-items-center"><i class="lni-cog"></i>
                                <div class="fea-text">
                                    <h6>Easy Setup</h6><span>Solution for setup</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Single Feature Area  -->
                    <div class="col-12 col-md-6">
                        <div class="card feature-card wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1000ms">
                            <div class="card-body d-flex align-items-center"><i class="lni-bug"></i>
                                <div class="fea-text">
                                    <h6>Fixed Bugs</h6><span>Unlimited bug fix</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-10 col-md-7 col-lg-4 col-xxl-3">
                <div class="card video-card video2 border-0 mt-5 mt-lg-0 wow fadeInUp" data-wow-delay="700ms" data-wow-duration="1000ms">
                    <div class="card-body p-0"><img src="{{ url('businessstatic/img/bg-img/6.jpg') }}" alt=""><a class="video-play-btn" href="#"><i class="lni-play"></i><span class="video-sonar"></span></a></div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- Portfolio Area-->
<section class="saasbox-portfolio-area section-padding-120">
    <div class="container">
        <div class="row align-items-end justify-content-between">
            <div class="col-12 col-md-8 col-lg-7 col-xxl-6">
                <div class="section-heading mb-0">
                    <h2>Our Products</h2>
                    <p>It's crafted with the latest trend of design & coded with all modern approaches. It's a robust & multi-dimensional usable template.</p>
                </div>
            </div>
            <!--div class="col-12 col-md-4 col-lg-5">
              <div class="portfolio-btn pb-3 text-md-end mt-5 mt-md-0"><a class="btn saasbox-btn" href="portfolio-creative.html">View All Works</a></div>
            </div-->
        </div>
    </div>
    <div class="container px-0">
        <div class="portfolio-slides-2 owl-carousel">
            <!-- Single Portfolio Area-->
            <div class="single-portfolio-area mx-3 mt-70 mb-80 no-boxshadow"><img src="{{ url('businessstatic/img/bg-img/p5.jpg') }}" alt="">
                <!-- Ovarlay Content-->
                <div class="overlay-content">
                    <div class="portfolio-title"><a href="#">Portfolio Title</a></div>
                    <div class="portfolio-links"><a class="image-popup" href="{{ url('businessstatic/img/bg-img/p5.jpg') }}" data-effect="mfp-zoom-in" title="Portfolio Title"><i class="lni-play"></i></a><a href="#"><i class="lni-heart"></i></a><a href="portfolio-details-one.html"><i class="lni-link"></i></a></div>
                </div>
            </div>
            <!-- Single Portfolio Area-->
            <div class="single-portfolio-area mx-3 mt-70 mb-80 no-boxshadow"><img src="{{ url('businessstatic/img/bg-img/p6.jpg') }}" alt="">
                <!-- Ovarlay Content-->
                <div class="overlay-content bg-gray">
                    <div class="portfolio-title"><a href="#">Portfolio Title</a></div>
                    <div class="portfolio-links"><a class="image-popup" href="{{ url('businessstatic/img/bg-img/p6.jpg') }}" data-effect="mfp-zoom-in" title="Portfolio Title"><i class="lni-play"></i></a><a href="#"><i class="lni-heart"></i></a><a href="portfolio-details-one.html"><i class="lni-link"></i></a></div>
                </div>
            </div>
            <!-- Single Portfolio Area-->
            <div class="single-portfolio-area mx-3 mt-70 mb-80 no-boxshadow"><img src="{{ url('businessstatic/img/bg-img/p7.jpg') }}" alt="">
                <!-- Ovarlay Content-->
                <div class="overlay-content bg-gray">
                    <div class="portfolio-title"><a href="#">Portfolio Title</a></div>
                    <div class="portfolio-links"><a class="image-popup" href="{{ url('businessstatic/img/bg-img/p7.jpg') }}" data-effect="mfp-zoom-in" title="Portfolio Title"><i class="lni-play"></i></a><a href="#"><i class="lni-heart"></i></a><a href="portfolio-details-one.html"><i class="lni-link"></i></a></div>
                </div>
            </div>
            <!-- Single Portfolio Area-->
            <div class="single-portfolio-area mx-3 mt-70 mb-80 no-boxshadow"><img src="{{ url('businessstatic/img/bg-img/p8.jpg') }}" alt="">
                <!-- Ovarlay Content-->
                <div class="overlay-content bg-gray">
                    <div class="portfolio-title"><a href="#">Portfolio Title</a></div>
                    <div class="portfolio-links"><a class="image-popup" href="{{ url('businessstatic/img/bg-img/p8.jpg') }}" data-effect="mfp-zoom-in" title="Portfolio Title"><i class="lni-play"></i></a><a href="#"><i class="lni-heart"></i></a><a href="portfolio-details-one.html"><i class="lni-link"></i></a></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Client Feedback Area-->
<section class="client-feedback-area d-md-flex align-items-center justify-content-between section-padding-120 bg-img bg-overlay jarallax" style="background-image: url('businessstatic/img/ban3.jpg');">
    <!-- Client Feedback Heading-->
    <div class="client-feedback-heading">
        <div class="section-heading mb-0 text-end white">
            <h6>Testimonials</h6>
            <!--h2 class="mb-0">Our Customers Feedback</h2-->
        </div>
    </div>
    <!-- Client Feedback Content-->
    <div class="client-feedback-content">
        <div class="client-feedback-slides owl-carousel">
            <!-- Single Feedback Slide-->
            <div class="card feedback-card bg-white">
                <div class="card-body"><i class="lni-quotation"></i>
                    <p>You've saved our business! Thanks guys, keep up the good work! The best on the net!</p>
                    <div class="client-info d-flex align-items-center">
                        <div class="client-thumb"><img src="{{ url('businessstatic/img/bg-img/t1.png') }}" alt=""></div>
                        <div class="client-name">
                            <h6>Lim Jannat</h6>
                            <p>UX/UI Designer</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Single Feedback Slide-->
            <div class="card feedback-card bg-white">
                <div class="card-body"><i class="lni-quotation"></i>
                    <p>I STRONGLY recommend agency to EVERYONE interested in running a successful business!</p>
                    <div class="client-info d-flex align-items-center">
                        <div class="client-thumb"><img src="{{ url('businessstatic/img/bg-img/t2.png') }}" alt=""></div>
                        <div class="client-name">
                            <h6>Pryce R.</h6>
                            <p>CEO</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Single Feedback Slide-->
            <div class="card feedback-card bg-white">
                <div class="card-body"><i class="lni-quotation"></i>
                    <p>Absolutely wonderful! I wish I would have thought of it first. I would be lost without agency.</p>
                    <div class="client-info d-flex align-items-center">
                        <div class="client-thumb"><img src="{{ url('businessstatic/img/bg-img/t3.png') }}" alt=""></div>
                        <div class="client-name">
                            <h6>Cy N.</h6>
                            <p>Developer</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Single Feedback Slide-->
            <div class="card feedback-card bg-white">
                <div class="card-body"><i class="lni-quotation"></i>
                    <p>I STRONGLY recommend agency to EVERYONE interested in running a successful business!</p>
                    <div class="client-info d-flex align-items-center">
                        <div class="client-thumb"><img src="{{ url('businessstatic/img/bg-img/t4.png') }}" alt=""></div>
                        <div class="client-name">
                            <h6>Juergen T.</h6>
                            <p>Business Owner</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- News Area-->
<section class="saasbox-news-area news2 section-padding-120">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-9 col-lg-7 col-xxl-6">
                <div class="section-heading text-center">
                    <h2>Latest News</h2>
                    <!--p>It's crafted with the latest trend of design & coded with all modern approaches. It's a robust & multi-dimensional usable template.</p-->
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center g-5">
            <!-- Blog Card-->
            <div class="col-12 col-sm-8 col-md-6 col-lg-4">
                <div class="card blog-card border-0 wow fadeInUp" data-wow-delay="200ms" data-wow-duration="1000ms"><a class="d-block mb-4" href="#"><img src="{{ url('businessstatic/img/bg-img/blog4.jpg') }}" alt=""></a>
                    <div class="post-content"><a class="d-block mb-1" href="#">News</a><a class="post-title d-block mb-3" href="#">
                            <h4>Seven ways agency can improve your business.</h4></a>
                        <div class="post-meta"><span class="text-muted"><i class="lni-timer me-2"></i>2 min read</span></div>
                    </div>
                </div>
            </div>
            <!-- Blog Card    -->
            <div class="col-12 col-sm-8 col-md-6 col-lg-4">
                <div class="card blog-card border-0 wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1000ms">
                    <div class="image-wrap mb-4"><img src="{{ url('businessstatic/img/bg-img/blog5.jpg') }}" alt="">
                        <!-- Video--><span class="video-content"><a class="video-play-btn" href="#"><i class="lni-play"></i></a></span>
                    </div>
                    <div class="post-content"><a class="d-block mb-1" href="#">Agency</a><a class="post-title d-block mb-3" href="#">
                            <h4>The reason why everyone love business.</h4></a>
                        <div class="post-meta"><span class="text-muted"><i class="lni-timer me-2"></i>4 min read</span></div>
                    </div>
                </div>
            </div>
            <!-- Blog Card-->
            <div class="col-12 col-sm-8 col-md-6 col-lg-4">
                <div class="card blog-card border-0 wow fadeInUp" data-wow-delay="800ms" data-wow-duration="1000ms"><a class="d-block mb-4" href="#"><img src="businessstatic/img/bg-img/blog6.jpg" alt=""></a>
                    <div class="post-content"><a class="d-block mb-1" href="#">Trend</a><a class="post-title d-block mb-3" href="#">
                            <h4>Seven ways agency can improve your business.</h4></a>
                        <div class="post-meta"><span class="text-muted"><i class="lni-timer me-2"></i>3 min read</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Partner Area
<div class="our-partner-area section-padding-0-120 partner2">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="our-partner-slides owl-carousel">
          <div class="single-partner"><img src="img/partner-img/1.png" alt=""></div>
          <div class="single-partner"><img src="img/partner-img/2.png" alt=""></div>
          <div class="single-partner"><img src="img/partner-img/3.png" alt=""></div>
          <div class="single-partner"><img src="img/partner-img/4.png" alt=""></div>
          <div class="single-partner"><img src="img/partner-img/5.png" alt=""></div>
          <div class="single-partner"><img src="img/partner-img/6.png" alt=""></div>
          <div class="single-partner"><img src="img/partner-img/1.png" alt=""></div>
          <div class="single-partner"><img src="img/partner-img/2.png" alt=""></div>
          <div class="single-partner"><img src="img/partner-img/3.png" alt=""></div>
          <div class="single-partner"><img src="img/partner-img/4.png" alt=""></div>
          <div class="single-partner"><img src="img/partner-img/5.png" alt=""></div>
          <div class="single-partner"><img src="img/partner-img/6.png" alt=""></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Cookie Alert Area-->
<!-- Footer Area-->
<footer class="footer-area footer2 section-padding-20">

    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-md-6 col-lg-6">
                <!-- Copywrite Text-->
                <div class="footer--content-text">
                    <p class="mb-0">Aakar360 Mentors Pvt. Ltd.</p>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-6">
                <!-- Footer Nav-->
                <div class="footer-nav">
                    <ul class="d-flex">
                        <li><a href="#" target="_blank">Privacy Policy</a></li>
                        <li><a href="#" target="_blank">Terms &amp; Conditions</a></li>
                        <li><a href="#" target="_blank">Get Support</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- All JavaScript Files-->
<script src="{{ url('businessstatic/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ url('businessstatic/js/jquery.min.js') }}"></script>
<script src="{{ url('businessstatic/js/default/classy-nav.min.js') }}"></script>
<script src="{{ url('businessstatic/js/waypoints.min.js') }}"></script>
<script src="{{ url('businessstatic/js/jquery.easing.min.js') }}"></script>
<script src="{{ url('businessstatic/js/default/jquery.scrollup.min.js') }}"></script>
<script src="{{ url('businessstatic/js/owl.carousel.min.js') }}"></script>
<script src="{{ url('businessstatic/js/imagesloaded.pkgd.min.js') }}"></script>
<script src="{{ url('businessstatic/js/default/isotope.pkgd.min.js') }}"></script>
<script src="{{ url('businessstatic/js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ url('businessstatic/js/jquery.animatedheadline.min.js') }}"></script>
<script src="{{ url('businessstatic/js/jquery.counterup.min.js') }}"></script>
<script src="{{ url('businessstatic/js/wow.min.js') }}"></script>
<script src="{{ url('businessstatic/js/jarallax.min.js') }}"></script>
<script src="{{ url('businessstatic/js/jarallax-video.min.js') }}"></script>
<script src="{{ url('businessstatic/js/default/cookiealert.js') }}"></script>
<script src="{{ url('businessstatic/js/default/jquery.passwordstrength.js') }}"></script>
<script src="{{ url('businessstatic/js/default/mail.js') }}"></script>
<script src="{{ url('businessstatic/js/default/active.js') }}"></script>
</body>
</html>