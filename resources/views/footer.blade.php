<section class="at-product-category" id="footer2" style="margin: 0; padding: 50px 0;border:none;background: #292929;">
	<div class="container">
		<div class="row">
            <div class="col-md-12">
                <div class="col-md-4">
                    <h4>HELP</h4>
                    <a href="{{route('retail.retailShipping')}}">Shipping Policy</a>
                    <a href="{{route('retail.retailCancellation')}}">Cancellation Policy</a>
                    <a href="{{route('retail.retailReturn')}}">Return Policy</a>
                    <a href="{{route('retail.retailRefund')}}">Refund Policy</a>
                    <a href="{{route('retail.retailFaqs')}}">FAQ</a>
                    <a href="{{route('retail.retailReportInfringement')}}">Report Infringement</a>
                    <a href="{{route('retail.retailTermsUse')}}">Terms of Use</a>
                    <a href="{{route('retail.retailTermsConditions')}}">Terms and Conditions</a>
                    <a href="{{route('retail.retailPrivacy')}}">Privacy Policy</a>
                </div>
                <div class="col-md-4">
                    <h4>Aakar360</h4>
                    <a href="{{route('retail.retailContactUs')}}">Contact Us</a>
                    <a href="{{route('retail.retailAboutUs')}}">About Us</a>
                    <a href="{{route('retail.retailCareers')}}">Careers</a>
                    <a href="{{route('retail.retailStories')}}">Aakar360 Stories</a>
                    <a href="{{route('retail.retailPress')}}">Press</a>
                    <a href="{{route('retail.retailSell')}}">Sell on Aakar360</a>
                    <a href="register/dealer">Become a Dealer</a>
                </div>
                <div class="col-md-4">
                    <h4>MISC</h4>
                    <a href="{{route('retail.retailOnlineShopping')}}">Online Shopping</a>
                    <a href="{{route('retail.retailAffiliateProgram')}}">Affiliate Program</a>
                    <a href="{{route('retail.retailGiftCard')}}">Gift Card</a>
                    <a href="{{route('retail.retailSubscription')}}">Aakar360 First Subscription</a>
                    <a href="{{route('retail.retailSitemap')}}">Sitemap</a>
                </div>
            </div>
		</div>
	</div>
</section>

<section class="at-product-category" id="footer3" style="margin: 0; padding: 50px 0;border:none;background: #191919;">
    <div class="container">
		<div class="row">
			<?php
			$pcats = false;														
			$pcats = $data['p_cat'];
			if($pcats){
				foreach($pcats as $pcat){ ?>
			        <div class="col-md-12">
				        <span class="text-uppercase"><?php echo $pcat->name; ?> : </span>
				        <?php $pscats = false;
					    $pscats = getProductCategories($pcat->id);
                        if($pscats){
                            foreach($pscats as $pscat){ ?>
                                <a href="/products?catlink=<?=$pscat->id;?>"><?=$pscat->name;?></a> |
                            <?php }
                        } ?>
			        </div>
			    <?php }
			} ?>
			<?php
			$scats = false;														
			$scats = $data['s_cat'];
			if($scats){
				foreach($scats as $scat){ ?>
                    <div class="col-md-12">
                        <span class="text-uppercase"><?php echo $scat->name; ?> : </span>
                        <?php
                        $sscats = false;
                        $sscats = getProductCategories($scat->id);
                        if($sscats){
                            foreach($sscats as $sscat){  ?>
                                <a href="/sub_service/<?php echo path($sscat->name, $sscat->id); ?>"><?=$sscat->name;?></a> |
                            <?php }
                        } ?>
                    </div>
			    <?php }
			} ?>
		</div>
	</div>
</section>

<!-- Copyright start from here -->
<section class="at-copyright">
    <div class="row">
        <p>Copyright ©2018 All Rights Reserved || Made with <i class="fa fa-heart" style="color: red;"></i> By <a href="https://www.adroweb.com" target="_blank">Adroweb</a> </p>
    </div>
</section>
    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- all plugins and JavaScript -->
    <script type="text/javascript" src="assets/js/css3-animate-it.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap-dropdownhover.min.js"></script>
    <script type="text/javascript" src="assets/js/featherlight.min.js"></script>
    <script type="text/javascript" src="assets/js/featherlight.gallery.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.flexslider.js"></script>
    <script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="assets/js/jarallax.js"></script>
    <script type="text/javascript" src="assets/js/jquery-ui.js"></script>
    <script type="text/javascript" src="assets/js/jquery-scrolltofixed-min.js"></script>
    <script type="text/javascript" src="assets/js/morphext.min.js"></script>
    <script type="text/javascript" src="assets/js/dyscrollup.js"></script>
<script>
    $(document).ready(function() {
        $('#datatable-editable').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        } );
    } );

    $(document).ready(function() {
        $('#datatable-editable1').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        } );
    } );
</script>
<!-- Main Custom JS -->
<script src="themes/default/assets/main.js"></script>
<script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script>
<!-- the jScrollPane script -->
<script type="text/javascript" src="assets/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="assets/js/jquery.contentcarousel.js"></script>
<script src="assets/js/slick.js"></script>
<script type="text/javascript" src="assets/js/main.js"></script>
<script src="assets/js/select2.js"></script>
<script src="assets/js/cbpFWTabs.js"></script>
<script src="assets/js/aos.js"></script>
<script src="themes/default/assets/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.bootstrap-dropdown-hover.js"></script>
<script src="assets/js/carouFredSel-6.2.1.js" type="text/javascript"></script>
<script src="assets/js/getwidthbrowser.js" type="text/javascript"></script>
<script src="assets/js/cs.bossthemes.js" type="text/javascript"></script>
<script>
    //$('[data-toggle="dropdown"]').bootstrapDropdownHover();
    $.fn.bootstrapDropdownHover();
    //$('#dropdownMenu1').bootstrapDropdownHover();
    //$('.navbar [data-toggle="dropdown"]').bootstrapDropdownHover();
</script>

<script>
    function submitRfr(){
        $('.error').hide();
        var variants = 0;
        if($('.variantsx')){
            if($('.variantsx').val()){
                variants = 1;
            }
        }else{
            variants = -1;
        }
        if(variants == 0){
            $('.variantsx').parent().find('.error').html('Variant is mandatory').show();
        }
        else if($('textarea[name=msg]').val() == ''){
            $('textarea[name=msg]').parent().find('.error').html('Enquiry/Message is mandatory.').show();
        }
        else {
            $('form#rfrForm').submit();
        }
    }
    $( function() {
        $( "#datepicker" ).datepicker();
    } );
    $( function() {
        $( "#datepicker1" ).datepicker();
    } );
	$(document).ready(function(){
        $('.select2').select2({
            width: '100%'
        });
	$("a.animation").on('click', function(event) {
		// Make sure this.hash has a value before overriding default behavior
		if (this.hash !== "") {
		  // Prevent default anchor click behavior
		  event.preventDefault();
		  // Store hash
		  var hash = this.hash;
		  // Using jQuery's animate() method to add smooth page scroll
		  // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
		  $('html, body').animate({
			scrollTop: $(hash).offset().top - 80
		  }, 800, function(){
			  setTimeout(function(){
				$('.anim1').addClass('fadeIn');
				setTimeout(function(){ $('.anim1').removeClass('fadeIn'); }, 3000);
			  }, 500);
		  });
		} // End if
	  });
	});
    function getRegModal(){
        event.preventDefault();
        $('#open-register').click();
    }
    $(document).on('click', '.request-rate', function(){
        $('#rfrModal').modal('show');
    });
</script>
<button class="btn btn-primary hidden" id="open-register" data-popup-open="popup-register">Get Started !</button>
<div class="popup" id="popup-register" data-popup="popup-register">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-lg-12 account" style="border: none; box-shadow: none;">
            <div class="row" style="margin-top: 40px;">
                <a href="{{route('retail.retailRegister',['individual'])}}">
                    <div class="col-sm-3 anim1 animated" data-duration="2000">
                        <div class="icon-box boxed-style align-center animated" data-animation="zoomOut" data-delay="600" style="width: 230px; height: 250px; box-shadow: 0 0 20px #a5a5a5; background-color: white;">
                            <div class="animated-icon colored" style="width: 100px;">
                                <img src="<?=url('assets/images/individual.png'); ?>" style="width: 70px;height: auto;"/>
                            </div>
                            <div class="ib-content">
                                <h5>Individual Buyer</h5>
                                <p>Starting your own Project?</p>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="{{route('business.institutionalRegister',['institutional'])}}">
                    <div class="col-sm-3 anim1 animated" data-duration="2000">
                        <div class="icon-box boxed-style align-center animated" data-animation="zoomOut" data-delay="300" style="width: 230px; height: 250px; box-shadow: 0 0 20px #a5a5a5; background-color: white;">
                            <div class="animated-icon colored" style="width: 100px;">
                                <img src="<?=url('assets/images/institutional.png'); ?>" style="width: 70px;height: auto;"/>
                            </div>
                            <div class="ib-content">
                                <h5 style="width: 200px; margin-left: -20px;">Institutional Buyer</h5>
                                <p>Require Material in Bulk Quantity?</p>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="{{route('retail.retailRegister',['professional'])}}">
                    <div class="col-sm-3 anim1 animated" data-duration="2000">
                        <div class="icon-box boxed-style align-center animated" data-animation="zoomOut"  style="width: 230px; height: 250px; box-shadow: 0 0 20px #a5a5a5; background-color: white;">
                            <div class="animated-icon colored" style="width: 100px;">
                                <img src="<?=url('assets/images/professional.png'); ?>" style="width: 70px;height: auto;"/>
                            </div>
                            <div class="ib-content">
                                <h5>Professional</h5>
                                <p>Want Technology Driven Solution for your Client?</p>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="{{route('retail.retailRegister',['seller'])}}">
                    <div class="col-sm-3 anim1 animated" data-duration="2000">
                        <div class="icon-box boxed-style align-center animated" data-animation="zoomOut" data-delay="600" style="width: 230px; height: 250px; box-shadow: 0 0 20px #a5a5a5; background-color: white;">
                            <div class="animated-icon colored" style="width: 100px;">
                                <img src="<?=url('assets/images/supplyer.png'); ?>" style="width: 70px;height: auto;"/>
                            </div>
                            <div class="ib-content">
                                <h5 style="width: 200px; margin-left: -20px;">Manufacturer / Supplier</h5>
                                <p>Looking for New Way to Reach?</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <a class="popup-close" id="pclose" data-popup-close="popup-register" href="#">x</a>
    </div>
</div>

<div id="rateModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <form action="<?=route('business.institutionalChangeHub'); ?>" method="post" id="hubForm">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                <h4 class="modal-title">Select Rate Type</h4>
            </div>
            <div class="modal-body">
                <?=csrf_field()?>
                <div class="row">
                    <?php foreach($hubs as $h){ ?>
                        <div class=" col-md-3">
                            <label><input type="radio" value="<?=$h->id?>" name="hub" required/> <?=$h->title?></label>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-default" >Done</button>
            </div>
            </form>
        </div>
    </div>
</div>
	</body>
</html>