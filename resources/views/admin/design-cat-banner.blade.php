<?php
echo $data['header'];
if(isset($_GET['add'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Add Category banner</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="design-cat-banner" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post"  enctype="multipart/form-data" style="max-width: 100%;">
			'.csrf_field().'
			
				<div class="row">
					  <div class="input-field col s12 m4">
						Title
						<input name="title" type="text"  class="form-control"/>
					  </div>
					  <div class="input-field col s12 m4">
						Short Description
						<input name="description" type="text" class="form-control"/>
					  </div>
					  
					  <div class="input-field col s12 m4">
						Banner image
						<input name="image" type="file" class="form-control" required="required" />
					  </div>
					  
					  <div class="row">
					  <input name="add" type="submit" value="Add banner" style="padding:3px 25px;" class="btn btn-primary" />
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} elseif(isset($_GET['edit'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Edit Category banner</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="design-cat-banner" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post"  enctype="multipart/form-data" style="max-width: 100%;">
			'.csrf_field().'
			
				<div class="row">
					  <div class="input-field col s12 m6">
						Banner title
						<input name="title" type="text"  value="'.$data['cbanner']->title.'" class="form-control"/>
					  </div>
					  <div class="input-field col s12 m6">
						Short Description
						<input name="description" type="text" value="'.$data['cbanner']->description.'" class="form-control"/>
					  </div>
					  </div><div class="row" style="margin-bottom: 10px;">';
    if (!empty($data['cbanner']->image)){

        echo '<p>Uploading new images will overwrtite current images .</p>';

        echo '<img class="col-md-2" src="'.url('/assets/banners/'.$data['cbanner']->image).'" required="required"/>';

        echo '<div class="clearfix"></div>';
    }


    echo '
					  <div class="input-field col s12 m6">
						Image
						<input type="file" class="form-control" name="image"/>
					  </div>
					  </div>
					  
					  
					  <input name="edit" type="submit" value="Edit banner" style="padding:3px 25px;" class="btn btn-primary" />
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} else {
?>
<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <h5>Category banner <a href="design-cat-banner?add" style="padding:3px 15px;" class="add">Add banner</a></h5>
                <h5 class="card-title" style="color: #0d1baa;">Manage Your Home page Banners </h5>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="datatable-editable">
                        <thead>
                        <tr class="bg-blue">
                            <th>Sr. No.</th>
                            <th>Image</th>
                            <th>Title</th>
                            <th>Short Description</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sr = 1;
                        echo $data['notices'];
                        foreach ($data['cbanners'] as $category){
                            echo'<tr>
            <td>'.$sr.'</td>
			<td><img src="../assets/banners/'.$category->image.'" style="height: auto; width: 100px;"></td>
            <td>'.$category->title.'</td>
			<td>'.$category->description.'</td>
			<td>
                <a href="design-cat-banner?delete='.$category->id.'"><i class="icon-trash"></i></a>
				<a href="design-cat-banner?edit='.$category->id.'"><i class="icon-pencil"></i></a>
            </td>
          </tr>';
                            $sr++; }
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php
echo $data['footer'];
?>
