<?php
echo $data['header'];
if(isset($_GET['add'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Add Layout addons</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="layout_addon" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
                <form action="" method="post"  enctype="multipart/form-data" style="max-width: 100%;">
			'.csrf_field().'
			
				<div class="row">
					  <div class="input-field col s12 m6">
						Addon name
						<input name="name" type="text"  class="form-control" />
					  </div>
					  <div class="input-field col s12 m6">
						Addon Price
						<input name="price" type="text" class="form-control"  />
					  </div>
					  </div>
					  <div class="row" style="margin-bottom: 10px;">
					  <div class="input-field col s12 m6">
						Addon Image
						<input name="image" type="file" class="form-control"  />
					  </div>
					  </div> &nbsp&nbsp&nbsp;
					  <input name="add" type="submit" value="Add addon" style="padding:3px 25px;" class="btn btn-primary" />
				
			</form>';
} elseif(isset($_GET['edit'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Edit Layout addons</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="layout_addon" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
                <form action="" method="post"  enctype="multipart/form-data" style="max-width: 100%;">
			'.csrf_field().'
			
				<div class="row">
					  <div class="input-field col s12 m6">
						Addon name
						<input name="name" type="text"  value="'.$data['addon']->name.'" class="form-control" />
					  </div>
					  
					  <div class="input-field col s12 m6">
						Addon Price
						<input name="price" type="text" value="'.$data['addon']->price.'" class="form-control"  />
					  </div>
					  </div>
					  <div class="row"><div class="input-field col s12 m6">';
    if (!empty($data['addon']->image)){
        echo '<p>Uploading new images will overwrtite current images .</p>';
        echo '<img class="col-md-2" src="'.url('/assets/addon/'.$data['addon']->image).'" />';
        echo '<div class="clearfix"></div>';
    }
    echo '
					  <div class="input-field col s12 m6">
						Image
						<input type="file" class="form-control" name="image"/>
					  </div>
					  </div>
					  </div>
					  <input name="edit" type="submit" value="Edit addon" style="padding:3px 25px;" class="btn btn-primary" />
				
			</form>';
} else {
    ?>
    <div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
    <h5>Layout addon<a href="layout_addon?add" style="padding:3px 15px;" class="add">Add layout addon</a></h5>
    <h5 class="card-title" style="color: #0d1baa;">Manage Your Layout Plans</h5>
    <div class="row">
    <div class="col-lg-12">
    <div class="table-responsive">
    <table class="table table-striped table-bordered" id="datatable-editable">
    <thead>
    <tr class="bg-blue">
        <th>Sr. No.</th>
        <th>Addon Image</th>
        <th>Addon Name</th>
        <th>Addon Price</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $sr = 1;
    echo $data['notices'];
    foreach ($data['addons'] as $addon){
        echo'<tr>
                            <td>'.$sr.'</td>
                            <td><img src="../assets/addon/'.image_order($addon->image).'" style="height: auto; width: 100px;"></td>
                            <td>'.$addon->name.'</td>
                            <td>'.$addon->price.'</td>
                            <td>
                                <a href="layout_addon?delete='.$addon->id.'"><i class="icon-trash"></i></a>
                                <a href="layout_addon?edit='.$addon->id.'"><i class="icon-pencil"></i></a>
                            </td>
                          </tr>';
        $sr++; }
}?>
    </tbody>
    </table>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
<?php
echo $data['footer'];
?>