<?php
echo $data['header'];
if(isset($_GET['add'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Add Service category</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="services_categories" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" enctype="multipart/form-data" style="max-width: 100%;">
			'.csrf_field().'
			
				<div class="row">
					  <div class="input-field col s12 m6">
						Category name
						<input name="name" type="text"  class="form-control" />
					  </div>
					  <div class="input-field col s12 m6">
						Short Description
						<textarea name="description" class="form-control" placeholder="Enter description..."></textarea>
					  </div>
					  </div>
					  <div class="row">
					  <div class="input-field col s12 m6">
						Category path
						<input name="path" type="text" class="form-control"  />
					  </div>
					  <div class="input-field col s12 m6">
						Category image
						<input name="image" type="file" class="form-control"  />
					  </div>
					  </div>
					  <div class="row">
					  <div class="input-field col s12 m6">
							Display In Home Page<br>
							<label style="margin-right: 5px; font-size: 12px;" class="radio_container">Yes<input name="display" type="radio" id="yes" value="1" /><span class="checkmark"></span></label><label style="margin-right: 5px; font-size: 12px;" class="radio_container">NO<input name="display" type="radio" id="no" value="0" checked/><span class="checkmark"></span></label>
					  </div>
					  <div class="input-field col s12 m6">
							List In Home Page<br>
							<label style="margin-right: 5px; font-size: 12px;" class="radio_container">Yes<input name="list" type="radio" id="yes" value="1" /><span class="checkmark"></span></label><label style="margin-right: 5px; font-size: 12px;" class="radio_container">No<input name="list" type="radio" id="no" value="0" checked/><span class="checkmark"></span></label>
					  </div>
					  </div>
					  <div class="row">
					  <div class="input-field col s12 m6">
							Parent category
							<select name="parent" class="form-control">
							<option value="0"></option>';
    foreach ($data['parents'] as $parent){
        echo '<option value="'.$parent->id.'">'.$parent->name.'</option>';
        $childs = DB::select("SELECT * FROM services_category WHERE parent = ".$parent->id);
        foreach ($childs as $child){
            echo '<option value="'.$child->id.'">- '.$child->name.'</option>';
            $subchilds = DB::select("SELECT * FROM services_category WHERE parent = ".$child->id);
            foreach ($subchilds as $subchild){
                echo '<option value="'.$subchild->id.'"><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
            }
        }
    }
    echo '</select>
					  </div>
					  </div>
					  <input name="add" type="submit" value="Add category" style="padding:3px 25px;" class="btn btn-primary" />
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} elseif(isset($_GET['edit'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Edit Service category</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="services_categories" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post"  enctype="multipart/form-data" style="max-width: 100%;">
			'.csrf_field().'
			
				<div class="row">
					  <div class="input-field col s12 m6">
						Category name
						<input name="name" type="text"  value="'.$data['services_categories']->name.'" class="form-control" />
					  </div>
					  <div class="input-field col s12 m6">
						Short Description
						<textarea name="description" class="form-control" placeholder="Enter description...">'.$data['services_categories']->description.'</textarea>
					  </div>
					  </div>
					  <div class="row">
					  <div class="input-field col s12 m6">
						Category path
						<input name="path" type="text" value="'.$data['services_categories']->path.'" class="form-control"  />
					  </div><div class="row"> 
					  <div class="input-field col s12 m6">
						Image
						<input type="file" class="form-control" name="image"/>
					  </div>';
    if (!empty($data['services_categories']->image)){

        echo '<div class="input-field col s12 m6"><p>Uploading new images will overwrtite current images .</p>';

        echo '<img class="col-md-4" src="'.url('/assets/services/'.$data['services_categories']->image).'" />';

        echo '<div class="clearfix"></div></div></div>';
    }

    if (!empty($data['services_categories']->download)){
        echo '<div class="input-field col s12 m6"><p>Uploading new file will overwrite current file .</p></div></div>';
    }
    echo'
                       
					  </div>
					 <div class="row">
					  <div class="input-field col s12 m6">
							  Display In Home Page<br>';
    $val = $data['services_categories']->display;
    if($val == '1'){
        echo '<label style="margin-right: 5px; font-size: 12px;" class="radio_container">Yes<input name="display" type="radio" id="yes" checked="checked" value="1" /><span class="checkmark"></span></label><label style="margin-right: 5px; font-size: 12px;" class="radio_container">No<input name="display" type="radio" id="no" value="0" /><span class="checkmark"></span></label>';
    }else {
        echo '<label style="margin-right: 5px; font-size: 12px;" class="radio_container">Yes<input name="display" type="radio" id="yes" value="1" /><span class="checkmark"></span></label><label style="margin-right: 5px; font-size: 12px;" class="radio_container">No<input name="display" type="radio" checked="checked"  id="no" value="0" /><span class="checkmark"></span></label>';
    }
    echo '</div>
                           
						  <div class="input-field col s12 m6">
							  List In Home Page<br>';
    $val = $data['services_categories']->list;
    if($val == '1'){
        echo '<label style="margin-right: 5px; font-size: 12px;" class="radio_container">Yes<input name="list" type="radio" id="yes1" checked="checked" value="1" /><span class="checkmark"></span></label><label style="margin-right: 5px; font-size: 12px;" class="radio_container">No<input name="list" type="radio" id="no1" value="0" /><span class="checkmark"></span></label>';
    }else {
        echo '<label style="margin-right: 5px; font-size: 12px;" class="radio_container">Yes<input name="list" type="radio" id="yes1" value="1" /><span class="checkmark"></span></label><label style="margin-right: 5px; font-size: 12px;" class="radio_container">No<input name="list" type="radio" checked="checked"  id="no1" value="0" /><span class="checkmark"></span></label>';
    }
    echo '</div>
                     </div>
                        <div class="row">
					  <div class="input-field col s12 m6">
							Parent category
							<select name="parent" class="form-control">
							<option value="0"></option>';
    foreach ($data['parents'] as $parent){
        echo '<option value="'.$parent->id.'" '.($parent->id == $services_categories->parent ? 'selected' : '').'>'.$parent->name.'</option>';
        $childs = DB::select("SELECT * FROM services_category WHERE parent = ".$parent->id);
        foreach ($childs as $child){
            echo '<option value="'.$child->id.'" '.($child->id == $services_categories->parent ? 'selected' : '').'>- '.$child->name.'</option>';
            $subchilds = DB::select("SELECT * FROM services_category WHERE parent = ".$child->id);
            foreach ($subchilds as $subchild){
                echo '<option value="'.$subchild->id.'" '.($subchild->id == $services_categories->parent ? 'selected' : '').'><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
            }
        }
    }
    echo '</select>
					  </div>
					  </div>
					  
					  <center>
					  <input name="edit" type="submit" value="Edit category" style="padding:3px 25px;" class="btn btn-primary" />
					  </center>
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} else {
    ?>
    <div class="row" style="margin-top: 10px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
    <h5>Service categories <a href="services_categories?add" style="padding: 3px 15px; font-size: 15px;" class="add">Add categories</a></h5>
    <h5 class="card-title" style=" color: #0d1baa;">Manage Your Service categories </h5>
    <div class="table-responsive">
    <table class="table table-striped table-bordered" id="datatable-editable">
    <thead>
    <tr class="bg-blue">
        <th>Sr. No.</th>
        <th>Category Image</th>
        <th>Category Name</th>
        <th>Diplay</th>
        <th>List</th>
        <th>Parent Category Name</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $sr = 1;
    echo $data['notices'];
    foreach ($data['services_categories'] as $category){
        $disp = 'No';
        $list = 'No';
        if($category->display == '0'){
            $disp = 'No';
        }else{
            $disp = 'Yes';
        }
        if($category->list == '0'){
            $list = 'No';
        }else{
            $list = 'Yes';
        }
        echo'<tr>
            <td>'.$sr.'</td>
            <td><img src="../assets/services/'.image_order($category->image).'" style="height: 100px; width: 100px;"></td>
            <td><a href="../'.$category->path.'">'.$category->name.'</a></td>
            <td>'.$disp.'</td>
            <td>'.$list.'</td>';
        if($category->parent != 0){
            $parent = DB::select("SELECT * FROM services_category WHERE id = ".$category->parent)[0];
            echo'<td>'.$parent->name.'</td>';
        } else {
            echo'<td>No Parent</td>';
        }
        echo'
            <td><a href="services_categories?delete='.$category->id.'" title="delete" data-toggle="tooltip"><i class="icon-trash"></i></a>
				<a href="services_categories?edit='.$category->id.'" title="edit" data-toggle="tooltip"><i class="icon-pencil"></i></a>
				<a href="category-update-questions?edit='.$category->id.'" title="add question" data-toggle="tooltip"><i class="icon-question"></i></a>';
        if($category->parent == 0) {
            echo '<a href="service_category_image?edit=' . $category->id . '"  title="add image" data-toggle="tooltip"><i class="icon-plus"></i></a>';
        }
        echo '</td>
          </tr>';
        $sr++; }
}?>
    </tbody>
    </table>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
<?php
echo $data['footer'];
?>