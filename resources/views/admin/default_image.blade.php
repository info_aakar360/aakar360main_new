<?php
echo $data['header'];
if(isset($_GET['add'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
    <h5>Add new Default Image</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="default_image" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
    <form action="" method="post" class="form-horizontal single"  enctype="multipart/form-data">
			'.csrf_field().'
			
				<fieldset>
					  <div class="form-group">
						<label class="control-label">Banner image</label>
						<input name="image" type="file" class="form-control" required="required" />
					  </div>
					  <div class="form-group">
						<label class="control-label">Page</label>
						<select name="page" class="form-control">
						    <option value="">Please Select Page</option>
						    <option value="Checkout">Checkout</option>
						    <option value="Login">Login</option>
						    <option value="Register">Register</option>
						    <option value="Account">Account</option>
						    <option value="Advices">Advices</option>
                        </select>
					  </div>
					  
					  <input name="add" type="submit" value="Add banner" style="padding:3px 25px;" class="btn btn-primary" />
				</fieldset>
			</form>
			</div>
			</div>
			</div>
			</div>';
} elseif(isset($_GET['edit'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
    <h5>Edit Default Image</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="default_image" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <form action="" method="post" style="max-width: 100%;" enctype="multipart/form-data">
			'.csrf_field().'
			
				<div class="row">';
    if (!empty($data['image']->image)){
        echo '<p>Uploading new images will overwrtite current images .</p>';
        echo '<img class="col-md-2" src="'.url('/assets/banners/'.$data['image']->image).'" required="required"/>';
        echo '<div class="clearfix"></div>';
    }
    echo '
					  <div class="form-group col-md-6">
						<label class="control-label">Image</label>
						<input type="file" class="form-control" name="image"/>
					  </div>
					  
					  <div class="form-group col-md-6">
						<label class="control-label">Page</label>
						<select name="page" class="form-control">';
    if($data['image']->page == 'Checkout'){
        echo '<option value="">Please Select Page</option>
                                <option value="Checkout" selected>Checkout</option>
                                <option value="Login">Login</option>
                                <option value="Register">Register</option>
                                <option value="Account">Account</option>
                                <option value="Advices">Advices</option>';
    }
    if($data['image']->page == 'Login'){
        echo '<option value="">Please Select Page</option>
                                <option value="Checkout">Checkout</option>
                                <option value="Login" selected>Login</option>
                                <option value="Register">Register</option>
                                <option value="Account">Account</option>
                                <option value="Advices">Advices</option>';
    }
    if($data['image']->page == 'Register'){
        echo '<option value="">Please Select Page</option>
                                <option value="Checkout">Checkout</option>
                                <option value="Login">Login</option>
                                <option value="Register" selected>Register</option>
                                <option value="Account">Account</option>
                                <option value="Advices">Advices</option>';
    }
    if($data['image']->page == 'Account'){
        echo '<option value="">Please Select Page</option>
                                <option value="Checkout">Checkout</option>
                                <option value="Login">Login</option>
                                <option value="Register">Register</option>
                                <option value="Account" selected>Account</option>
                                <option value="Advices">Advices</option>';
    }
    if($data['image']->page == 'Advices'){
        echo '<option value="">Please Select Page</option>
                                <option value="Checkout">Checkout</option>
                                <option value="Login">Login</option>
                                <option value="Register">Register</option>
                                <option value="Account">Account</option>
                                <option value="Advices" selected>Advices</option>';
    }
    echo '</select>
					  </div>
					  
					  <input name="edit" type="submit" value="Edit image" style="padding:3px 25px;" class="btn btn-primary" />
				
			</form>
			</div>
			</div>
			</div>
			</div>';
} else {
?>
<div class="row" style="margin-top: 10px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <div class="head">
                    <h4>Default Image<a href="default_image?add" style="padding: 3px 15px;" class="add">Add Image</a></h4>
                    <p>Manage your default image</p>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="datatable-editable">
                                <thead>
                                <tr class="bg-blue">
                                    <th>Sr. No.</th>
                                    <th>Image</th>
                                    <th>Page</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $sr = 1;
                                echo $data['notices'];
                                foreach ($data['images'] as $category){
                                    echo'<tr>
                        <td>'.$sr.'</td>
                        <td><img src="../assets/banners/'.$category->image.'" style="height: auto; width: 100px;"></td>
                        <td>'.$category->page.'</td>
                        <td>
                            <a href="default_image?delete='.$category->id.'"><i class="icon-trash"></i></a>
                            <a href="default_image?edit='.$category->id.'"><i class="icon-pencil"></i></a>
                        </td>
                      </tr>';
                                    $sr++; }
                                } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <?php
                echo $data['footer'];
                ?>
