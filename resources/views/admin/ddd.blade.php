<div class="row" style="position:relative;">
    <div class="col s12 table-responsive"><!--1x 11661593 6YDgrn-->
        <?php if($quot=='brand_wise'){
            ?>
            <input type="hidden" value="0" name ="price_hide" class="price_hide">
            <table class="highlight responsive-table">
                <thead>
                <tr>
                    <th style="border: 1px solid; text-align: center;">S. No.</th>
                    <th style="border: 1px solid; text-align: center;">Item</th>
                    <th style="border: 1px solid; text-align: center;">Brand</th>
                    <th style="border: 1px solid; text-align: center;">Length</th>
                    <th style="border: 1px solid; text-align: center;">Qty in Pc</th>
                    <th style="border: 1px solid; text-align: center;">Qty in MT</th>
                    <th class="price_details" style="border: 1px solid; text-align: center;">Basic</th>
                    <th class="price_details" style="border: 1px solid; text-align: center;">Size Diffrence</th>
                    <th class="price_details" style="border: 1px solid; text-align: center;">Tax Amount</th>
                    <th style="border: 1px solid; text-align: center;">Inclusive Tax</th>
                    <th style="border: 1px solid; text-align: center;">Remark</th>
                </tr>
                </thead>
                <tbody>
                <?php $sr = 1;
                foreach ($brands as $br) {
                    $br_id =  $br->id;

                    $get_cat = DB::select("SELECT category FROM products  WHERE id = '$br_id'")[0];
                    $get_cat_id = $get_cat->category;
                    $cust_product_price = getCategoryAddPrice($get_cat_id, $cust_id);

                    foreach ($variants as $b) {
                        $vid =  $b->id;
                        $variants_details = DB::select("SELECT p.*,pv.* FROM products as p INNER JOIN product_variants as pv ON p.id = pv.product_id WHERE p.id = '$br_id' AND pv.variant_title = (SELECT variant_title FROM product_variants WHERE id ='$vid')");
                        if(empty($variants_details)){
                            $variants_details = DB::select("SELECT p.* FROM products as p  WHERE p.id = '$br_id'");
                        }

                        $vsr = 0;
                        foreach ($var_id as $varid){

                            if($brn_id[$vsr]==$br_id && $var_id[$vsr]==$vid ){

                                if($hide_status[$vsr]==0){
                                    ?>
                                    <tr>
                                        <td style="border: 1px solid; text-align: center;"><input type="hidden" name="var[]" value="<?=$vid; ?>"> <?=$sr?></td>

                                        <td style="border: 1px solid; text-align: center;"><input class="variant_details" type="hidden" name="size[]" value="<?=$b->variant_title; ?>"><?=$b->variant_title; ?></td>
                                        <td style="border: 1px solid; text-align: center;">
                                            <select name="brand[]">
                                                <option value="<?=$variants_details[0]->brand_id; ?>"><?=getBrandNames($variants_details[0]->brand_id); ?></option>
                                                <option value="NA">NA</option>
                                            </select>
                                        </td>
                                        <td style="border: 1px solid; text-align: left; padding: 10px;"><input type="text" name="length[]" class="form-control"></td>
                                        <td style="border: 1px solid; text-align: left; padding: 10px;"><input type="text" name="Qty_pc[]" class="form-control"></td>
                                        <td style="border: 1px solid; text-align: left; padding: 10px;"><input type="text" name="Qty_mt[]" class="form-control"></td>
                                        <?php

                                        $sd = is_numeric($variants_details[0]->price) ? $variants_details[0]->price : 0;
                                        $pp = is_numeric($variants_details[0]->purchase_price) ? $variants_details[0]->purchase_price : 0;

                                        $pp = $pp + $set_price + $cust_product_price;
                                        $tax = $variants_details[0]->tax;
                                        $tx_val =  ($sd+$pp)*$tax/100;
                                        ?>
                                        <td class="price_details" style="border: 1px solid; text-align: center;"><input type="hidden" name="basic_price[]" value="<?=$pp; ?>"><?=$pp; ?></td>
                                        <td class="price_details" style="border: 1px solid; text-align: center; padding: 10px;"><input type="hidden" name="size_diffrence_price[]" value="<?=$sd; ?>"><?=$sd; ?></td>
                                        <input type="hidden" name="tax_set[]" value="<?=$tax;?>">
                                        <td class="price_details" style="border: 1px solid; text-align: center; padding: 10px;"><input type="hidden" name="tax_amount[]" value="<?=$tx_val =  ($sd+$pp)*$tax/100; ?>"><?=$tx_val =  ($sd+$pp)*$tax/100; ?> (<?=$tax; ?>%)</td>
                                        <td style="border: 1px solid; text-align: center; padding: 10px;"><input type="hidden" name="total[]" value="<?php echo $result = $tx_val + ($sd+$pp); ?>"><?php echo $result = $tx_val + ($sd+$pp); ?></td><input type="hidden" class="v_tot_price" value="<?=$result?>">

                                        <td style="border: 1px solid; text-align: left; padding: 10px;"><input type="text" name="remark[]" class="form-control"></td>
                                    </tr>

                                    <?php
                                    $sr++;
                                }
                            }
                            $vsr++;
                        }




                    }
                }
                ?>
                </tbody>
            </table>
            <?php
        }
        else{
            ?>
            <table class="highlight responsive-table">
                <input type="hidden" value="0" name ="price_hide" class="price_hide">
                <thead>
                <tr>
                    <th style="border: 1px solid; text-align: center;">S. No.</th>
                    <th style="border: 1px solid; text-align: center;">Item</th>
                    <th style="border: 1px solid; text-align: center;">Brand</th>
                    <th style="border: 1px solid; text-align: center;">Length</th>
                    <th style="border: 1px solid; text-align: center;">Qty in Pc</th>
                    <th style="border: 1px solid; text-align: center;">Qty in MT</th>
                    <th class="price_details" style="border: 1px solid; text-align: center;">Basic </th>
                    <th class="price_details" style="border: 1px solid; text-align: center;">Size Diffrence </th>
                    <th class="price_details" style="border: 1px solid; text-align: center;">Inclusive Tax</th>
                    <th style="border: 1px solid; text-align: center;">Total</th>
                    <th style="border: 1px solid; text-align: center;">Remark</th>
                </tr>
                </thead>
                <tbody>
                <?php $sr = 1;

                foreach ($variants as $b) {
                    $vid =  $b->id;
                    $vt =  $b->variant_title;
                    $variants_details = DB::select("SELECT t1.*,p.title,p.purchase_price,p.tax,p.category FROM products as p INNER JOIN (SELECT * FROM product_variants WHERE price = (SELECT min(price) FROM product_variants WHERE variant_title = '$vt')) as t1 ON p.id = t1.product_id");
                    $get_cat_id = $variants_details[0]->category;

                    if(empty($variants_details)){
                        $variants_details = DB::select("SELECT p.* FROM products as p  WHERE p.id = '$br_id'");
                    }

                    $cust_product_price = getCategoryAddPrice($get_cat_id, $cust_id);

                    ?>
                    <tr>
                        <td style="border: 1px solid; text-align: center;"><input type="hidden" name="var[]" value="<?=$sr; ?>"> <?=$sr?></td>

                        <td style="border: 1px solid; text-align: center;"><input class="variant_details" type="hidden" name="size[]" value="<?=$b->variant_title; ?>"><?=$b->variant_title; ?></td>
                        <td style="border: 1px solid; text-align: center;">
                            <select name="brand[]">
                                <option value="<?=$variants_details[0]->brand_id; ?>"><?=getBrandNames($variants_details[0]->brand_id); ?></option>
                                <option value="NA">NA</option>
                            </select>
                        </td>
                        <td style="border: 1px solid; text-align: left; padding: 10px;"><input type="text" name="length[]" class="form-control"></td>
                        <td style="border: 1px solid; text-align: left; padding: 10px;"><input type="text" name="Qty_pc[]" class="form-control"></td>
                        <td style="border: 1px solid; text-align: left; padding: 10px;"><input type="text" name="Qty_mt[]" class="form-control"></td>
                        <?php

                        $sd = is_numeric($variants_details[0]->price) ? $variants_details[0]->price : 0;
                        $pp = is_numeric($variants_details[0]->purchase_price) ? $variants_details[0]->purchase_price : 0;
                        $pp = $pp + $set_price + $cust_product_price;
                        $tax = $variants_details[0]->tax;
                        $tx_val =  ($sd+$pp)*$tax/100;
                        ?>
                        <td class="price_details" style="border: 1px solid; text-align: center;"><input type="hidden" name="basic_price[]" value="<?=$pp; ?>"><?=$pp; ?></td>
                        <td class="price_details" style="border: 1px solid; text-align: center; padding: 10px;"><input type="hidden" name="size_diffrence_price[]" value="<?=$sd; ?>"><?=$sd; ?></td>
                        <input type="hidden" name="tax_set[]" value="<?=$tax;?>">
                        <td class="price_details" style="border: 1px solid; text-align: center; padding: 10px;"><input type="hidden" name="tax_amount[]" value="<?=$tx_val =  ($sd+$pp)*$tax/100; ?>"><?=$tx_val =  ($sd+$pp)*$tax/100; ?> (<?=$tax; ?>%)</td>
                        <td style="border: 1px solid; text-align: center; padding: 10px;"><input type="hidden" name="total[]" value="<?php echo $result = $tx_val + ($sd+$pp); ?>"><?php echo $result = $tx_val + ($sd+$pp); ?></td><input type="hidden" class="v_tot_price" value="<?=$result?>">
                        <td style="border: 1px solid; text-align: left; padding: 10px;"><input type="text" name="remark[]" class="form-control"></td>
                    </tr>
                    <?php $sr++;

                }
                ?>
                </tbody>
            </table>
            <?php
        }
        ?>

    </div>
</div>