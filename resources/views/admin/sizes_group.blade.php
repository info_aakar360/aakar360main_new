<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <!--    <title>-->
    <!--        --><?//=$title; ?>
    <!--    </title>-->
    <link rel="apple-touch-icon" href="<?=$data['tp']; ?>/admin/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$data['tp']; ?>/admin/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/custom/custom.css">
    <link href="<?=$data['tp']; ?>/admin/css/select2.min.css" rel="stylesheet" />
    <!-- END: Custom CSS-->
</head>
<style>
    /*.select2-container--default .select2-selection--multiple .select2-selection__rendered {*/

    /*max-height: 300px;*/
    /**/
    /*}*/


    /*.select2-container--default.select2-container--focus .select2-selection--multiple {*/
    /*border-bottom: solid black 1px;*/
    /*outline: 0;*/
    /*height: 300px;*/
    /*width:400px;*/
    /*}*/
    /*.select2-container--default .select2-selection--multiple .select2-selection__rendered {*/
    /*box-sizing: border-box;*/
    /*list-style: none;*/
    /*margin: 0;*/
    /*padding: 0 5px;*/
    /*width: 10%;*/
    /*max-height: 300px;*/
    /*overflow-y: auto;*/
    /*}*/

</style>
<?php
echo $data['header'];
if(isset($_GET['add']))
{
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Add Size Group</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="sizes_group" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" style="max-width: 100%">
				'.csrf_field().'
				
					<div class="row">
						  <div class="input-field col s12 m12">
							<label class="control-label">Name</label>
							<input name="name" type="text" class="form-control" required />
						  </div>
						  </div>
						  <div class="row">
						  <div class="input-field col s12 m12" >
							Sizes
							<select name="sizes[]" class="form-control select2" multiple>
							    <option value="">Please select sizes</option>';
    foreach ($data['sizes'] as $size){
        echo '<option value="'.$size->id.'">'.$size->name.'</option>';
    }
    echo '</select>
						  </div>
						  </div>
						  <div class="row">
						  <input name="add" type="submit" value="Add size" class="btn btn-primary" />
					</div>
					</div>
				</form>
				</div>
				</div>
				</div>
				</div>';
}
elseif(isset($_GET['edit']))
{
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Edit Size Group</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="sizes_group" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" style="max-width: 100%">
				'.csrf_field().'
				
					<div class="row">
						  <div class="input-field col s12 m12">
							Name
							<input name="name" value="'.$data['size']->name.'" type="text" class="form-control" required />
						  </div>
						  </div>
						  <div class="row">
						  <div class="input-field col s12 m12">
							Sizes
							<select name="sizes[]" class="form-control select2" multiple>
							    <option value="">Please select sizes</option>';
    foreach ($data['sizes'] as $siz){
        $selected = '';
        $ss = explode(',', $size->sizes);
        if (in_array($siz->id, $ss)) { $selected = 'selected'; } else { echo ''; }
        echo '<option value="'.$siz->id.'" '.$selected.'>'.$siz->name.'</option>';
    }
    echo '</select>
						  </div>
						  </div>
						  <div class="row">
						  <input name="edit" type="submit" value="Edit size" class="btn btn-primary" />
					</div>
					</div>
				</form>
				</div>
				</div>
				</div>
				</div>';
} else {
    ?>
    <div class="row" style="margin-top: 13px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 550px;">
                    <h5>Size Group<a href="sizes_group?add" style="padding:3px 15px; font-size: 15px;" class="add">Add Size Group</a></h5>
                    <h5 class="card-title" style=" color: #0d1baa;">Manage Your Size Group</h5>
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="datatable-editable">
                                <thead>
                                <tr class="bg-blue">
                                    <th>Sr. No.</th>
                                    <th>Name</th>
                                    <th>Sizes</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $sr = 1;
                                echo $data['notices'];
                                foreach ($data['groups'] as $category){
                                    echo'<tr>
                                <td>'.$sr.'</td>
                                <td>'.$category->name.'</td>
                                <td>';
                                    $sx = explode(',', $category->sizes);
                                    $ss = array();
                                    foreach ($sx as $s) {
                                        $sv = DB::table('size')->where('id', '=', $s)->first();
                                        if($sv !== null){
                                            $ss[] = $sv->name;
                                        }
                                    }
                                    echo implode(', ', $ss).'</td>
                                <td>
                                    <a href="sizes_group?delete='.$category->id.'"><i class="icon-trash"></i></a>
                                    <a href="sizes_group?edit='.$category->id.'"><i class="icon-pencil"></i></a>
                                </td>
                              </tr>';
                                    $sr++;
                                }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php }
echo $data['footer'];
?>

<script src="<?=$data['tp']; ?>/admin/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$data['tp']; ?>/admin/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$data['tp']; ?>/admin/js/plugins.js" type="text/javascript"></script>
<script src="<?=$data['tp']; ?>/admin/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$data['tp']; ?>/admin/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$data['tp']; ?>/admin/js/select2.min.js"></script>
<script src="<?=$data['tp']; ?>/admin/js/scripts/ui-alerts.js" type="text/javascript"></script>
<!-- <script src="<?php //$data['tp']; ?>/js/scripts/data-tables.js" type="text/javascript"></script> -->
<script>
    $( document ).ready(function() {
        $('.select2').select2({
            placeholder: "Select sizes",
            allowClear: true
        });
    });
</script>
