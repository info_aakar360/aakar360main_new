<?php
echo $data['header'];
if(isset($_GET['add']))
{
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Add New Postcodes</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="postcodes" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" style="max-width: 100%">
				'.csrf_field().'
				
					<div class="row">
						<div class="input-field col s12 m6">
							Postcode
							<input name="postcode" type="text" class="form-control" required />
						</div>
						<div class="input-field col s12 m6">
							City
							<input name="city" type="text" class="form-control" required />
						</div>
						</div>
						<div class="row">
						<div class="input-field col s12 m6">
							District
							<input name="district" type="text" class="form-control" required />
						</div>
						<div class="input-field col s12 m6">
							State
							<input name="state" type="text" class="form-control" required />
						</div>
						</div>
						<div class="row">
						<div class="input-field col s12 m6">
							Delivery Available<br>
							<label style="margin-right: 5px; font-size: 12px;" class="radio_container">Yes<input name="delivery" type="radio" id="yes" value="1" checked="true"/><span class="checkmark"></span></label> <label style="margin-right: 5px; font-size: 12px;" class="radio_container">No<input name="delivery" type="radio" id="no" value="0" /><span class="checkmark"></span></label>
						</div>
						<div class="input-field col s12 m6">
							Delivery Information
							<input name="info" type="text" class="form-control" required />
						</div>
						</div>
						<input name="add" type="submit" value="Add Postcode" style="padding:3px 25px;" class="btn btn-primary" />
					</div>
				</form>
				</div>
				</div>
				</div>
				</div>';
}
elseif(isset($_GET['edit']))
{
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Edit Postcodes</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="postcodes" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" style="max-width: 100%">
				'.csrf_field().'
				
					<div class="row">
						<div class="input-field col s12 m6">
							Postcode
							<input name="postcode" type="text" class="form-control" required value="'.$data['postcode']->pincode.'"/>
						</div>
						<div class="input-field col s12 m6">
							City
							<input name="city" type="text" class="form-control" required value="'.$data['postcode']->city.'"/>
						</div>
						</div>
						<div class="row">
						<div class="input-field col s12 m6">
							District
							<input name="district" type="text" class="form-control" required value="'.$data['postcode']->district.'"/>
						</div>
						<div class="input-field col s12 m6">
							State
							<input name="state" type="text" class="form-control" required value="'.$data['postcode']->state.'"/>
						</div></div>
						<div class="row">
						<div class="input-field col s12 m6">
							Delivery Available<br>';
    $val = $data['postcode']->delivery;
    if($val == '1'){
        echo '<label style="margin-right: 5px; font-size: 12px;" class="radio_container">Yes<input name="delivery" type="radio" id="yes" checked="checked" value="1" /><span class="checkmark"></span></label><label style="margin-right: 5px; font-size: 12px;" class="radio_container">No<input name="delivery" type="radio" id="no" value="0" /><span class="checkmark"></span></label>';
    }else {
        echo '<label style="margin-right: 5px; font-size: 12px;" class="radio_container">Yes<input name="delivery" type="radio" id="yes" value="1" /><span class="checkmark"></span></label><label style="margin-right: 5px; font-size: 12px;" class="radio_container">No<input name="delivery" type="radio" checked="checked"  id="no" value="0" /><span class="checkmark"></span></label>';
    }
    echo '
						</div>
						<div class="input-field col s12 m6">
							Delivery Information
							<input name="info" type="text" class="form-control" required value="'.$data['postcode']->info.'"/>
						</div>
						</div>
						<input name="edit" type="submit" style="padding:3px 25px;" value="Update Postcode" class="btn btn-primary" />
					</div>
				</form>
				</div>
				</div>
				</div>
				</div>';
} else {
    ?>
    <div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
    <h5>Postcodes<a href="postcodes?add" style="padding: 3px 15px;" class="add">Add Postcode</a></h5>
    <h6 style="color: #0a6ebd">Set your shipping postcodes and delivery informations</h6>

    <div class="row">
    <div class="col-lg-12">
    <div class="table-responsive">
    <table class="table table-striped table-bordered" id="datatable-editable">
    <thead>
    <tr class="bg-blue">
        <th>Sr. No.</th>
        <th>Postcode</th>
        <th>Delivery Status</th>
        <th>Delivery Info</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $sr = 1;
    echo $data['notices'];
    foreach ($data['postcodes'] as $postcode){
        echo'<tr>
            <td>'.$sr.'</td>
			<td>'.$postcode->pincode.'</td>
			<td>';
        if($postcode->delivery){
            echo '<label class="label label-success">Yes</label>';
        }else{
            echo '<label class="label label-warning">No</label>';
        }
        echo '</td>
			<td>'.$postcode->info.'</td>
			<td>
                <a href="postcodes?delete='.$postcode->id.'"><i class="icon-trash"></i></a>
				<a href="postcodes?edit='.$postcode->id.'"><i class="icon-pencil"></i></a>
            </td>
          </tr>
          ';
        $sr++; }
}?>
    </tbody>
    </table>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
<?php
echo $data['footer'];
?>