<?php
echo $data['header'];
if(isset($_GET['add'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Add Featured</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="featured" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post"  enctype="multipart/form-data" style="max-width: 100%">
			'.csrf_field().'
			
				<div class="row">
						<div class="input-field col s12 m6">
							Category Page
							<select name="category" class="form-control select2">
							<option value="0"></option>';
    foreach ($data['categories'] as $category){
        echo '<option value="'.$category->id.'">'.$category->name.'</option>';
        $childs = DB::select("SELECT * FROM category WHERE parent = ".$category->id);
        foreach ($childs as $child){
            echo '<option value="'.$child->id.'">- '.$child->name.'</option>';
            $subchilds = DB::select("SELECT * FROM category WHERE parent = ".$child->id);
            foreach ($subchilds as $subchild){
                echo '<option value="'.$subchild->id.'"><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
            }
        }
    }
    echo '</select>
						</div>
						<div class="input-field col s12 m6">
							Suggested Products
							<select name="products[]" multiple class="form-control select2">';
    foreach ($data['products'] as $product){
        echo '<option value="'.$product->id.'">'.$product->title.'</option>';
    }
    echo '</select>
						</div>
					  <input name="add" type="submit" value="Submit" class="btn btn-primary" />
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} elseif(isset($_GET['edit'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Edit Featured</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="featured" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" enctype="multipart/form-data" style="max-width: 100%">
			'.csrf_field().'
			
				<div class="row">
					  
					  <div class="input-field col s12 m6">
							Category Page
							<select name="category" class="form-control select2">
							<option value="0"></option>';
    foreach ($data['categories'] as $category){
        echo '<option value="'.$category->id.'" '.($category->id == $featured->category_id ? 'selected' : '').'>'.$category->name.'</option>';
        $childs = DB::select("SELECT * FROM category WHERE parent = ".$category->id);
        foreach ($childs as $child){
            echo '<option value="'.$child->id.'" '.($child->id == $featured->category_id ? 'selected' : '').'>- '.$child->name.'</option>';
            $subchilds = DB::select("SELECT * FROM category WHERE parent = ".$child->id);
            foreach ($subchilds as $subchild){
                echo '<option value="'.$subchild->id.'" '.($subchild->id == $featured->category_id ? 'selected' : '').'><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
            }
        }
    }
    echo '</select>
					  </div>
					  <div class="input-field col s12 m6">
							Suggested Products
							<select name="products[]" multiple class="form-control select2">';
    foreach ($data['products'] as $product){
        $cbrands = array();
        if($data['featured']->product_id){
            $cbrands = explode(',', $data['featured']->product_id);
        }
        echo '<option value="'.$product->id.'" '.(in_array($product->id, $cbrands) ? 'selected' : '').'>'.$product->title.'</option>';

    }
    echo '</select>
						  </div>
					  <input name="edit" type="submit" value="Update" class="btn btn-primary" />
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} else {
?>
<div class="row" style="margin-top: 10px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <h5>Featured products<a href="featured?add" style="padding: 3px 15px;" class="add">Add Featured</a></h5>
                <h5 class="card-title" style="color: #0d1baa;">Manage Your Suggestions </h5>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="datatable-editable">
                        <thead>
                        <tr class="bg-blue">
                            <th>Sr. No.</th>
                            <th>Category Page</th>
                            <th>Product Name(s)</th>
                            <th>Created On</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sr = 1;
                        echo $data['notices'];
                        foreach ($data['featured_products'] as $feature){
                            echo'<tr>
            <td>'.$sr.'</td>
			<td>'.getCategoryName($feature->category_id).'</td>';
                            $pros = getProducts($feature->product_id);
                            $html = '';
                            $i = 1;
                            foreach($pros as $pro){
                                $html.= $i.'. '.$pro->title.'<br>';
                                $i++;
                            }
                            echo '
			<td>'.$html.'</td>
			<td>'.$feature->created_at.'</td>
			<td>
                <a href="featured?delete='.$feature->id.'"><i class="icon-trash"></i></a>
				<a href="featured?edit='.$feature->id.'"><i class="icon-pencil"></i></a>
            </td>
          </tr>
          ';
                            $sr++; }
                        }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php
echo $data['footer'];
?>
<script>
    $( document ).ready(function() {
        $('.select2').select2({
            placeholder: "Please Select...",
            allowClear: true
        });
    });
</script>