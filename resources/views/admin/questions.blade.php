<?php
echo $header;
if(isset($_GET['add'])) {
    echo $notices.'<form action="" method="post" class="form-horizontal single">
			'.csrf_field().'
			<h5><a href="questions"><i class="icon-arrow-left"></i></a>Add new Question</h5>
				<fieldset>
					  <div class="form-group">
						<label class="control-label">Question</label>
						<input name="question" type="text"  class="form-control" />
					  </div>
					  <div class="form-group" id="que-type">
							<label class="control-label">Question Type</label>
							<select name="type" onChange="getData(this.value)" class="form-control">
							    <option value="text" >Text</option>
							    <option value="single">Single Select</option>
								<option value="dropdown">Drop Down</option>
							    <option value="multiple">Multiple Select</option>
							    <option value="textarea">Textarea</option>
							</select>
							<div class="col-md-12 options" style="display: none;">
								<label class="control-label col-md-12" data="0">Option 1</label>
								<input name="option[]" type="text" data="0" class="form-control col-md-11" style="width: 90%;margin-right: 5px;" />
							</div>
							<a onClick="" class="btn btn-primary pull-right options" id="add-option" style="display: none;">Add New</a>
							
					  </div>
					  
					  
					  <div class="form-group">
						<label class="control-label">Weightage</label>
						<input name="weightage" type="text" class="form-control" value="1" />
					  </div>
					  <input name="add" type="submit" value="Submit" class="btn btn-primary" />
				</fieldset>
			</form>';
} elseif(isset($_GET['edit'])) {
    $selected['text'] = '';
    $selected['single'] = '';
    $selected['dropdown'] = '';
    $selected['multiple'] = '';
    $selected['textarea'] = '';
    if($ques->type){
        $selected[$ques->type] = 'selected="selected"';
    }

    echo $notices.'<form action="" method="post" class="form-horizontal single">
			'.csrf_field().'
			<h5><a href="questions"><i class="icon-arrow-left"></i></a>Edit Question</h5>
				<fieldset>
					  <div class="form-group">
						<label class="control-label">Question</label>
						<input name="question" type="text"  value="'.$ques->question.'" class="form-control" />
					  </div>
					  <div class="form-group" id="que-type">
							<label class="control-label">Question Type</label>
							<select name="type" onChange="getData(this.value)" class="form-control">
							    <option value="text" '.$selected['text'].'>Text</option>
							    <option value="single" '.$selected['single'].'>Single Select</option>
							    <option value="dropdown" '.$selected['dropdown'].'>Drop Down</option>
							    <option value="multiple" '.$selected['multiple'].'>Multiple Select</option>
							    <option value="textarea" '.$selected['textarea'].'>Textarea</option>
							</select>
					  ';
					  if($ques->type == 'text' || $ques->type == 'textarea'){
						  echo '<div class="col-md-12 options" style="display: none;">
									<label class="control-label col-md-12" data="0">Option 1</label>
									<input name="option[]" type="text" data="0" class="form-control col-md-11" style="width: 90%;margin-right: 5px;" />
								</div>
								<a onClick="" class="btn btn-primary pull-right options" id="add-option" style="display: none;">Add New</a>';
					  }else{
						  $options = json_decode($ques->options);
						  foreach($options as $key => $value){
							echo '<div class="col-md-12 options">
									<label class="control-label col-md-12" data="'.$key.'">Option '.($key+1).'</label>
									<input name="option[]" type="text" data="'.$key.'" value="'.$value.'" class="form-control col-md-11" style="width: 90%;margin-right: 5px;" />';
									if($key != 0){
										echo '<a onClick="removeThis('.$key.');" class="pul-right" style="line-height: 3;" data="'.$key.'"><i class="icon-close" style="font-size: 20px;"></i></a>';
									}
								echo '</div>';
						  }
						  echo '<a onClick="" class="btn btn-primary pull-right options" id="add-option" >Add New</a>';
					  }
					  echo '</div>
					  <div class="form-group">
						<label class="control-label">Weightage</label>
						<input name="weightage" type="text" value="'.$ques->weightage.'" class="form-control"  />
					  </div>
					  
					  <input name="edit" type="submit" value="Update" class="btn btn-primary" />
				</fieldset>
			</form>';
} else {
    ?>
    <div class="head">
        <h3>Questions<a href="questions?add" class="add">Add Questions</a></h3>
        <p>Manage your Questions</p>
    </div>
    <?php
    echo $notices;
    foreach ($que as $que){
        echo'<div class="mini bloc">
			<h5>
				<a href="questions?edit='.$que->id.'">'.$que->question.'</a>
                <div class="tools">
					<a href="questions?delete='.$que->id.'"><i class="icon-trash"></i></a>
					<a href="questions?edit='.$que->id.'"><i class="icon-pencil"></i></a>
				</div>
			</h5>
		</div>';
    }
}
echo $footer;
?>
<script>
function removeThis($id){
	event.preventDefault();
	$('[data='+$id+']').remove();
}
$('#add-option').on('click', function(){
	var id = $('#que-type > div').length;
	var html = '<div class="col-md-12 options" data="'+id+'"><label class="control-label col-md-12" data="'+id+'">Option '+(id+1)+'</label><input name="option[]" type="text" data="'+id+'" class="form-control col-md-11" style="width: 90%;margin-right: 5px;" /><a onClick="removeThis('+id+');" class="pul-right" style="line-height: 3;" data="'+id+'"><i class="icon-close" style="font-size: 20px;"></i></a></div>';
	$('#add-option').before(html);
});
function getData(data){
	if(data == 'text' || data == 'textarea'){
		$('.options').hide();
	}else{
		$('.options').show();
	}	
}


</script>