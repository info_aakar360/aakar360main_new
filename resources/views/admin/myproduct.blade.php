<?php echo $header; ?>
    <div class="head">
        <h3>Products of seller '<?=$username[0]->name;?> '</h3>
        <p>Manage your Seller accounts</p>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered" id="datatable-editable">
            <thead>
            <tr class="bg-blue">
                <th>Sr. No.</th>
                <th>Product Name</th>
                <th>Market Price</th>
                <th>Sale Price</th>
                <th>Minimum quantity </th>
                <th>Active</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sr = 1;
            echo $notices;
            foreach ($products as $customer){
                $st = $customer->status;
                echo'<tr>
            <td>'.$sr.'</td>
            <td>'.$customer->title.'</td>
            <td>'.$customer->price.'</td>
            <td>'.$customer->sale.'</td>
            <td>'.$customer->min_qty.'</td>
            <td>'.$customer->status.'</td>
            <td>';
                   echo '<a href="myproduct?id='.$username[0]->id.'&reject='.$customer->id.'" class="btn btn-xs btn-danger">Reject</a>';
                    echo '<a href="myproduct?id='.$username[0]->id.'&approve='.$customer->id.'" class="btn btn-xs btn-success">Approve</a>';
                echo '</td>
          </tr>';

                $sr++; }?>
            </tbody>
        </table>
    </div>
<?php
echo $footer;
?>