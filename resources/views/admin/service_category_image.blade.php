<?php
echo $header;
echo '<div class="row" style="margin-top: 13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 650px;">
            <form action="service_category_image?edit='.$_GET['edit'].'" method="post" class="form-horizontal" enctype="multipart/form-data" style="max-width: 100%;">
			'.csrf_field().'
			<h5><a href="services_categories"><i class="icon-arrow-left"></i></a>Update Questions for <b>'.getServiceCategoryName($_GET['edit']).'</b></h5>
				<div class="row">
					  <div class="form-group col-md-12" style="padding-left: 30px;padding-right: 30px;">
							<label class="control-label">Select Design Category</label>
							<select name="gallery_category_id" onChange="getImage(this.value);" class="form-control select2" required="required">
                                <option></option>';
                                foreach ($category as $question){
                                    echo '<option value="'.$question->id.'">'.$question->name.'</option>';
                                }
                            echo '</select>
					  </div>
					  <div class="form-group col-md-12" style="padding-left: 30px;">
						<div class="col-md-2">
                            <select name="image_1" id="imageid1" class="form-control" required="required">
                                <option value="">Please Select Category First</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select name="image_2" id="imageid2" class="form-control" required="required">
                                <option value="">Please Select Category First</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select name="image_3" id="imageid3" class="form-control" required="required">
                                <option value="">Please Select Category First</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select name="image_4" id="imageid4" class="form-control" required="required">
                                <option value="">Please Select Category First</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select name="image_5" id="imageid5" class="form-control" required="required">
                                <option value="">Please Select Category First</option>
                            </select>
                        </div>
					  </div>
					  
					  <input name="update" type="submit" value="Add Image" class="btn btn-primary col-md-2" style="padding-left: 30px;padding-right: 30px;"/>
				</div>
			</form>
			';

?>

<br><br>
<div class="table-responsive">
    <table class="table table-striped table-bordered" id="datatable-editable">
        <thead>
        <tr class="bg-blue">
            <th>Sr. No.</th>
            <th>Design Category</th>
            <th>Image 1</th>
            <th>Image 2</th>
            <th>Image 3</th>
            <th>Image 4</th>
            <th>Image 5</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $sr = 1;
        echo $notices;
        foreach ($gallery as $que){
                $desi = DB::select('SELECT * FROM design_category WHERE id = "'.$que->gallery_category_id.'"')[0];
                $pic1 = DB::select('SELECT * FROM photo_gallery WHERE id = "'.$que->image_1.'"')[0];
                $pic2 = DB::select('SELECT * FROM photo_gallery WHERE id = "'.$que->image_2.'"')[0];
                $pic3 = DB::select('SELECT * FROM photo_gallery WHERE id = "'.$que->image_3.'"')[0];
                $pic4 = DB::select('SELECT * FROM photo_gallery WHERE id = "'.$que->image_4.'"')[0];
                $pic5 = DB::select('SELECT * FROM photo_gallery WHERE id = "'.$que->image_5.'"')[0];
            echo'<tr>
                    <td>'.$sr.'</td>
                    <td>'.$desi->name.'</td>
                    <td>'.$pic1->title.'</td>
                    <td>'.$pic2->title.'</td>
                    <td>'.$pic3->title.'</td>
                    <td>'.$pic4->title.'</td>
                    <td>'.$pic5->title.'</td>
                    <td>
                        <a href="service_category_image?delete='.$_GET['edit'].'&delete='.$que->id.'"><i class="icon-trash"></i></a>
                    </td>
                  </tr>
							 ';
            $sr++;}

        ?>
        </tbody>
    </table>
</div>
</div>
</div>
</div>
</div>
<style>
    .button {
        background: gainsboro;
        padding: 5px 20px;
        cursor: pointer;
        border-radius: 30px;
    }
    .mini-button {
        cursor: pointer;
        border-radius: 30px;
        background: transparent;
        padding: 5px 0px;
        display: block;
    }
</style>

<?php
echo $footer;
?>
<script>
    $( document ).ready(function() {
        $('.select2').select2({
            placeholder: "Please Select Category...",
            allowClear: true
        });
    });

    function getImage(val) {
        //alert(val);
        $.ajax({
            type: "POST",
            url: "get_image",
            data:'_token=<?=csrf_token();?>&category_id='+val,
            success: function(data){
                //alert(data);
                $("#imageid1").html(data);
                $("#imageid2").html(data);
                $("#imageid3").html(data);
                $("#imageid4").html(data);
                $("#imageid5").html(data);
            }
        });
    }

</script>