<?php echo $header?>
    <div class="profile-root bg-white ng-scope">
        <div class="clearfix buffer-top text-center wrapper-1400 bg-color">
            <div class="clearfix wrapper-1140 two-bgs">
                <div class="col-sm-12">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/banners/0e94f355317086ae791f78e1e851d4fb.jpg" class="cover">
                    </div>
                </div>
            </div>
            <div class="profile-overlay clearfix wrapper-980 bgs-white ng-isolate-scope" style="width: 980px;">
                <img itemprop="image" class="profile-icon" src="assets/invoice.png">
                <h1 itemprop="name" class="name hand-writing font-44 font-36-xs">
                    <a itemprop="url" class="url">
                        <?=translate('Invoice')?>
                    </a>
                </h1>
            </div>
        </div>
        <div class="clearfix wrapper-980 bg-white m-t-xs m-b-md">
            <div class="about-designer clearfix">
                <div class="text-left" style="padding-top: 50px;">
                    <div class="sdb-tabl-com sdb-pro-table">
                        <div class="udb-sec udb-prof">
                            <div class="col-md-8">
                                <div class="list">
                                    <div class="title">
                                        <i class="icon-user"></i> Customer details
                                    </div>
                                    <div class="item"><h6><b>Order ID</b> : #<?=$order->id?></h6></div>
                                    <div class="item"><h6><b>Order time</b> : <?=date('l jS \of F Y H:i:s',$order->time)?></h6></div>
                                    <?php foreach($fields as $field){
                                        $code = $field->code;
                                        if ($code == 'country') {$order->$code = country($order->$code);}
                                        if($code != 'address') {
                                            echo '<div class="item order"><h6><b>' . $field->name . '</b> : ' . $order->$code . '</h6></div>';
                                        }else{
                                            $data = json_decode($order->$code);
                                            echo '<div class="item order"><h6><b>Shipping Address</b> : </h6>
                                        <p>';
                                            echo '<b>Name : </b> '.$data->shipping_fname.'<br>';
                                            echo '<b>Company : </b> '.$data->shipping_company.'<br>';
                                            echo '<b>Address Line 1 : </b> '.$data->shipping_address_line_1.'<br>';
                                            echo '<b>Address Line 2 : </b> '.$data->shipping_address_line_2.'<br>';
                                            echo '<b>Country : </b> '.getCountryName($data->shipping_country).'<br>';
                                            echo '<b>State : </b> '.getStateName($data->shipping_state).'<br>';
                                            echo '<b>City : </b> '.getCityName($data->shipping_city).'<br>';
                                            echo '<b>Pincode : </b> '.$data->shipping_postcode.'<br>';
                                            echo '<b>Contact No. : </b> '.$data->shipping_mobile.'<br>';
                                            echo '<b>Alternate Contact No. : </b> '.$data->shipping_alternate_contact.'<br>';
                                            echo '</p>
                                        </div>';
                                        } echo   '<br/>';
                                    }?>
                                </div>
                                <div class="list">
                                    <div class="title">
                                        <i class="icon-basket"></i> Products
                                    </div>
                                    <?php $products_data = $order->products;
                                    $products = json_decode($products_data, true);
                                    if(count($products)>0){
                                        $ids = "";
                                        foreach($products as $data){
                                            $ids = $ids . $data['id'] . ",";
                                            $q[$data['id']] = $data['quantity'];
                                            $options_list[$data['id']] = $data['options'];
                                        }
                                        $ids = rtrim($ids, ',');
                                        $order_products = DB::select("SELECT * FROM products WHERE id IN (".$ids.") ORDER  BY id DESC ");
                                        foreach ($order_products as $product){
                                            $options = json_decode($options_list[$product->id],true);
                                            $option_array = array();
                                            if(count($options) > 0) {
                                                foreach ($options as $option) {
                                                    $option_array[] = '<i>' . $option['title'] . '</i> : ' . $option['value'];
                                                }
                                            }
                                            echo '<div class="item"><h6>'.$product->title.' x '.$q[$product->id].'<b class="pull-right">'.c($product->price).'</b><br>'.implode('<br/>',$option_array).'</div>';
                                        }

                                        if (!empty($order->coupon)) {
                                            // Check if coupon is valid
                                            if (DB::select("SELECT COUNT(code) as count FROM coupons WHERE code = '".$order->coupon."'")[0]->count > 0){
                                                $coupon_data = DB::select("SELECT * FROM coupons WHERE code = '".$order->coupon."'")[0];
                                                echo '<div class="item text-right"> Coupon : <b>'.$coupon_data->discount.$coupon_data->type.'</b></div>';
                                            }
                                        }
                                        if ($order->shipping != 0) {
                                            // Check if coupon is valid
                                            echo '<div class="item text-right"> Shipping : <b>'.c($order->shipping).'</b></div>';
                                        }
                                        echo '<div class="item text-right">Total : <b>'.c($order->summ).'</b></div>';
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="list">
                                    <div class="title">
                                        <i class="icon-credit-card"></i> Payment details
                                    </div>
                                    <?php $payment_data = json_decode($order->payment,true);
                                    $payment_data = json_decode($order->payment,true);
                                    echo '<div class="alert alert-warning" style="margin-bottom: 0;">Order unpaid</div>';
                                    if ($payment_data['method'] == 'paypal') {
                                        echo '<div class="item order"><h6><b>Payment method : </b>PayPal</h6></div>
                                        <div class="item order"><h6><b>Payment status : </b>'.$payment_data['payment_status'].'</h6></div>
                                        <div class="item order"><h6><b>Transcation ID : </b>'.$payment_data['txn_id'].'</h6></div>
                                        <div class="item order"><h6><b>Receiver e-mail : </b>'.$payment_data['receiver_email'].'</h6></div>
                                        <div class="item order"><h6><b>Payer e-mail : </b>'.$payment_data['payer_email'].'</h6></div>
                                        <div class="item order"><h6><b>Payment amount : </b>'.$payment_data['payment_amount'].'</h6></div>
                                        <div class="item order"><h6><b>Payment currency : </b>'.$payment_data['payment_currency'].'</h6></div>';
                                    } elseif ($payment_data['method'] == 'stripe') {
                                        echo '<div class="item order"><h6><b>Payment method : </b>Stripe</h6></div>
                                        <div class="item order"><h6><b>Charge ID : </b>'.$payment_data['charge_id'].'</h6></div>
                                        <div class="item order"><h6><b>Balance transaction : </b>'.$payment_data['balance_transaction'].'</h6></div>
                                        <div class="item order"><h6><b>Card ID : </b>'.$payment_data['card'].'</h6></div>
                                        <div class="item order"><h6><b>Card brand : </b>'.$payment_data['card_brand'].'</h6></div>
                                        <div class="item order"><h6><b>Last 4 : </b>'.$payment_data['last4'].'</h6></div>
                                        <div class="item order"><h6><b>Expiry month : </b>'.$payment_data['exp_month'].'</h6></div>
                                        <div class="item order"><h6><b>Expiry year : </b>'.$payment_data['exp_year'].'</h6></div>
                                        <div class="item order"><h6><b>Fingerprint : </b>'.$payment_data['fingerprint'].'</h6></div>';
                                    } elseif ($payment_data['method'] == 'cash') {
                                        echo '<div class="item order"><h6><b>Payment method : </b>Cash on delivery</h6></div>';
                                    } elseif ($payment_data['method'] == 'bank') {
                                        echo '<div class="item order"><h6><b>Payment method : </b>Bank Transfer</h6></div>';
                                    }?>
                                </div>
                                <div class="list">
                                    <div class="title">
                                        <i class="icon-badge"></i>Order status
                                    </div>
                                    <div class="item text-center"><br/><h6><?php $oid = $order->id?></h6><br/>
                                        <?php $shipping_status = DB::select("SELECT * FROM seller_po WHERE order_id='$oid'");
                                        foreach ($shipping_status as $key){
                                            $po_id = $key->id;
                                            $shipping_data = json_decode($key->products , true);
                                            ?>
                                            <table class="table-bordered table-responsive" width="100%">
                                                <thead>
                                                <th>Sr.</th>
                                                <th>Title</th>

                                                </thead>
                                                <tbody>
                                                <?php
                                                $sr =1;
                                                foreach ($shipping_data as $val) {
                                                    // print_r($val['title']);
                                                    ?>
                                                    <tr>
                                                        <td><?=$sr;?></td>
                                                        <td><?=$val['title'];?></td>
                                                    </tr>
                                                    <?php $sr++;
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                            <table class="table-bordered table-responsive" width="100%">
                                                <thead>
                                                <th>Status</th>
                                                <th>Date</th>
                                                </thead>
                                                <tbody>
                                                <?php
                                                $s_status =  DB::select("SELECT * FROM `shipping_status` WHERE po_id ='$po_id'");
                                                foreach ($s_status as $val2) {
                                                    // print_r($val['title']);
                                                    ?>
                                                    <tr>
                                                        <td><?=$val2->status;?></td>
                                                        <td><?=$val2->update_date;?></td>
                                                    </tr>
                                                    <?php
                                                }
                                                if(empty($s_status)){
                                                    ?>
                                                    <tr>
                                                        <td colspan="2">Pending</td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                                </tbody>
                                            </table><br>
                                            <?php

                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php echo $footer?>