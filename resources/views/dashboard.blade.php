<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>Party Panel | Kamal Textile Agency, Raipur</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
        <meta content="Themesbrand" name="author" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- Bootstrap Css -->
        <link href="assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
        <!-- Icons Css -->
        <link href="assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <!-- App Css-->
        <link href="assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />

    </head>

    <body data-layout="horizontal">

        <!-- Loader -->
        <div id="preloader"><div id="status"><div class="spinner"></div></div></div>

        <!-- Begin page -->
        <div id="layout-wrapper">

            <header id="page-topbar">
                <div class="navbar-header">
                    <div class="d-flex">
                        <!-- LOGO -->
                        <div class="navbar-brand-box">
                            <a href="index.html" class="logo logo-dark">
                                <span class="logo-sm">
                                        <img src="assets/images/logo-sm.png" alt="" height="22">
                                    </span>
                                <span class="logo-lg">
                                        <img src="assets/images/logo-dark.png" alt="" height="20">
                                    </span>
                            </a>

                            <a href="index.html" class="logo logo-light">
                                <span class="logo-sm">
                                        <img src="assets/images/logo-sm.png" alt="" height="22">
                                    </span>
                                <span class="logo-lg">
                                        <img src="assets/images/logo-light.png" alt="" height="20">
                                    </span>
                            </a>
                        </div>

                        <button type="button" class="btn btn-sm mr-2 font-size-24 d-lg-none header-item waves-effect waves-light" data-toggle="collapse" data-target="#topnav-menu-content">
                            <i class="mdi mdi-menu"></i>
                        </button>

                    </div>

                    <div class="d-flex">
                        <div class="dropdown d-none d-lg-inline-block">
                            <button type="button" class="btn header-item noti-icon waves-effect" data-toggle="fullscreen">
                                <i class="mdi mdi-fullscreen"></i>
                            </button>
                        </div>

                        <div class="dropdown d-inline-block ml-2">
                            <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="rounded-circle header-profile-user" src="assets/images/users/avatar-1.jpg" alt="Header Avatar">
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <!-- item-->
                                <a class="dropdown-item" href="#"><i class="dripicons-user font-size-16 align-middle mr-2"></i> Profile</a>
                                <a class="dropdown-item" href="#"><i class="dripicons-wallet font-size-16 align-middle mr-2"></i> My Wallet</a>
                                <a class="dropdown-item d-block" href="#"><span class="badge badge-success float-right">5</span><i class="dripicons-gear font-size-16 align-middle mr-2"></i> 
                                </a>
                                <a class="dropdown-item" href="#"><i class="dripicons-lock font-size-16 align-middle mr-2"></i> Lock screen</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#"><i class="dripicons-exit font-size-16 align-middle mr-2"></i> Logout</a>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="topnav">
                    <div class="container-fluid">
                        <nav class="navbar navbar-light navbar-expand-lg topnav-menu">

                            <div class="collapse navbar-collapse" id="topnav-menu-content">
                                <ul class="navbar-nav">
                                    <li class="nav-item">
                                        <a class="nav-link" href="index.html">
                                            <i class="dripicons-device-desktop mr-2"></i>Dashboard
                                        </a>
                                    </li>

                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle arrow-none" href="select-indent-report.html" id="topnav-user-Interface" role="button" aria-haspopup="true" aria-expanded="false">
                                            <i class="dripicons-suitcase mr-2"></i>Indent Report
                                        </a>
                                    </li>

                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle arrow-none" href="select-bill-report.html" id="topnav-user-Interface" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="dripicons-inbox mr-2"></i>Billing Report
                                        </a>
                                    </li>

                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-pages" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="dripicons-copy mdi-drop mr-2"></i>Other Reports
                                            <div class="arrow-down"></div>
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="topnav-layout">
                                            <a href="#" class="dropdown-item">Product Report</a>
                                            <a href="#" class="dropdown-item">Sale Report</a>
                                            <a href="#" class="dropdown-item">Payment Report</a>
                                            <a href="#" class="dropdown-item">DTA Mumbai Report</a>
                                        </div>
                                    </li>

                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-layout" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="dripicons-toggles mr-2"></i>Settings & Preferences
                                            <div class="arrow-down"></div>
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="topnav-layout">
                                            <a href="#" class="dropdown-item">Change Password</a>
                                            <a href="#" class="dropdown-item">Notification Preferences</a>
                                            <a href="#" class="dropdown-item">Transport Preferences</a>
                                            <a href="#" class="dropdown-item">Agency Contact Details</a>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>

                <!-- start page title -->
                <div class="page-title-box">
                    <div class="container-fluid">
                        <div class="page-title-content">
                            <div class="row align-items-center">
                                <div class="col-sm-6">
                                    <h4>Dashboard</h4>
                                </div>

                                <div class="col-sm-6">
                                    <div class="float-right d-none d-md-block">
                                        <form class="app-search ">
                                            <input type="text" class="form-control" placeholder="Search...">
                                            <span class="fa fa-search"></span>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

            </header>

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">
                <div class="page-content">
                    

                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-md-6 col-xl-3">
                                <div class="card text-center">
                                    <div class="mb-2 card-body text-muted">
                                        <h3 class="text-info mt-2">Indent Report</h3> View Indent & Dispatch Status
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-3">
                                <div class="card text-center">
                                    <div class="mb-2 card-body text-muted">
                                        <h3 class="text-purple mt-2">Billing Report</h3> View Billing & Outstanding Status
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-3">
                                <div class="card text-center">
                                    <div class="mb-2 card-body text-muted">
                                        <h3 class="text-primary mt-2">Indent Request</h3> (Feature Under Developement)
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-3">
                                <div class="card text-center">
                                    <div class="mb-2 card-body text-muted">
                                        <h3 class="text-danger mt-2">Payment Update</h3> (Feature Under Developement)
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <div class="row">
                            <div class="col-xl-8">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title mb-4">Recent Pending Indents</h4>

                                        <div class="table-responsive">
                                            <table class="table mt-0 mb-0 table-centered table-nowrap">

                                                <tbody>
                                                    <tr>
                                                        <td><b>Company</b></td>
                                                        <td><b>In. No.</b></td>
                                                        <td><b>In. Dt.</b></td>
                                                        <td><b>Product</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Padamshree Mfg. & Wvg, Pvt. Ltd.</td>
                                                        <td>I-001</td>
                                                        <td>01/04/2021</td>
                                                        <td>Aaina</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title mb-4">Recent Pending Bills</h4>

                                        <div class="table-responsive">
                                            <table class="table mt-0 mb-0 table-centered table-nowrap">
                                                <tbody>
                                                    <tr>
                                                        <td><b>Company</b></td>
                                                        <td><b>Bill. No.</b></td>
                                                        <td><b>Bill. Dt.</b></td>
                                                        <td><b>Amount</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Padamshree Mfg. & Wvg, Pvt. Ltd.</td>
                                                        <td>B-001</td>
                                                        <td>01/04/2021</td>
                                                        <td>25000</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-4">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title mb-3">Recent Dispatch Updates</h4>

                                        <ol class="activity-feed mb-0">
                                            <li class="feed-item">
                                                <span class="date">Sep 25</span>
                                                <span class="activity-text">Responded to need “Volunteer Activities”</span>
                                            </li>

                                            <li class="feed-item">
                                                <span class="date">Sep 24</span>
                                                <span class="activity-text">Added an interest “Volunteer Activities”</span>
                                            </li>
                                            <li class="feed-item">
                                                <span class="date">Sep 23</span>
                                                <span class="activity-text">Joined the group “Boardsmanship Forum”</span>
                                            </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- end row -->

                    </div>
                    <!-- container-fluid -->
                </div>
                <!-- End Page-content -->

                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                © 2022 - Kamal Textile Agency, Raipur | S.J. Group, Raipur
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
            <!-- end main content-->

        </div>
        <!-- END layout-wrapper -->
        
        <!-- JAVASCRIPT -->
        <script src="assets/libs/jquery/jquery.min.js"></script>
        <script src="assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/libs/metismenu/metisMenu.min.js"></script>
        <script src="assets/libs/simplebar/simplebar.min.js"></script>
        <script src="assets/libs/node-waves/waves.min.js"></script>

        <!--Morris Chart-->
        <script src="assets/libs/morris.js/morris.min.js"></script>
        <script src="assets/libs/raphael/raphael.min.js"></script>

        <script src="assets/js/pages/dashboard.init.js"></script>

        <script src="assets/js/app.js"></script>

    </body>

</html>