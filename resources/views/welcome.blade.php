<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="author" content="Aakar360 Mentors Pvt. Ltd."/>
    <meta name="description" content=""/>
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- SITE TITLE -->
    <title>Aakar360 Mentors Pvt. Ltd.</title>
    <!-- FAVICON AND TOUCH ICONS -->
    <link rel="shortcut icon" href="{{ url('/main/images/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ url('/main/images/favicon.ico') }}" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ url('/main/images/apple-touch-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ url('/main/images/apple-touch-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ url('/main/images/apple-touch-icon-76x76.png') }}">
    <link rel="apple-touch-icon" href="{{ url('/main/images/apple-touch-icon.png') }}">
    <link rel="icon" href="{{ url('/main/images/apple-touch-icon.png') }}" type="image/x-icon">
    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;700;800;900&amp;display=swap" rel="stylesheet">
    <!-- BOOTSTRAP CSS -->
    <link href="{{ url('/main/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- FONT ICONS -->
    <link href="{{ url('/main/css/flaticon.css') }}" rel="stylesheet">
    <!-- PLUGINS STYLESHEET -->
    <link href="{{ url('/main/css/menu.css') }}" rel="stylesheet">
    <link id="effect" href="{{ url('/main/css/dropdown-effects/fade-down.css') }}" media="all" rel="stylesheet">
    <link href="{{ url('/main/css/magnific-popup.css') }}" rel="stylesheet">
    <link href="{{ url('/main/css/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ url('/main/css/flexslider.css') }}" rel="stylesheet">
    <link href="{{ url('/main/css/slick.css') }}" rel="stylesheet">
    <link href="{{ url('/main/css/slick-theme.css') }}" rel="stylesheet">
    <link href="{{ url('/main/css/owl.theme.default.min.css') }}" rel="stylesheet">
    <!-- TEMPLATE CSS -->
    <link href="{{ url('/main/css/yellow-theme.css') }}" rel="stylesheet">
    <!-- Style Switcher CSS -->
    <link href="{{ url('/main/css/azure-theme.css') }}" rel="alternate stylesheet" title="azure-theme">
    <link href="{{ url('/main/css/blue-theme.css') }}" rel="alternate stylesheet" title="blue-theme">
    <link href="{{ url('/main/css/brown-theme.css') }}" rel="alternate stylesheet" title="brown-theme">
    <link href="{{ url('/main/css/dimgreen-theme.css') }}" rel="alternate stylesheet" title="dimgreen-theme">
    <link href="{{ url('/main/css/olive-theme.css') }}" rel="alternate stylesheet" title="olive-theme">
    <link href="{{ url('/main/css/orange-theme.css') }}" rel="alternate stylesheet" title="orange-theme">
    <link href="{{ url('/main/css/purple-theme.css') }}" rel="alternate stylesheet" title="purple-theme">
    <link href="{{ url('/main/css/red-theme.css') }}" rel="alternate stylesheet" title="red-theme">
    <link href="{{ url('/main/css/rose-theme.css') }}" rel="alternate stylesheet" title="rose-theme">
    <link href="{{ url('/main/css/royalblue-theme.css') }}" rel="alternate stylesheet" title="royalblue-theme">
    <link href="{{ url('/main/css/skyblue-theme.css') }}" rel="alternate stylesheet" title="skyblue-theme">
    <link href="{{ url('/main/css/violet-theme.css') }}" rel="alternate stylesheet" title="violet-theme">
    <link href="{{ url('/main/css/yellow-theme.css') }}" rel="alternate stylesheet" title="yellow-theme">
    <!-- ON SCROLL ANIMATION -->
    <link href="{{ url('/main/css/animate.css') }}" rel="stylesheet">
    <!-- RESPONSIVE CSS -->
    <link href="{{ url('/main/css/responsive.css') }}" rel="stylesheet">
</head>
<body>
<!-- PRELOADER SPINNER
============================================= -->
<div id="loader-wrapper"><div id="loader"></div></div>
<!-- PAGE CONTENT
============================================= -->
<div id="page" class="page">
    <!-- HEADER
    ============================================= -->
    <header id="header" class="header tra-menu navbar-light">
        <div class="header-wrapper">
            <!-- MOBILE HEADER -->
            <div class="wsmobileheader clearfix">
                <span class="smllogo"><img src="{{ url('/main/images/logo.png') }}" alt="mobile-logo"/></span>
                <a id="wsnavtoggle" class="wsanimated-arrow"><span></span></a>
            </div>
            <!-- NAVIGATION MENU -->
            <div class="wsmainfull menu clearfix">
                <div class="wsmainwp clearfix">
                    <!-- HEADER LOGO -->
                    <div class="desktoplogo"><a href="#hero-19" class="logo-black"><img src="{{ url('/main/images/logo.png') }}" alt="header-logo"></a></div>
                    <div class="desktoplogo"><a href="#hero-19" class="logo-white"><img src="{{ url('/main/images/logo.png') }}" alt="header-logo"></a></div>
                    <!-- MAIN MENU
                      <nav class="wsmenu clearfix">
                        <ul class="wsmenu-list nav-theme-hover">
                              <li aria-haspopup="true"><a href="#">About <span class="wsarrow"></span></a>
                                <ul class="sub-menu">
                                    <li aria-haspopup="true"><a href="#content-2">About PowerNode</a></li>
                                    <li aria-haspopup="true"><a href="#features-4">Why Choose Us</a></li>
                                    <li aria-haspopup="true"><a href="#content-12">Best Solutions</a></li>
                                    <li aria-haspopup="true"><a href="#features-5">Core Services</a></li>
                                    <li aria-haspopup="true"><a href="#features-10">Best Approach</a></li>
                                       <li aria-haspopup="true"><a href="#reviews-1">Testimonials</a></li>
                                   </ul>
                            </li>
                            <li class="nl-simple" aria-haspopup="true"><a href="#gallery-3">Projects</a></li>
                              <li aria-haspopup="true"><a href="#">Pages <span class="wsarrow"></span></a>
                                <div class="wsmegamenu clearfix">
                                     <div class="container">
                                           <div class="row">
                                               <ul class="col-lg-3 col-md-12 col-xs-12 link-list">
                                                <li><a href="about.html">About Us Page</a></li>
                                                <li><a href="services-1.html">Our Services v1</a></li>
                                                <li><a href="services-2.html">Our Services v2</a></li>
                                                <li><a href="service-details-design.html">Service Details #1</a></li>
                                                <li><a href="service-details.html">Service Details #2</a></li>
                                            </ul>
                                               <ul class="col-lg-3 col-md-12 col-xs-12 link-list">
                                                   <li><a href="service-details-constr.html">Service Details #3</a></li>
                                                <li><a href="service-details-handyman.html">Service Details #4</a></li>
                                                <li><a href="gallery-1.html">Our Projects v1</a></li>
                                                <li><a href="gallery-2.html">Our Projects v2</a></li>
                                                <li><a href="project-details-1.html">Project Details v1</a></li>
                                            </ul>
                                               <ul class="col-lg-3 col-md-12 col-xs-12 link-list">
                                                <li><a href="project-details-2.html">Project Details v2</a></li>
                                                <li><a href="team.html">Meet The Team</a></li>
                                                <li><a href="pricing.html">Pricing Packages</a></li>
                                                <li><a href="customers.html">Our Customers</a></li>
                                                <li><a href="faqs.html">FAQs Page</a></li>
                                            </ul>
                                               <ul class="col-lg-3 col-md-12 col-xs-12 link-list">
                                                <li><a href="blog-listing-1.html">Blog Listing Sidebar</a></li>
                                                <li><a href="blog-listing-2.html">Blog Listing Modern</a></li>
                                                <li><a href="single-post.html">Single Blog Post</a></li>
                                                <li><a href="contacts.html">Contact Us</a></li>
                                                <li><a href="terms.html">Terms & Privacy</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="nl-simple" aria-haspopup="true"><a href="#blog-1">Blog</a></li>
                            <li class="nl-simple" aria-haspopup="true">
                                <a href="#contacts-1" class="btn btn-theme tra-white-hover last-link">Contacts</a>
                            </li>
                        </ul>
                    </nav>	<!-- END MAIN MENU -->
                </div>
            </div>	<!-- END NAVIGATION MENU -->
        </div>     <!-- End header-wrapper -->
    </header>	<!-- END HEADER -->
    <!-- HERO-19
    ============================================= -->
    <section id="hero-19" class="bg-fixed hero-section division" style="height: 550px;">
        <div class="container">
            <div class="row">
                <!-- HERO TEXT -->
                <div class="col">
                    <div class="hero-19-txt white-color text-center">
                        <!-- Title -->
                        <h2 class="h2-xl">Transforming construction through technology and innovation</h2>
                        <!-- Text
                        <p class="p-xl txt-400">Egestas magna egestas magna ipsum vitae purus ipsum primis in cubilia laoreet augue
                           luctus magna dolor luctus undo magna an dolor integer congue magna pretium
                        </p>
                        <!-- Button
                        <a href="#gallery-3" class="btn btn-md btn-theme tra-white-hover">Our Lates Projects</a-->
                    </div>
                </div>	<!-- END HERO TEXT -->
            </div>	   <!-- End row -->
        </div> 	   <!-- End container -->
    </section>	<!-- END HERO-19 -->
    <!-- CONTENT-2
    ============================================= -->
    <section id="content-2" class="wide-60 content-section division">
        <div class="container">
            <div class="row d-flex align-items-center">
                <!-- IMAGE BLOCK -->
                <div class="col-md-5 col-lg-4">
                    <div class="img-block left-column pc-20 mb-40 wow fadeInRight" data-wow-delay="0.6s">
                        <img class="img-fluid" src="{{ url('/main/images/retailblue.png') }}" alt="content-image">
                    </div>
                </div>
                <!-- TEXT BLOCK -->
                <div class="col-md-7 col-lg-8">
                    <div class="txt-block right-column pc-30 mb-40 wow fadeInLeft" data-wow-delay="0.4s">
                        <!-- Title -->
                        <h3 class="h3-sm">Retail</h3>
                        <!-- Text -->
                        <p class="p-md grey-color">Building your project! Buy construction material online or at our offline extension stores. Here you can buy different brands at the cheapest price.
                        </p>
                        <!--  Button -->
                        <a href="{{ route('retail.retailIndex') }}" target="_blank" class="btn btn-md btn-tra-grey theme-hover">Shop Now</a>
                    </div>
                </div>	<!-- END TEXT BLOCK -->
            </div>	   <!-- End row -->
        </div>	   <!-- End container -->
    </section>	<!-- END CONTENT-2 -->
    <section id="content-2" class="wide-60 content-section division">
        <div class="container">
            <div class="row d-flex align-items-center">
                <!-- TEXT BLOCK -->
                <div class="col-md-7 col-lg-8">
                    <div class="txt-block right-column pc-30 mb-40 wow fadeInLeft" data-wow-delay="0.4s">
                        <!-- Title -->
                        <h3 class="h3-sm">Business</h3>
                        <!-- Text -->
                        <p class="p-md grey-color">We provide you wide range of construction material across the construction life cycle at unbeatable price point.
                        </p>
                        <!--  Button -->
                        <a href="{{ route('business.institutionalStatic') }}" target="_blank" class="btn btn-md btn-tra-grey theme-hover">View Detail</a>
                    </div>
                </div>	<!-- END TEXT BLOCK -->
                <!-- IMAGE BLOCK -->
                <div class="col-md-5 col-lg-4">
                    <div class="img-block left-column pc-20 mb-40 wow fadeInRight" data-wow-delay="0.6s">
                        <img class="img-fluid" src="{{ url('/main/images/retailblue.png') }}" alt="content-image">
                    </div>
                </div>
            </div>	   <!-- End row -->
        </div>	   <!-- End container -->
    </section>	<!-- END CONTENT-2 -->
    <section id="content-2" class="wide-60 content-section division">
        <div class="container">
            <div class="row d-flex align-items-center">
                <!-- IMAGE BLOCK -->
                <div class="col-md-5 col-lg-4">
                    <div class="img-block left-column pc-20 mb-40 wow fadeInRight" data-wow-delay="0.6s">
                        <img class="img-fluid" src="{{ url('/main/images/retailblue.png') }}" alt="content-image">
                    </div>
                </div>
                <!-- TEXT BLOCK -->
                <div class="col-md-7 col-lg-8">
                    <div class="txt-block right-column pc-30 mb-40 wow fadeInLeft" data-wow-delay="0.4s">
                        <!-- Title -->
                        <h3 class="h3-sm">Project Management</h3>
                        <!-- Text -->
                        <p class="p-md grey-color">Construction management software for small to large projects, which helps you finish quality projects on time and within budget.
                        </p>
                        <!--  Button -->
                        <a href="https://pms.aakar360.com/" target="_blank" class="btn btn-md btn-tra-grey theme-hover">View Detail</a>
                    </div>
                </div>	<!-- END TEXT BLOCK -->
            </div>	   <!-- End row -->
        </div>	   <!-- End container -->
    </section>	<!-- END CONTENT-2 -->
    <!-- CONTENT-12
    =============================================
    <section id="content-12" class="pt-100 content-section division">
         <div class="container">
             <div class="row">
                <div class="col-md-12">
                    <div class="img-block rel text-center">
                        <img class="img-fluid" src="images/collage-construction.png" alt="content-image">
                        <div class="content-12-txt bg-01 white-color">
                            <h4 class="h4-sm">Our commitment, your convenience</h4>
                            <p class="p-md">Aliqum  mullam blandit and tempor sapien gravida donec ipsum porta justo. Velna and vitae auctor
                                and congue magna impedit at ligula risus mauris donec ociis magnis
                            </p>
                        </div>
                    </div>
                </div>
            </div>
         </div>
    </section>
    <section id="content-12" class="pt-100 content-section division">
         <div class="container">
             <div class="row">
                <div class="col-md-12">
                    <div class="img-block rel text-center">
                        <img class="img-fluid" src="images/collage-construction.png" alt="content-image">
                        <div class="content-12-txt bg-01 white-color">
                            <h4 class="h4-sm">Our commitment, your convenience</h4>
                            <p class="p-md">Aliqum  mullam blandit and tempor sapien gravida donec ipsum porta justo. Velna and vitae auctor
                                and congue magna impedit at ligula risus mauris donec ociis magnis
                            </p>
                        </div>
                    </div>
                </div>
            </div>
         </div>
    </section>
    <section id="content-12" class="pt-100 content-section division">
         <div class="container">
             <div class="row">
                <div class="col-md-12">
                    <div class="img-block rel text-center">
                        <img class="img-fluid" src="images/collage-construction.png" alt="content-image">
                        <div class="content-12-txt bg-01 white-color">
                            <h4 class="h4-sm">Our commitment, your convenience</h4>
                            <p class="p-md">Aliqum  mullam blandit and tempor sapien gravida donec ipsum porta justo. Velna and vitae auctor
                                and congue magna impedit at ligula risus mauris donec ociis magnis
                            </p>

                        </div>
                    </div>
                </div>
            </div>
         </div>
    </section>	 <!-- END CONTENT-12 -->
    <!-- CONTACTS-1
    =============================================
    <section id="contacts-1" class="wide-30 contacts-clouds contacts-section division">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <div class="section-title text-center mb-60">
                        <h2 class="h2-xs">Got An Idea? Get In Touch</h2>
                        <p class="p-xl">Aliquam a augue suscipit, luctus neque purus ipsum neque at dolor primis libero
                           tempus, blandit and cursus varius magna tempus a dolor
                        </p>
                    </div>
                </div>
            </div>
             <div class="row">
                <div class="col-sm-6 col-lg-3">
                    <div class="contact-1-box ico-70">
                        <span class="theme-color flaticon-pins"></span>
                        <h5 class="h5-xs">Our Location</h5>
                        <p class="grey-color">121 King Street, Melbourne, <br /> Victoria 3000 Australia</p>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="contact-1-box ico-70">
                        <span class="theme-color flaticon-smartphone"></span>
                        <h5 class="h5-xs">Let's Talk</h5>
                        <p class="grey-color">Phone : +12 3 3456 7890</p>
                        <p class="grey-color">Fax : +12 9 8765 4321</p>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="contact-1-box ico-70">
                        <span class="theme-color flaticon-email-2"></span>
                        <h5 class="h5-xs">Drop a Line</h5>
                        <p class="grey-color"><a href="mailto:yourdomain@mail.com">hello@domain.com</a></p>
                        <p class="grey-color"><a href="mailto:career@mail.com">career@domain.com</a></p>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="contact-1-box ico-70">
                        <span class="theme-color flaticon-alarm-clock"></span>
                        <h5 class="h5-xs">Working Hours</h5>
                        <p class="grey-color">Mon - Fri: 8:30am - 7:30pm</p>
                        <p class="grey-color">Sat: 8:30am - 3:30pm</p>
                    </div>
                </div>
             </div>
        </div>
    </section>	<!-- END CONTACTS-1 -->
    <!-- GOOGLE MAP
    =============================================
     <div id="gmap">
        <div class="google-map">
             <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3151.838357620288!2d144.95358331497258!3d-37.81725497975171!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad65d4dd5a05d97%3A0x3e64f855a564844d!2zMTIxIEtpbmcgU3QsIE1lbGJvdXJuZSBWSUMgMzAwMCwg0JDQstGB0YLRgNCw0LvQuNGP!5e0!3m2!1sru!2sua!4v1605805230806!5m2!1sru!2sua" width="600" height="450" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
         </div>
     </div>	<!-- END GOOGLE MAP -->
    <!-- FOOTER-1
   ============================================= -->
    <footer id="footer-1" class="footer division">
        <div class="container">
            <!--div class="row">
                <div class="col-md-5 col-lg-4">
                    <div class="footer-info mb-40">
                        <div class="footer-logo"><img src="images/logo-01.png" alt="footer-logo"/></div>
                        <p>Aliquam nullam tempor sapien and gravida donec congue ipsum porta justo undo velna auctor magna enim
                           laoreet augue porta
                        </p>
                    </div>
                </div>
                <div class="col-md-4 col-lg-3">
                    <div class="footer-contacts mb-40">
                        <h6 class="h6-xl">Let's Talk</h6>
                        <p>121 King Street, Melbourne,</p>
                        <p>Victoria 3000 Australia</p>
                        <div class="mt-15">
                            <p class="foo-email">E: <a href="mailto:yourdomain@mail.com">hello@yourdomain.com</a></p>
                            <p>Phone: +12 9 8765 4321</p>
                            <p>Skype : <a href="tel:123456789">support.powernode</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-lg-2">
                    <div class="footer-links mb-40">
                        <h6 class="h6-xl">Quick Links</h6>
                        <ul class="clearfix">
                            <li><p><a href="#">About Us</a></p></li>
                            <li><p><a href="#">Advertising</a></p></li>
                            <li><p><a href="#">Help & Support</a></p></li>
                            <li><p><a href="#">Our Pricing</a></p></li>
                            <li><p><a href="#">Terms & Privacy</a></p></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-12 col-lg-3">
                    <div class="footer-img mb-40">
                        <h6 class="h6-xl">Instagram</h6>
                        <ul class="text-center clearfix">
                            <li><a href="#" target="_blank"><img class="insta-img" src="images/instagram/img-07.jpg" alt="insta-img"></a></li>
                            <li><a href="#" target="_blank"><img class="insta-img" src="images/instagram/img-08.jpg" alt="insta-img"></a></li>
                            <li><a href="#" target="_blank"><img class="insta-img" src="images/instagram/img-09.jpg" alt="insta-img"></a></li>
                            <li><a href="#" target="_blank"><img class="insta-img" src="images/instagram/img-10.jpg" alt="insta-img"></a></li>
                            <li><a href="#" target="_blank"><img class="insta-img" src="images/instagram/img-11.jpg" alt="insta-img"></a></li>
                            <li><a href="#" target="_blank"><img class="insta-img" src="images/instagram/img-12.jpg" alt="insta-img"></a></li>
                        </ul>

                    </div>
                </div>
            </div-->
            <!-- BOTTOM FOOTER -->
            <div class="bottom-footer">
                <div class="row d-flex align-items-center">
                    <!-- FOOTER COPYRIGHT -->
                    <div class="col-lg-6">
                        <div class="footer-copyright">
                            <p>&copy; 2021 Aakar360 Mentors Pvt. Ltd. All Rights Reserved</p>
                        </div>
                    </div>
                    <!-- BOTTOM FOOTER LINKS -->
                    <!--div class="col-lg-6">
                        <ul class="bottom-footer-list ico-15 text-right clearfix">
                            <li><p class="first-list-link"><a href="#"><span class="flaticon-facebook"></span> Facebook</a></p></li>
                            <li><p><a href="#"><span class="flaticon-twitter"></span> Twitter</a></p></li>
                            <li><p><a href="#"><span class="flaticon-youtube"></span> YouTube</a></p></li>
                            <li><p class="last-li"><a href="#"><span class="flaticon-yelp"></span> Yelp</a></p></li>
                        </ul>
                    </div-->
                </div>  <!-- End row -->
            </div>	<!-- END BOTTOM FOOTER -->
        </div>	   <!-- End container -->
    </footer>	<!-- END FOOTER-1 -->
</div>	<!-- END PAGE CONTENT -->
<!-- EXTERNAL SCRIPTS
============================================= -->
<script src="{{ url('/main/js/jquery-3.5.1.min.js') }}"></script>
<script src="{{ url('/main/js/bootstrap.min.js') }}"></script>
<script src="{{ url('/main/js/modernizr.custom.js') }}"></script>
<script src="{{ url('/main/js/jquery.easing.js') }}"></script>
<script src="{{ url('/main/js/jquery.appear.js') }}"></script>
<script src="{{ url('/main/js/jquery.scrollto.js') }}"></script>
<script src="{{ url('/main/js/menu.js') }}"></script>
<script src="{{ url('/main/js/materialize.js') }}"></script>
<script src="{{ url('/main/js/slick.min.js') }}"></script>
<script src="{{ url('/main/js/imagesloaded.pkgd.min.js') }}"></script>
<script src="{{ url('/main/js/isotope.pkgd.min.js') }}"></script>
<script src="{{ url('/main/js/jquery.flexslider.js') }}"></script>
<script src="{{ url('/main/js/owl.carousel.min.js') }}"></script>
<script src="{{ url('/main/js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ url('/main/js/jquery.validate.min.js') }}"></script>
<script src="{{ url('/main/js/jquery.ajaxchimp.min.js') }}"></script>
<script src="{{ url('/main/js/wow.js') }}"></script>
<!-- Custom Script -->
<script src="{{ url('/main/js/custom.js') }}"></script>
<script src="{{ url('/main/js/changer.js') }}"></script>
<script defer src="{{ url('/main/js/styleswitch.js') }}"></script>
</body>
</html>