<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class PhotoGallery extends Model
{

    protected $table = 'photo_gallery';

}
