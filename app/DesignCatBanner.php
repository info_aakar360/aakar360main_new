<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class DesignCatBanner extends Model
{

    protected $table = 'design_cat_banner';

}
