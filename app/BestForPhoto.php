<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class BestForPhoto extends Model
{

    protected $table = 'best_for_photo';

}
