<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class LayoutWishlist extends Model
{
    protected $table = 'layout_wishlist';
}
