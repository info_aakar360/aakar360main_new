<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class BestForAdvice extends Model
{

    protected $table = 'best_for_advices';

}
