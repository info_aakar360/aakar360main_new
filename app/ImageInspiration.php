<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class ImageInspiration extends Model
{
    protected $table = 'image_inspiration';
}
