<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class ManufacturingHubs extends Model
{
    protected $table = 'manufacturing_hubs';
}
