<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class CityWiseShippingCharges extends Model
{

    protected $table = 'city_wise_shipping_charges';

}
