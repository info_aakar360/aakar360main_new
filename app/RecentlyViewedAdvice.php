<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class RecentlyViewedAdvice extends Model
{
    protected $table = 'recently_viewed_advice';
}
