<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class LayoutPlanQuestion extends Model
{

    protected $table = 'layout_plan_questions';

}
