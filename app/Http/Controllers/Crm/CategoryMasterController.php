<?php

namespace App\Http\Controllers\CRM;

use App\Category;
use App\Config;
use App\CustomerCategory;
use App\CustomerCategoryPrice;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Crm;
use App\Style;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CategoryMasterController extends Crm
{
    public $cfg;
    public $style;
    private $pdf;
    public $buttons;

    public function __construct()
    {
        $this->cfg = Config::where('id', '=', 1)->first();

        $this->style = Style::first();
        $this->pdf = '';
        $this->buttons = "['copy', 'excel', 'pdf']";
        if(empty(Session::get('crm'))){
            return redirect('crm/login');
        }
    }
    public function Categorymaster(Request $request) {
        if(empty(Session::get('crm'))){
            return redirect('crm/login');
        }
        $cfg = '';
        $customer_type = '';
        $user = User::where('secure', Session::get('crm'))->first();
        if(!checkRole($user->u_id,"mas_cat")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';

        if (isset($_POST['add'])) {
            $data['type'] = $_POST['name'];
            DB::table('customer_category') -> insertGetId($data);
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Category Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
            return back();
        }

        if(isset($_POST['edit'])){
            $pro_cat = $_POST['product_category'];
            $cust_cat = $_POST['cid'];
            $record = DB::table('customer_category_price')->where('customer_category', $cust_cat)->where('product_category', $pro_cat);
            if($record->count()){
                $record->update(['price' => $_POST['price']]);
            }else{
                $data['customer_category'] = $cust_cat;
                $data['product_category'] = $pro_cat;
                $data['price'] = $_POST['price'];
                $rid = DB::table('customer_category_price')->insertGetId($data);
            }
            return back();
        }
        $product_category = Category::all();
        $customer_category = CustomerCategory::all();
        if (isset($_GET['edit'])) {
            $records = CustomerCategoryPrice::join('customer_category', 'customer_category.id', '=', 'customer_category_price.customer_category')->join('category', 'category.id', '=', 'customer_category_price.product_category')->select(['customer_category_price.*', 'customer_category.type', 'category.name'])->where('customer_category_price.customer_category', $_GET['edit'])->orderBy('category.id', 'ASC')->get();
        }else{
            $records = CustomerCategoryPrice::join('customer_category', 'customer_category.id', '=', 'customer_category_price.customer_category')->join('category', 'category.id', '=', 'customer_category_price.product_category')->select(['customer_category_price.*', 'customer_category.type', 'category.name'])->orderBy('category.id', 'ASC')->get();
        }

        $tp = url("/assets/crm/");
        $header = $this -> header('Crm', 'index');
        $footer = $this -> footer();
        $title = 'CRM';

        return view('crm/category-master') -> with(compact('header', 'cfg', 'tp', 'footer', 'title', 'notices', 'customer_type', 'product_category', 'customer_category', 'records', 'buttons'));
    }
}
