<?php

namespace App\Http\Controllers\CRM;

use App\Config;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Crm;
use App\LeadOrder;
use App\Style;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class FullCalenderController extends Crm
{
    public $cfg;
    public $style;
    private $pdf;
    public $buttons;

    public function __construct()
    {
        $this->cfg = Config::where('id', '=', 1)->first();

        $this->style = Style::first();
        $this->pdf = '';
        $this->buttons = "['copy', 'excel', 'pdf']";
        if(empty(Session::get('crm'))){
            return redirect('crm/login');
        }
    }
    public function calenderReport(Request $request){
        if(empty(Session::get('crm'))){
            return redirect('crm/login');
        }
        $getcrmtype = isset($_COOKIE['crmType']) ? $_COOKIE['crmType'] : false;
        if($request->ajax()) {
            $data = LeadOrder::whereDate('select_date', '>=', $request->start)
                ->whereDate('select_date',   '<=', $request->end)->where('crm_type',$getcrmtype)->where('pipelines_stage','won')
                ->get(['id', 'message as title','select_date as start','updated_at as end']);
            return response()->json($data);
        }
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $tp1 = url("/themes/default/admin/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Calender Report';
        return view('crm/calender')-> with(compact('header', 'cfg', 'tp','tp1', 'footer', 'title'));;
    }

    public function ajax(Request $request){
        switch ($request->type) {
            case 'add':
                $event = Event::create([
                    'title' => $request->title,
                    'start' => $request->start,
                    'end' => $request->end,
                ]);

                return response()->json($event);
                break;

            case 'update':
                $event = Event::find($request->id)->update([
                    'title' => $request->title,
                    'start' => $request->start,
                    'end' => $request->end,
                ]);

                return response()->json($event);
                break;

            case 'delete':
                $event = Event::find($request->id)->delete();

                return response()->json($event);
                break;

            default:
                # code...
                break;
        }

    }
}
