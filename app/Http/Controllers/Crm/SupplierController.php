<?php

namespace App\Http\Controllers\CRM;

use App\Category;
use App\Config;
use App\CrmSupplier;
use App\CustomerCategory;
use App\CustomerClass;
use App\CustomerType;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Crm;
use App\State;
use App\Style;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class SupplierController extends Crm
{
    public $cfg;
    public $style;
    private $pdf;
    public $buttons;

    public function __construct()
    {
        $this->cfg = Config::where('id', '=', 1)->first();

        $this->style = Style::first();
        $this->pdf = '';
        $this->buttons = "['copy', 'excel', 'pdf']";
        if(empty(Session::get('crm'))){
            return redirect('crm/login');
        }
    }
    public function Suppliers(Request  $request)
    {
        if(empty(Session::get('crm'))){
            return redirect('crm/login');
        }
        $suppliers = '';
        $user = User::where('secure', Session::get('crm'))->first();
        if(!checkRole($user->u_id,"mas_sup")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';
        $pcid = '';
        $selected_district=array();
        if(isset($_POST['add'])){
            //dd($_POST);
            if(isset($_POST['category'])){
                $pcid = implode(',',$_POST['category']);
            }
            $customer_category = $_POST['customer_category'];
            $name = $_POST['name'];
            $proprieter_name = $_POST['proprieter_name'];
            $postal_address = $_POST['postal_address'];
            $email = $_POST['email'];
            $contact_no = $_POST['contact_no'];
            $district = $_POST['district'];
            $state = $_POST['state'];
            $msg_active = $_POST['msg_active'];
            DB::insert("INSERT INTO `crm_supplier`(`customer_category`, `product_category`, `name`, `proprieter_name`, `postal_address`, `email`, `contact_no`, `district`, `state`, `msg_active`) VALUES ('$customer_category','$pcid','$name','$proprieter_name','$postal_address','$email','$contact_no','$district','$state','$msg_active')");
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Supplier Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
        }
        if(isset($_POST['edit'])){
            // dd($_POST);
            if(isset($_POST['category'])){
                $pcid = implode(',',$_POST['category']);
            }
            $cidd = $_POST['cidd'];
            $customer_category = $_POST['customer_category'];
            $name = $_POST['name'];
            $proprieter_name = $_POST['proprieter_name'];
            $postal_address = $_POST['postal_address'];
            $email = $_POST['email'];
            $contact_no = $_POST['contact_no'];
            $district = $_POST['district'];
            $state = $_POST['state'];
            $msg_active = $_POST['msg_active'];
            DB::update("UPDATE `crm_supplier` SET `customer_category`='$customer_category',`product_category` = '$pcid',`name`='$name',`proprieter_name`='$proprieter_name',`postal_address`='$postal_address',`email`='$email',`contact_no`='$contact_no',`district`='$district',`state`='$state',`msg_active`='$msg_active' WHERE id = '$cidd'");
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p> Supplier Updated Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

        }
        if(isset($_GET['edit'])){
            $cid = $_GET['edit'];
            $selected_district = CrmSupplier::leftJoin('district', 'state_id', '=', 'crm_supplier.state')
                ->where('crm_supplier.id', '=', $cid)
                ->select('district.name as d_name','district.id as did')
                ->get();
        }
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Supplier';
        $a = '';
        $states = State::where('country_id','=',101)->get();
        $customer_class = CustomerClass::all();
        $customer_type = CustomerType::all();
        $customer_category = CustomerCategory::all();
        $product_category = Category::all();
        return view('crm/suppliers')->with(compact('header','cfg','tp','footer', 'title','notices','states','customer_class','customer_type','customer_category','selected_district','product_category', 'suppliers','buttons'));

    }
}
