<?php

namespace App\Http\Controllers\CRM;

use App\Config;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Crm;
use App\Referrer;
use App\ReferrerMaster;
use App\Style;
use App\User;
use Brian2694\Toastr\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;

class ReferrerMasterController extends Crm
{
    public $cfg;
    public $style;
    private $pdf;
    public $buttons;

    public function __construct()
    {
        $this->cfg = Config::where('id', '=', 1)->first();

        $this->style = Style::first();
        $this->pdf = '';
        $this->buttons = "['copy', 'excel', 'pdf']";
        if(empty(Session::get('crm'))){
            return redirect('crm/login');
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(empty(Session::get('crm'))){
            return redirect('crm/login');
        }
        $tp = url("/assets/crm/");
        $header = $this ->header('Crm', 'index');
        $footer = $this ->footer();
        $title = 'Referrer Master';
        $user = User::where('secure', Session::get('crm'))->first();
        if(!checkRole($user->u_id,"referrer_mas")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        return view('crm.referrer')->with(compact('header','tp', 'footer', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(empty(Session::get('crm'))){
            return redirect('crm/login');
        }
        $tp = url("/assets/crm/");
        $header = $this ->header('Crm', 'index');
        $footer = $this ->footer();
        $title = 'Create Referrer Master';
        return view('crm.create_referrer')->with(compact('header','tp', 'footer', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $referrer = new ReferrerMaster();
        $referrer->name = $request->name;
        $referrer->email = $request->email;
        $referrer->address = $request->address;
        $referrer->contact = $request->contact;
        $referrer->relation = $request->relation;
        $referrer->save();
        redirect(route('referrermaster.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(empty(Session::get('crm'))){
            return redirect('crm/login');
        }
        $referrer = ReferrerMaster::find($id);
        $tp = url("/assets/crm/");
        $header = $this ->header('Crm', 'index');
        $footer = $this ->footer();
        $title = 'Edit Referrer Master';
        return view('crm.edit_referrer')->with(compact('header','tp', 'footer', 'title','referrer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $referrer = ReferrerMaster::find($id);
        $referrer->name = $request->name;
        $referrer->email = $request->email;
        $referrer->address = $request->address;
        $referrer->contact = $request->contact;
        $referrer->relation = $request->relation;
        $referrer->save();
        redirect(route('referrermaster.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $referrer = ReferrerMaster::find($id);
        $referrer->delete();
        Toastr::success('Referrer Deleted Successfully :)','Success');
        redirect(route('referrermaster'));
    }

    public function getReferrerData(){
        $records = ReferrerMaster::all();
        return DataTables::of($records)
            ->editColumn('name', function($row){
                return $row->name;
            })->editColumn('email', function($row){
                return $row->email;
            })->editColumn('contact', function($row){
                return $row->contact;
            })->editColumn('address', function($row){
                return $row->address;
            })->editColumn('relation', function($row){
                return $row->relation;
            })
            ->addColumn('action', function($row){
                $html ='<button value="'.$row->id.'" class="btn myred waves-light" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="'.route('referrermaster.edit',[$row->id]).'"><i class="material-icons">edit</i></a>';
                return $html;
            })
            ->make(true);
    }
}
