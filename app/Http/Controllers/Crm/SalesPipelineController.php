<?php

namespace App\Http\Controllers\CRM;

use App\Config;
use App\CrmUserRole;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Crm;
use App\SalesPipeline;
use App\Style;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SalesPipelineController extends Crm
{
    public $cfg;
    public $style;
    private $pdf;
    public $buttons;

    public function __construct()
    {
        $this->cfg = Config::where('id', '=', 1)->first();

        $this->style = Style::first();
        $this->pdf = '';
        $this->buttons = "['copy', 'excel', 'pdf']";
        if(empty(Session::get('crm'))){
            return redirect('crm/login');
        }
    }
    public function SalesPipeline(){
        if(empty(Session::get('crm'))){
            return redirect('crm/login');
        }
        $notices = '';
        $salespipelines = SalesPipeline::all();
        $cfg = $this -> cfg;
        $tp = url("/assets/crm/");
        $header = $this -> header('Crm', 'index');
        $footer = $this -> footer();
        $title = 'Sales Pipelines';
        $roles = CrmUserRole::all();
        return view('crm/sales-pipeline')-> with(compact('header', 'cfg', 'tp', 'footer', 'title', 'notices', 'roles','salespipelines'));
    }
}
