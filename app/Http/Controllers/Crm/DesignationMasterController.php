<?php

namespace App\Http\Controllers\CRM;

use App\Category;
use App\Config;
use App\CustomerCategoryPrice;
use App\CustomerDesignation;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Crm;
use App\Style;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class DesignationMasterController extends Crm
{
    public $cfg;
    public $style;
    private $pdf;
    public $buttons;

    public function __construct()
    {
        $this->cfg = Config::where('id', '=', 1)->first();

        $this->style = Style::first();
        $this->pdf = '';
        $this->buttons = "['copy', 'excel', 'pdf']";
        if(empty(Session::get('crm'))){
            return redirect('crm/login');
        }
    }
    public function Designationmaster(){
        if(empty(Session::get('crm'))){
            return redirect('crm/login');
        }
        $cfg = '';
        $customer_type = '';
        $user = User::where('secure', Session::get('crm'))->first();
        $users = User::where('user_type','=','crm_user')->get();

        if(!checkRole($user->u_id,"mas_des")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';
        if (isset($_POST['add'])) {
            $data['name'] = $_POST['name'];
            DB::table('customer_designation') -> insertGetId($data);
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Designation Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
            return back();
        }

        if(isset($_POST['edit'])){
            $users = '';
            $id = $_POST['cid'];
            $name = $_POST['name'];

            DB::update("UPDATE customer_designation set `name`='$name' where `id`='$id'");
            return back();
        }

        $product_category = Category::all();
        $customer_designation = CustomerDesignation::all();
        if (isset($_GET['edit'])) {
            $records = CustomerCategoryPrice::join('customer_category', 'customer_category.id', '=', 'customer_category_price.customer_category')->join('category', 'category.id', '=', 'customer_category_price.product_category')->select(['customer_category_price.*', 'customer_category.type', 'category.name'])->where('customer_category_price.customer_category', $_GET['edit'])->orderBy('category.id', 'ASC')->get();
        }else{
            $records = CustomerCategoryPrice::join('customer_category', 'customer_category.id', '=', 'customer_category_price.customer_category')->join('category', 'category.id', '=', 'customer_category_price.product_category')->select(['customer_category_price.*', 'customer_category.type', 'category.name'])->orderBy('category.id', 'ASC')->get();
        }
        $tp = url("/assets/crm/");
        $header = $this -> header('Crm', 'index');
        $footer = $this -> footer();
        $title = 'CRM';
        return view('crm/designation-master') -> with(compact('header', 'cfg', 'tp', 'footer', 'title', 'notices', 'customer_type', 'product_category', 'customer_designation','users', 'records', 'buttons'));
    }
}
