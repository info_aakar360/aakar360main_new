<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
class FrontendController extends Admin
{
    public function banners(){
        $notices = '';
        $banner = '';
        if(isset($_POST['add'])){
            $title = escape($_POST['title']);
            $short_description = escape($_POST['short_description']);
            $priority = $_POST['priority'];
            $section = $_POST['section'];
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'banners/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }
            DB::insert("INSERT INTO banner (title,short_description,image,priority,section) VALUE ('$title','$short_description','$file','$priority', '$section')");
            $notices .= "<div class='alert mini alert-success'> Banner has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $title = escape($_POST['title']);
            $short_description = escape($_POST['short_description']);
            $priority = $_POST['priority'];
            $section = $_POST['section'];
            DB::update("UPDATE banner SET title = '$title',short_description = '$short_description',priority = '$priority',section='$section' WHERE id = '".$_GET['edit']."'");
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'banners/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                DB::update("UPDATE banner SET image = '$file' WHERE id = '".$_GET['edit']."'");
            }
            $notices .= "<div class='alert mini alert-success'> Page edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table('banner')->where('id','=',$_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'> Banner has been deleted successfully !</div>";
        }
        $header = $this->header('Banners','banners');
        $banners = DB::table('banner')->orderBy('priority','ASC')->get();
        if(isset($_GET['edit'])) {
            $banner = DB::table('banner')->where('id','=',$_GET['edit'])->first();
        }
        $footer = $this->footer();
        $tp = url("/themes/".$this->cfg->theme);
        return view('admin/banners')->with(compact('header','notices','tp','banners','banner','footer'));
    }
    public function home_page_design(){
        $data['notices'] = '';
        $data['header'] = $this->header('Home Page Design','home-page-design');
        if(isset($_POST['update'])){
            $text = escape($_POST['text']);
            $link = escape($_POST['link']);
            $title = escape($_POST['title']);
            DB::update("UPDATE home_page_design SET title = '$title', text = '$text',link = '$link' WHERE id = '".$_GET['edit']."'");
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'homepagedesign/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                DB::update("UPDATE home_page_design SET image = '$file' WHERE id = '".$_GET['edit']."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Home Page Design has been successfully updated !</div>";
        }
        if(isset($_GET['edit'])) {
            $data['design'] = DB::select("SELECT * FROM home_page_design WHERE id = ".$_GET['edit'])[0];
        }
        $data['footer'] = $this->footer();
        $data['links'] = DB::table('design_category')->orderBy('id','ASC')->get();
        $data['designs'] = DB::table('home_page_design')->orderBy('id','ASC')->get();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        return view('admin/home-page-design')->with('data',$data);
    }
    public function mega_menu_image(){
        $data['notices'] = '';
        $data['header'] = $this->header('Mega Menu Image','mega-menu-image');
        if(isset($_POST['update'])){
            $section = escape($_POST['section']);
            DB::update("UPDATE mega_menu_image SET section = '$section' WHERE id = '".$_GET['edit']."'");
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'megamenuimage/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                DB::update("UPDATE mega_menu_image SET image = '$file' WHERE id = '".$_GET['edit']."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Mega Menu Image has been successfully updated !</div>";
        }
        if(isset($_GET['edit'])) {
            $data['menu'] = DB::table('mega_menu_image')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        $data['menus'] = DB::table('mega_menu_image')->orderBy('id','ASC')->get();
        return view('admin/mega-menu-image')->with('data',$data);
    }
    public function DefaultImage(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $page = escape($_POST['page']);
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'banners/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }
            DB::insert("INSERT INTO default_image (image,page) VALUE ('$page','$file')");
            $data['notices'] .= "<div class='alert mini alert-success'> Image has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $page = escape($_POST['page']);
            DB::update("UPDATE default_image SET page = '$page' WHERE id = '".$_GET['edit']."'");
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'banners/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                DB::update("UPDATE default_image SET image = '$file' WHERE id = '".$_GET['edit']."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Image edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table('default_image')->where('id','=',$_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Image has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Deefault Image','default_image');
        $data['images'] = DB::table('default_image')->get();
        if(isset($_GET['edit'])) {
            $data['image'] = DB::table('default_image')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/default_image')->with('data',$data);
    }
    public function Link(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $datas['link'] = $_POST['link'];
            $datas['page'] = $_POST['page'];
            $datas['content'] = $_POST['content'];
            $post = DB::table('link')->insertGetId($datas);
            if (request()->file('image')) {
                $bname = request()->file('image')->getClientOriginalName();
                $bfile = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $bpath = base_path().'/assets/'.'products/';
                request()->file('image')->move($bpath,$bfile);
                $img = Image::make($bpath.$bfile)->resize(150, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($bpath.$bfile);
                DB::update("UPDATE link SET image = '".$bfile."' WHERE id = '".$post."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Link has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $link = $_POST['link'];
            $page = $_POST['page'];
            $content = $_POST['content'];
            if (request()->file('image')) {
                $bname = request()->file('image')->getClientOriginalName();
                $bfile = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $bpath = base_path().'/assets/'.'products/';
                request()->file('image')->move($bpath,$bfile);
                $img = Image::make($bpath.$bfile)->resize(150, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($bpath.$bfile);
                DB::update("UPDATE link SET image = '".$bfile."' WHERE id = '".$_GET['edit']."'");
            }
            DB::update("UPDATE link SET link = '$link', page = '$page', content = '$content' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Data Updated successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("link")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Link has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Link Master','link');
        $data['links'] = DB::table('link')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['link'] = DB::table('link')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        $data['categories'] = DB::table('category')->where('parent',0)->orderBy('id','DESC')->get();
        return view('admin/link')->with('data',$data);
    }
    public function pages(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            DB::insert("INSERT INTO pages (title,content,path) VALUE ('".escape($_POST['title'])."','".escape($_POST['content'])."','".$_POST['path']."')");
            $data['notices'] .= "<div class='alert mini alert-success'> Page has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            DB::update("UPDATE pages SET title = '".escape($_POST['title'])."',content = '".escape($_POST['content'])."',path = '".$_POST['path']."' WHERE id = ".$_GET['edit']);
            $data['notices'] .= "<div class='alert mini alert-success'> Page edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("pages")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Page has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Pages','pages');
        $data['pages'] = DB::table('pages')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['page'] = DB::table('pages')->where('id','=',$_GET['edit'])->first();
        }
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['footer'] = $this->footer();
        return view('admin/pages')->with('data',$data);
    }
}
