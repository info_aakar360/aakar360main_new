<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\City;
use App\CityWiseShippingCharges;
use App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\State;
use App\Unit;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Image;
class SettingsController extends Admin
{
    public function postcodes()
    {
        $data['notices'] = '';
        if (isset($_POST['add'])) {
            $postcode = $_POST['postcode'];
            $city = $_POST['city'];
            $district = $_POST['district'];
            $state = $_POST['state'];
            $delivery = $_POST['delivery'];
            $info = $_POST['info'];
            $pin = DB::table('pincode')->where('id', '=', $_GET['postcode'])->get();
            if (count($pin) > 0) {
                $data['notices'] .= "<div class='alert mini alert-danger'> Postcode already exists !</div>";
            } else {
                DB::insert("INSERT INTO pincode (pincode, city, district, state, delivery, info) VALUE ('$postcode', '$city', '$district', '$state', '$delivery', '$info')");
                $data['notices'] .= "<div class='alert mini alert-success'> Postcode has been successfully added !</div>";
            }
        }
        if (isset($_POST['edit'])) {
            $postcode = $_POST['postcode'];
            $city = $_POST['city'];
            $district = $_POST['district'];
            $state = $_POST['state'];
            $delivery = $_POST['delivery'];
            $info = $_POST['info'];
            DB::update("UPDATE pincode SET pincode = '$postcode',city = '$city',district = '$district',state = '$state',delivery = '$delivery',info = '$info' WHERE id = '" . $_GET['edit'] . "'");
            $data['notices'] .= "<div class='alert mini alert-success'> Postcode edited successfully !</div>";
        }
        if (isset($_GET['delete'])) {
            DB::table("pincode")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Postcode has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Postcodes', 'postcodes');
        $data['postcodes'] = DB::table('pincode')->orderBy('pincode', 'ASC')->get();
        if (isset($_GET['edit'])) {
            $data['postcode'] = DB::table('pincode')->where('id', '=', $_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/postcodes')->with('data', $data);
    }
    public function cities(){
        $data['notices'] = '';
        $data['buttons'] = "[]";
        if(isset($_POST['edit'])){
            $id = $_GET['edit'];
            $file = '';
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'images';
                request()->file('image')->move($path,$file);
            }
            $city = City::find($id);
            $city->is_available = $_POST['is_available'];
            $city->image = $file;
            $city->save();
            $data['notices'] .= "<div class='alert mini alert-success'> City updated successfully !</div>";
        }
        if(isset($_GET['add_pincodes'])){
            $data['id'] = $_GET['add_pincodes'];
            $data['categories'] = Category::get();
            $data['units'] = Unit::get();
            $data['pincodes'] = CityWiseShippingCharges::where('city_id','=',$_GET['add_pincodes'])->get();
        }
        if(isset($_GET['edit_pincode'])){
            $data['id'] = $_GET['edit_pincode'];
            $data['categories'] = Category::get();
            $data['units'] = Unit::get();
            $data['pincode'] = CityWiseShippingCharges::where('id','=',$_GET['edit_pincode'])->first();
        }
        if(isset($_POST['add_pincode'])){
            $data['id'] = $_GET['add_pincodes'];
            $pincode = new CityWiseShippingCharges();
            $pincode->from_weight = $_POST['from'];
            $pincode->to_weight = $_POST['to'];
            $pincode->unit = $_POST['unit'];
            $pincode->city_id = $_GET['add_pincodes'];
            $pincode->pincode = $_POST['pincode'];
            $pincode->price = $_POST['price'];
            $pincode->save();
            $data['notices'] .= "<div class='alert mini alert-success'> Pincode added successfully !</div>";
        }
        if(isset($_POST['edit_pincode'])){
            $data['id'] = $_GET['edit_pincode'];
            $pincode = CityWiseShippingCharges::find($_GET['edit_pincode']);
            $pincode->from_weight = $_POST['from'];
            $pincode->to_weight = $_POST['to'];
            $pincode->unit = $_POST['unit'];
            $pincode->pincode = $_POST['pincode'];
            $pincode->price = $_POST['price'];
            $pincode->save();
            $data['notices'] .= "<div class='alert mini alert-success'> Pincode updated successfully !</div>";
        }
        $data['header'] = $this->header('Cities','cities');
        $states = Arr::pluck(State::where('country_id', '101')->get(), 'id');
        $data['cities'] = City::whereIn('state_id',$states)->orderBy('name','ASC')->get();
        $data['states'] = State::where('country_id', '101')->get();
        if(isset($_GET['edit'])) {
            $data['city'] = City::where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        return view('admin/cities')->with('data',$data);
    }
    public function shipping(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $country = $_POST['country'];
            $name = $_POST['name'];
            $postcodes = $_POST['postcodes'];
            $category = implode(',', $_POST['category']);
            $cost = $_POST['cost'];
            DB::insert("INSERT INTO shipping (name,category,postcodes,country,cost) VALUE ('$name','$category','$postcodes','$country','$cost')");
            $data['notices'] .= "<div class='alert mini alert-success'> Shipping cost has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $country = $_POST['country'];
            $name = $_POST['name'];
            $postcodes = $_POST['postcodes'];
            $category = implode(',', $_POST['category']);
            $cost = $_POST['cost'];
            DB::update("UPDATE shipping SET name = '$name', category='$category', postcodes = '$postcodes', country = '$country',cost = '$cost' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Shipping cost edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("shipping")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Shipping cost has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Shipping cost','shipping');
        $data['costs'] = DB::table('shipping')->orderBy('id','DESC')->get();
        $data['countries'] = DB::table('country')->orderBy('nicename','ASC')->get();
        $data['postcodes'] = DB::table('pincode')->orderBy('pincode','ASC')->get();
        $data['category'] = DB::table('category')->get();
        if(isset($_GET['edit'])) {
            $data['cost'] = DB::table('shipping')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/shipping')->with('data',$data);
    }
    public function flat()
    {
        $data['notices'] = '';
        if (isset($_POST['add'])) {
            $country = $_POST['country'];
            $name = $_POST['name'];
            $postcodes = $_POST['postcodes'];
            $cost = $_POST['cost'];
            DB::insert("INSERT INTO shipping (name,postcodes,country,cost) VALUE ('$name','$postcodes','$country','$cost')");
            $data['notices'] .= "<div class='alert mini alert-success'> Shipping cost has been successfully added !</div>";
        }
        if (isset($_POST['edit'])) {
            $country = $_POST['country'];
            $name = $_POST['name'];
            $postcodes = $_POST['postcodes'];
            $cost = $_POST['cost'];
            DB::update("UPDATE shipping SET name = '$name', postcodes = '$postcodes', country = '$country',cost = '$cost' WHERE id = '" . $_GET['edit'] . "'");
            $data['notices'] .= "<div class='alert mini alert-success'> Shipping cost edited successfully !</div>";
        }
        if (isset($_GET['delete'])) {
            DB::table("shipping")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Shipping cost has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Shipping cost', 'shipping');
        $data['costs'] = DB::table('shipping')->orderBy('id', 'DESC')->get();
        $data['countries'] = DB::table('country')->orderBy('nicename', 'ASC')->get();
        $data['postcodes'] = DB::table('pincode')->orderBy('pincode', 'ASC')->get();
        if (isset($_GET['edit'])) {
            $data['cost'] = DB::table('shipping')->where('id', '=', $_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/flat')->with('data', $data);
    }
    public function payment(){
        $data['notices'] = '';
        if(isset($_POST['edit'])){
            $method = DB::table('payments')->where('id','=',$_GET['edit'])->first();
            $method_options = json_decode($method->options,true);
            $options = array();
            foreach ($method_options as $key => $value){
                $options[$key] = $_POST[$key];
            }
            $data_options = json_encode($options);
            DB::update("UPDATE payments SET title = '".escape($_POST['title'])."',options = '".$data_options."',active = '".$_POST['active']."' WHERE id = ".$_GET['edit']);
            $data['notices'] .= "<div class='alert mini alert-success'> Payment method edited successfully !</div>";
        }
        $data['header'] = $this->header('Payment methods','payment');
        $data['methods'] = DB::table('payments')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['method'] = DB::table('payments')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/payment')->with('data',$data);
    }
    public function currency(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $name = escape($_POST['name']);
            $code = escape($_POST['code']);
            $rate = $_POST['rate'];
            DB::insert("INSERT INTO currency (name,code,rate) VALUE ('$name','$code','$rate')");
            $data['notices'] .= "<div class='alert mini alert-success'> Currency has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $name = escape($_POST['name']);
            $code = escape($_POST['code']);
            $rate = $_POST['rate'];
            DB::update("UPDATE currency SET name = '$name',code = '$code',rate = '$rate' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Currency edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("currency")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Currency has been deleted successfully !</div>";
        }
        if(isset($_GET['default']))
        {
            DB::update("UPDATE currency SET `default` = '0' WHERE 1");
            DB::update("UPDATE currency SET `default` = '1' WHERE id = '".$_GET['default']."'");
            $data['notices'] .= "<div class='alert alert-success'> Currency has been set as default !</div>";
        }
        $data['header'] = $this->header('Currency','currency');
        $data['currencies'] = DB::table('currency')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['currency'] = DB::table('currency')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/currency')->with('data',$data);
    }
    public function settings(){
        $notices = '';
        if(isset($_POST['save'])){
            unset($_POST['save'],$_POST['_token']);
            DB::table('config')->update($_POST);
            $notices .= '<div class="alert alert-success mini">Settings updated successfully !</div>';
        }
        $header = $this->header('Settings','settings');
        $languages = DB::table('langs')->orderBy('id','DESC')->get();
        $cfg = $this->cfg;
        $footer = $this->footer();
        return view('admin/settings')->with(compact('header','notices','languages','cfg','footer'));
    }
    public function theme(){
        $notices = '';
        if(isset($_POST['save'])){
            unset($_POST['save'],$_POST['_token']);
            $_POST['background'] = $_POST['color1'].','.$_POST['color2'];
            $_POST['button'] = $_POST['button_text'].','.$_POST['button_link'];
            unset($_POST['color1'],$_POST['color2'],$_POST['button_text'],$_POST['button_link']);
            DB::table('style')->update($_POST);
            $notices .= '<div class="alert alert-success mini">Settings updated successfully !</div>';
        }
        $header = $this->header('Theme settings','theme');
        $style = $this->style;
        $footer = $this->footer();
        return view('admin/theme')->with(compact('header','notices','style','footer'));
    }
    public function lang(){
        $data['notices'] = '';
        if (isset($_GET['lang'])){
            $data['l'] = $_GET['lang'];
        } else {
            $data['l'] = $this->cfg->lang;
        }
        if(isset($_GET['save'])) {
            DB::update("UPDATE translate SET translation = '".$_POST['translation']."' WHERE id = ".$_GET['save']);
            return "success";
        }
        if(isset($_POST['add'])){
            $name = $_POST['name'];
            $code = $_POST['code'];
            DB::insert("INSERT INTO langs (name,code) VALUE ('$name','$code')");
            $data['notices'] .= "<div class='alert mini alert-success'> Language has been added !</div>";
        }
        if(isset($_POST['edit'])){
            $name = $_POST["name"];
            $code = $_POST["code"];
            DB::update("UPDATE langs SET name = '$name',code = '$code' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Language has been updated successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("translate")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Translation deleted successfully !</div>";
        }
        if(isset($_GET['delete_language'])) {
            DB::table("langs")->where('id', '=', $_GET['delete_language'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Language deleted successfully !</div>";
        }
        if(isset($_GET['edit'])){
            $data['lang'] = DB::table('langs')->where('id','=',$_GET['edit'])->first();
        }
        $data['header'] = $this->header('Language','lang');
        $data['langs'] = DB::table('langs')->get();
        $data['translations'] = DB::table('translate')->where('lang','=',$data['l'])->orderBy('id','DESC')->get();
        $data['footer'] = $this->footer();
        return view('admin/lang')->with('data',$data);
    }
    public function tokens(){
        $notices = '';
        if(isset($_GET['add'])){
            DB::insert("INSERT INTO tokens (token,requests) VALUE ('".md5(time())."',0)");
            $notices .= "<div class='alert mini alert-success'> An Api token has been generated !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("tokens")->where('token', '=', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'> Token deleted successfully !</div>";
        }
        $header = $this->header('API Tokens','tokens');
        $langs =  DB::table('tokens')->get();
        $tokens =  DB::table('tokens')->get();
        $footer = $this->footer();
        return view('admin/tokens')->with(compact('header','notices','tokens','footer'));
    }
    public function export(){
        $notices = '';
        if(isset($_GET['database']))
        {
            exec('mysqldump -u '.env('DB_USERNAME').' -p '.env('DB_PASSWORD').' -h '.env('DB_HOST', '127.0.0.1').' '.env('DB_DATABASE').' > '.base_path('file.sql'));
            header( "Pragma: public" );
            header( "Expires: 0" );
            header( "Cache-Control: must-revalidate, post-check=0, pre-check=0" );
            header( "Cache-Control: public" );
            header( "Content-Description: File Transfer" );
            header( "Content-type: application/zip" );
            header( "Content-Disposition: attachment; filename=\"file.sql\"" );
            header( "Content-Transfer-Encoding: binary" );
            readfile( base_path('file.sql') );
            unlink( base_path('file.sql') );
            return;
        }
        elseif (isset($_GET['files'])) {
            $rootPath = base_path();
            $backup = md5(time()).'.zip';
            $zip = new \ZipArchive();
            $zip->open($backup, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
            $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($rootPath),\RecursiveIteratorIterator::LEAVES_ONLY);
            foreach ($files as $name => $file)
            {
                if (!$file->isDir())
                {
                    $filePath = $file->getRealPath();
                    $relativePath = substr($filePath, strlen($rootPath) + 1);
                    $zip->addFile($filePath, $relativePath);
                }
            }
            $zip->close();
            header( "Pragma: public" );
            header( "Expires: 0" );
            header( "Cache-Control: must-revalidate, post-check=0, pre-check=0" );
            header( "Cache-Control: public" );
            header( "Content-Description: File Transfer" );
            header( "Content-type: application/zip" );
            header( "Content-Disposition: attachment; filename=\"" . $backup . "\"" );
            header( "Content-Transfer-Encoding: binary" );
            header( "Content-Length: " . filesize( $backup ) );
            ob_get_clean();
            readfile( $backup );
            ob_get_clean();
            unlink($backup);
            return;
        }
        $header = $this->header('Export','export');
        $footer = $this->footer();
        return view('admin/export')->with(compact('header','footer'));
    }

}
