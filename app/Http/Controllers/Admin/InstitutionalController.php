<?php

namespace App\Http\Controllers\Admin;

use App\DispatchPrograms;
use App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
class InstitutionalController extends Admin
{
    public function bookings(Request $request){
        $notices = '';
        $bookings = '';
        $booking = '';
        define('API_ACCESS_KEY','AAAAd2L0SKc:APA91bHWNCFq52FHj1XBUMjD_dUn6iaCVdV0EZMKTjoSPygXCOLCEIUfLnoPHePCMPriu7l63ASRpgvPJSbXN_cErCsGacQMxUvLWsVXOv9YcZJj3hL6avefq5BdwfBug3h7aHaupKtA');
        if(isset($_GET['approve']))
        {
            $items = DB::update("UPDATE rfq_booking SET status = 1 WHERE id = '".$_GET['approve']."'");
            $notices .= "<div class='alert alert-success'> Booking ID #".$_GET['approve']." Approved!</div>";
            if($items == '1'){
                $bookings = DB::table('rfq_booking')->where('id', $_GET['approve'])->first();
                if($bookings->direct_booking == '0'){
                    $status = '3';
                    DB::update("UPDATE request_quotation SET status = '$status' WHERE id = '".$bookings->rfq_id."'");
                }
                $getrfq = DB::table('rfq_booking')
                    ->where('id','=',$_GET['approve'])
                    ->first();
                $coun = DB::table('booking_count')
                    ->select('customer_id','booking_counter')
                    ->where('customer_id','=',$getrfq->user_id)
                    ->first();
                if($coun){
                    $total = $coun->booking_counter+1;
                    DB::update("UPDATE booking_count SET booking_counter = '$total' WHERE customer_id = '".$getrfq->user_id."'");
                }
                else{
                    $id = DB::insert("INSERT INTO booking_count (customer_id,booking_counter)
					VALUE ('".$getrfq->user_id."',1)");
                }
                $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                $customers = DB::table('customers')->where('id',$getrfq->user_id)->first();
                $cust_id = $customers->id;
                $note_read = 0;
                $token =$customers->gsm_id;
                $note = [
                    'title' =>'Bookings',
                    'body' => 'Your booking approved by admin'
                ];
                $extraNotificationData = ["message" => $note,"moredata" =>'dd'];
                $fcmNotification = [
                    'to'        => $token, //single token
                    'notification' => $note
                ];
                $headers = [
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                $result = curl_exec($ch);
                curl_close($ch);
                $result;
            }
        }
        if(isset($_GET['reject']))
        {
            $items = DB::update("UPDATE rfq_booking SET status = 2 WHERE id = '".$_GET['reject']."'");
            $notices .= "<div class='alert alert-danger'> Booking ID #".$_GET['reject']." Rejected!</div>";
            if($items == '1'){
                $bookings = DB::table('rfq_booking')->where('id', $_GET['reject'])->first();
                if($bookings->direct_booking == '0'){
                    $status = '4';
                    DB::update("UPDATE request_quotation SET status = '$status' WHERE id = '".$bookings->rfq_id."'");
                }
                $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                $customers = DB::table('customers')->where('id',$getrfq->user_id)->first();
                $cust_id = $customers->id;
                $note_read = 0;
                $token =$customers->gsm_id;
                $note = [
                    'title' =>'Bookings',
                    'body' => 'Your booking rejected by admin'
                ];
                $extraNotificationData = ["message" => $note,"moredata" =>'dd'];
                $fcmNotification = [
                    'to'        => $token, //single token
                    'notification' => $note
                ];
                $headers = [
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                $result = curl_exec($ch);
                curl_close($ch);
                $result;
            }
        }
        if(isset($_GET['view']))
        {
            $booking = DB::table('rfq_booking')->where('id',$_GET['view'])->first();//("select  rfq_booking SET status = 2 WHERE id = '".$_GET['reject']."'");
        }
        if(isset($_POST['updateStatus'])){
            $pid = $_POST['id'];
            $status = $_POST['status'];
            $items = DB::update("UPDATE rfq_booking SET status = '$status' WHERE id = '".$pid."'");
            if($items){
                $bookings = DB::table('rfq_booking')->where('id', $_GET['updateStatus'])->first();
                if($bookings->direct_booking !== '0'){
                    if($status == '1'){
                        $getrfq = DB::table('rfq_booking')
                            ->where('id','=',$_GET['updateStatus'])
                            ->first();
                        $coun = DB::table('booking_count')
                            ->select('customer_id','booking_counter')
                            ->where('customer_id','=',$getrfq->user_id)
                            ->first();
                        if($coun){
                            $total = $coun->booking_counter+1;
                            DB::update("UPDATE booking_count SET booking_counter = '$total' WHERE customer_id = '".$getrfq->user_id."'");
                        }
                        else{
                            $id = DB::insert("INSERT INTO booking_count (customer_id,booking_counter)
							VALUE ('".$getrfq->user_id."',1)");
                        }
                        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                        $customers = DB::table('customers')->where('id',$getrfq->user_id)->first();
                        $cust_id = $customers->id;
                        $note_read = 0;
                        $token =$customers->gsm_id;
                        $note = [
                            'title' =>'Bookings',
                            'body' => 'Your booking approved by admin'
                        ];
                        $extraNotificationData = ["message" => $note,"moredata" =>'dd'];
                        $fcmNotification = [
                            'to'        => $token, //single token
                            'notification' => $note
                        ];
                        $headers = [
                            'Authorization: key=' . API_ACCESS_KEY,
                            'Content-Type: application/json'
                        ];
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                        $result = curl_exec($ch);
                        curl_close($ch);
                        $result;
                    }
                }
                elseif($bookings->direct_booking == '0'){
                    if($status == '2'){
                        $getrfq = DB::table('rfq_booking')
                            ->where('id','=',$_GET['updateStatus'])
                            ->first();
                        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                        $customers = DB::table('customers')->where('id',$getrfq->user_id)->first();
                        $cust_id = $customers->id;
                        $note_read = 0;
                        $token =$customers->gsm_id;
                        $note = [
                            'title' =>'Bookings',
                            'body' => 'Your booking rejected by admin'
                        ];
                        $extraNotificationData = ["message" => $note,"moredata" =>'dd'];
                        $fcmNotification = [
                            'to'        => $token, //single token
                            'notification' => $note
                        ];
                        $headers = [
                            'Authorization: key=' . API_ACCESS_KEY,
                            'Content-Type: application/json'
                        ];
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                        $result = curl_exec($ch);
                        curl_close($ch);
                        $result;
                        $st = '4';
                        DB::update("UPDATE request_quotation SET status = '$st' WHERE id = '".$bookings->rfq_id."'");
                    }elseif($status == '1'){
                        $getrfq = DB::table('rfq_booking')
                            ->where('id','=',$_GET['updateStatus'])
                            ->first();
                        $coun = DB::table('booking_count')
                            ->select('customer_id','booking_counter')
                            ->where('customer_id','=',$getrfq->user_id)
                            ->first();
                        if($coun){
                            $total = $coun->booking_counter+1;
                            DB::update("UPDATE booking_count SET booking_counter = '$total' WHERE customer_id = '".$getrfq->user_id."'");
                        }
                        else{
                            $id = DB::insert("INSERT INTO booking_count (customer_id,booking_counter)
							VALUE ('".$getrfq->user_id."',1)");
                        }
                        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                        $customers = DB::table('customers')->where('id',$getrfq->user_id)->first();
                        $cust_id = $customers->id;
                        $note_read = 0;
                        $token =$customers->gsm_id;
                        $note = [
                            'title' =>'Bookings',
                            'body' => 'Your booking approved by admin'
                        ];
                        $extraNotificationData = ["message" => $note,"moredata" =>'dd'];
                        $fcmNotification = [
                            'to'        => $token, //single token
                            'notification' => $note
                        ];
                        $headers = [
                            'Authorization: key=' . API_ACCESS_KEY,
                            'Content-Type: application/json'
                        ];
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                        $result = curl_exec($ch);
                        curl_close($ch);
                        $result;

                        $st = '3';
                        DB::update("UPDATE request_quotation SET status = '$st' WHERE id = '".$bookings->rfq_id."'");
                    }elseif($status == '0'){
                        $getrfq = DB::table('rfq_booking')
                            ->where('id','=',$_GET['updateStatus'])
                            ->first();
                        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                        $customers = DB::table('customers')->where('id',$getrfq->user_id)->first();
                        $cust_id = $customers->id;
                        $note_read = 0;
                        $token =$customers->gsm_id;
                        $note = [
                            'title' =>'Bookings',
                            'body' => 'Your booking rejected by admin'
                        ];
                        $extraNotificationData = ["message" => $note,"moredata" =>'dd'];
                        $fcmNotification = [
                            'to'        => $token, //single token
                            'notification' => $note
                        ];
                        $headers = [
                            'Authorization: key=' . API_ACCESS_KEY,
                            'Content-Type: application/json'
                        ];
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                        $result = curl_exec($ch);
                        curl_close($ch);
                        $result;
                        $st = '2';
                        DB::update("UPDATE request_quotation SET status = '$st' WHERE id = '".$bookings->rfq_id."'");
                    }
                }
            }
            $notices .= "<div class='alert mini alert-success'>Status Changed successfully !</div>";
        }
        if(isset($_GET['edit']))
        {
            $booking = DB::table('rfq_booking')->where('id',$_GET['edit'])->first();//("select  rfq_booking SET status = 2 WHERE id = '".$_GET['reject']."'");
        }
        $header = $this->header('Bookings','bookings');
        $bookings = DB::table('rfq_booking')->orderBy('created_at', 'DESC')->get();//Bookings::orderBy('created_at', 'DESC')->get();
        $footer = $this->footer();
        $tp = url("/themes/".$this->cfg->theme);
        return view('admin/bookings')->with(compact('header','notices','tp','bookings','footer','booking'));
    }
    public function dispatchPrograms(){
        $notices = '';
        if(isset($_GET['status']) && isset($_GET['id']))
        {
            DB::update("UPDATE dispatch_programs SET status = ".$_GET['status']." WHERE id = '".$_GET['id']."'");
            $notices = "<div class='alert alert-success'> Status Changed successfully!</div>";
        }
        $header = $this->header('Dispatch Programs','dispatch');
        $dispatches = DispatchPrograms::orderBy('created_at', 'DESC')->get();

        $footer = $this->footer();
        $tp = url("/themes/".$this->cfg->theme);
        return view('admin/dispatch-programs')->with(compact('header','notices','tp','dispatches','footer'));
    }
    public function requestQuotation(){
        $data['notices'] = '';
        define('API_ACCESS_KEY','AAAAd2L0SKc:APA91bHWNCFq52FHj1XBUMjD_dUn6iaCVdV0EZMKTjoSPygXCOLCEIUfLnoPHePCMPriu7l63ASRpgvPJSbXN_cErCsGacQMxUvLWsVXOv9YcZJj3hL6avefq5BdwfBug3h7aHaupKtA');
        $retVar = array();
        if(isset($_POST['request_quotation'])){
            $pid = $_GET['edit'];
            $data['price_diff'] = $_POST['diff_price'];
            $action = $_POST['action'];
            $new_var = '';
            $i=0;
            $var = DB::table('request_quotation')->where('id',$pid)->first();
            $total = DB::table('products')->where('id',$var->product_id)->first();
            $validity  = $_POST['datetime'];
            $val1 = str_replace(" ", "", $validity);
            $val =   date('Y-m-d H:i:s', strtotime($val1));
            $variant = (array)json_decode(str_replace("\\", "", $var->variants));
            if($var->method == 'bysize'){
                $variant = json_decode($var->variants);
                $bids = explode(',', $var->brand_id);
                foreach($bids as $bid){
                    foreach($variant->$bid as $j=>$var){
                        $var_price = 0;
                        if($action == 0){
                            if(is_numeric($variant->$bid[$j]->price_detail->price)){
                                $var_price = $variant->$bid[$j]->price_detail->price-$data['price_diff'];
                            }
                        }
                        elseif ($action == 1){
                            if(is_numeric($variant->$bid[$j]->price_detail->price)){
                                $var_price = $variant->$bid[$j]->price_detail->price+$data['price_diff'];
                            }
                        }
                        $variant->$bid[$j]->var_price = $var_price;
                        $variant->$bid[$j]->basic_price = $variant->$bid[$j]->price_detail->price;
                        $variant->$bid[$j]->action = $action;
                        $variant->$bid[$j]->action_price = $data['price_diff'];
                        $variant->$bid[$j]->total_quantity = $total->quantity-$variant->$bid[$j]->quantity;
                    }
                }
                $new_var = json_encode($variant);
            }else{
                foreach ($variant as  $vari){
                    $var_price = 0;
                    $title = $_POST['variant_title'][$i];
                    $pricet = $_POST['var_price'][$i];
                    $basic_price = $_POST['basic_price'][$i];
                    if($action == 0){
                        if(is_numeric($basic_price)){
                            $var_price = $basic_price-$data['price_diff'];
                        }
                    }
                    elseif ($action == 1){
                        if(is_numeric($basic_price)){
                            $var_price = $basic_price+$data['price_diff'];
                        }
                    }
                    $info = array();
                    if(isset($vari->price_detail)){
                        foreach ($vari->price_detail as  $vpd){
                            $info[] = array(
                                "basic" => $vpd->basic,
                                "size_diff" => $vpd->size_diff,
                                "loading" => $vpd->loading,
                                "tax" => $vpd->tax,
                                "total_amount" => $vpd->total_amount,
                            );
                        }
                    }
                    $retVar[] = array(
                        "id" => $vari->id,
                        "title" => $vari->title,
                        "quantity" => $vari->quantity,
                        "unit" => $vari->unit,
                        "var_price" => $data['price_diff'],
                        "basic_price" => $basic_price,
                        "action" => $action,
                        "action_price" => $pricet,
                        "total_quantity" =>$total->quantity-$vari->quantity,
                        "price_detail" => $info
                    );
                    $new_var = json_encode($retVar);
                    $i++;
                }
            }
            $payment_option = $_POST['payment_size'];
            date_default_timezone_set('Asia/Kolkata');
            $date = date('Y-m-d H:i:s');
            $pd = $data['price_diff'];
            if($val > $date){
                DB::update("UPDATE request_quotation SET payment_option = '$payment_option', variants = '$new_var',status = 1,validity = '$val', price_diff='$pd', action = $action WHERE id = '".$pid."'");
                $data['notices'] .= "<div class='alert mini alert-success'>Request Quotation successfully !</div>";

                $getrfq = DB::table('request_quotation')
                    ->where('id','=',$pid)
                    ->first();
                $items = DB::table('rfq_count')
                    ->select('customer_id','rfq_counter')
                    ->where('customer_id','=',$getrfq->user_id)
                    ->first();
                if($items){
                    $total = $items->rfq_counter+1;
                    DB::update("UPDATE rfq_count SET rfq_counter = '$total' WHERE customer_id = '".$getrfq->user_id."'");
                }
                else{
                    $id = DB::insert("INSERT INTO rfq_count (customer_id,rfq_counter) 
				VALUE ('".$getrfq->user_id."','1')");
                }
                $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                $customers = DB::table('customers')->where('id',$getrfq->user_id)->first();
                $cust_id = $customers->id;
                $note_read = 0;
                $token =$customers->gsm_id;
                $note = [
                    'title' =>'Bookings',
                    'body' => 'Your quotation approved by admin'
                ];
                $extraNotificationData = ["message" => $note,"moredata" =>'dd'];
                $fcmNotification = [
                    'to'        => $token, //single token
                    'notification' => $note
                ];
                $headers = [
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                $result = curl_exec($ch);
                curl_close($ch);
                $result;
                $data['notices'] .= "<div class='alert mini alert-success'>Updated successfully.</div>";
            }else{
                $data['notices'] .= "<div class='alert mini alert-success'>Please change date if you want to update.</div>";
            }
        }
        if(isset($_GET['status']) && isset($_GET['id']))
        {
            DB::update("UPDATE dispatch_programs SET status = ".$_GET['status']." WHERE id = '".$_GET['id']."'");
            $data['notices'] = "<div class='alert alert-success'> Status Changed successfully!</div>";
        }
        if(isset($_POST['updateStatus'])){
            $pid = $_POST['id'];
            $status = $_POST['status'];
            DB::update("UPDATE request_quotation SET status = '$status' WHERE id = '".$pid."'");
            $data['notices'] .= "<div class='alert mini alert-success'>Status Changed successfully !</div>";
            if($status == '1'){
                $getrfq = DB::table('request_quotation')
                    ->where('id','=',$pid)
                    ->first();
                $items = DB::table('rfq_count')
                    ->select('customer_id','rfq_counter')
                    ->where('customer_id','=',$getrfq->user_id)
                    ->first();
                if($items){
                    $total = $items->rfq_counter+1;
                    DB::update("UPDATE rfq_count SET rfq_counter = '$total' WHERE customer_id = '".$getrfq->user_id."'");
                }
                else{
                    $id = DB::insert("INSERT INTO rfq_count (customer_id,rfq_counter) 
				VALUE ('".$getrfq->user_id."','1')");
                }
                $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                $customers = DB::table('customers')->where('id',$getrfq->user_id)->first();
                $cust_id = $customers->id;
                $note_read = 0;
                $token =$customers->gsm_id;
                $note = [
                    'title' =>'Bookings',
                    'body' => 'Your quotation approved by admin'
                ];
                $extraNotificationData = ["message" => $note,"moredata" =>'dd'];
                $fcmNotification = [
                    'to'        => $token, //single token
                    'notification' => $note
                ];
                $headers = [
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                $result = curl_exec($ch);
                curl_close($ch);
                $result;
                $data['notices'] .= "<div class='alert mini alert-success'>Updated successfully.</div>";
            }else{
                $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                $customers = DB::table('customers')->where('id',$getrfq->user_id)->first();
                $cust_id = $customers->id;
                $note_read = 0;
                $token =$customers->gsm_id;
                $note = [
                    'title' =>'Bookings',
                    'body' => 'Your quotation rejected by admin'
                ];
                $extraNotificationData = ["message" => $note,"moredata" =>'dd'];
                $fcmNotification = [
                    'to'        => $token, //single token
                    'notification' => $note
                ];
                $headers = [
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                $result = curl_exec($ch);
                curl_close($ch);
                $result;
                $data['notices'] .= "<div class='alert mini alert-success'>Updated successfully.</div>";
            }
        }
        if(isset($_GET['updateStatus']))
        {
            $data['qStatus'] = DB::table('request_quotation')->where('id',$_GET['updateStatus'])->first();

        }
        $data['header'] = $this->header('Request Quotation','dispatch');
        $data['quotations'] = DB::table('request_quotation')->orderBy('id', 'DESC')->get();
        $data['footer'] = $this->footer();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['notices'] .= "";

        return view('admin/request-quotation')->with('data',$data);
    }
    public function requestForCall(){
        $data['notices'] = '';
        $data['header'] = $this->header('Request For Call','request-for-call');
        $data['quotations'] = DB::table('request_for_call')->orderBy('created_at', 'DESC')->get();
        $data['footer'] = $this->footer();
        if(isset($_GET['edit']))
        {
            $data['requestCall'] = DB::table('request_for_call')->where('id', $_GET['edit'])->first();
        }
        if(isset($_POST['updateStatus']))
        {
            $pid = $_POST['id'];
            $status = $_POST['status'];
            $remark = $_POST['remark'];
            $items = DB::update("UPDATE request_for_call SET remark='$remark', status = '$status' WHERE id = '".$pid."'");
        }
        $data['tp'] = url("/themes/".$this->cfg->theme);
        return view('admin/request-for-call')->with('data',$data);
    }
    public function quickQuote(){
        $notices = '';
        $header = $this->header('Quick Quote','quick-quote');
        $quotations = DB::table('quick_quote')->orderBy('time', 'DESC')->get();
        $footer = $this->footer();
        $tp = url("/themes/".$this->cfg->theme);
        if(isset($_GET['status']) && isset($_GET['id']))
        {
            DB::update("UPDATE quick_quote SET status = ".$_GET['status']." WHERE id = '".$_GET['id']."'");
            $notices = "<div class='alert alert-success'> Status Changed successfully!</div>";
        }
        return view('admin/quick-quote')->with(compact('header','notices','tp','quotations','footer'));
    }
    public function mentorProgram(){
        $data['notices'] = '';
        $data['header'] = $this->header('Mentor Program','mentor-program');
        $data['quotations'] = DB::table('mentor')->orderBy('id', 'DESC')->get();
        $data['footer'] = $this->footer();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        if(isset($_GET['edit']))
        {
            $data['requestCall'] = DB::table('mentor')->where('id', $_GET['edit'])->first();
        }
        if(isset($_POST['updateStatus']))
        {
            $pid = $_POST['id'];
            $status = $_POST['status'];
            $items = DB::update("UPDATE mentor SET status = '$status' WHERE id = '".$pid."'");
        }
        return view('admin/mentor-program')->with('data',$data);
    }
    public function creditCharges(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            DB::insert("INSERT INTO credit_charges (days,amount) VALUE ('".escape($_POST['days'])."','".$_POST['amount']."')");
            $data['notices'] .= "<div class='alert mini alert-success'> Charges has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            DB::update("UPDATE credit_charges SET days = '".escape($_POST['days'])."',amount = '".$_POST['amount']."' WHERE id = ".$_GET['edit']);
            $data['notices'] .= "<div class='alert mini alert-success'> Charges edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("credit_charges")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Charges has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Credit Charges','credit-charges');
        $data['pages'] = DB::table('credit_charges')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['page'] = DB::table('credit_charges')->where('id','=',$_GET['edit'])->first();
        }
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['footer'] = $this->footer();
        return view('admin/credit-charges')->with('data',$data);
    }
    public function counterOffer(){
        $data['notices'] = '';
        $data['header'] = $this->header('Counter Offer','counter-offer');
        $data['quotations'] = DB::table('counter_offer')->orderBy('id', 'DESC')->get();
        $data['footer'] = $this->footer();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        if(isset($_POST['updateStatus']))
        {
            $pid = $_POST['id'];
            $status = $_POST['status'];

            DB::update("UPDATE counter_offer SET status = '$status' WHERE id = '".$pid."'");

            if($status == 2){
                $co = DB::table('counter_offer')->where('id', $pid)->first();
                //dd($co);
                $pro = DB::table('products')->where('id', $co->product_id)->first();
                $price_detail[] = getInstitutionalPrice($co->product_id, $co->user_id, $co->hub_id, false,false,false);
                $variants[] = array(
                    "id"=>$pro->id,
                    "title"=>$pro->title,
                    "quantity"=>$co->quantity,
                    "unit"=>$co->unit,
                    "price"=>$co->price,
                    "price_detail" => $price_detail
                );
                $product_id = $co->product_id;
                $brand_id = $co->product_id;
                $user_id = $co->user_id;
                $hub_id = $co->hub_id;
                $method = 'bybasic';
                $unit = $co->unit;
                $credit = $co->credit_charge_id;
                DB::insert("INSERT INTO rfq_booking (product_id,brand_id,credit_charge_id,user_id,hub_id,method,variants,without_variants,status)
					VALUE ('".$product_id."','".$brand_id."','".$credit."','".$user_id."','".$hub_id."','".$method."','".json_encode($variants)."','1','1')");
                $items = DB::table('booking_count')
                    ->select('customer_id','booking_counter')
                    ->where('customer_id','=',$co->user_id)
                    ->first();
                if($items){
                    $total = $items->booking_counter+1;
                    DB::update("UPDATE booking_count SET booking_counter = '$total' WHERE customer_id = '".$co->user_id."'");
                }
                else{
                    $id = DB::insert("INSERT INTO booking_count (customer_id,booking_counter)
					VALUE ('".$co->user_id."',1)");
                }
            }
        }
        if(isset($_GET['edit']))
        {
            $data['counter'] = DB::table('counter_offer')->where('id',$_GET['edit'])->first();
        }
        return view('admin/counter-offer')->with('data',$data);
    }
}
