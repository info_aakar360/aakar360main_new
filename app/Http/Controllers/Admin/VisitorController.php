<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
class VisitorController extends Admin
{
    public function referrers(){
        $header = $this->header('Referrers','referrers');
        $referrers = DB::table('referrer')->orderBy('visits','DESC')->get();
        $footer = $this->footer();
        return view('admin/referrers')->with(compact('header','referrers','footer'));
    }
    public function os(){
        $header = $this->header('Operating systems','os');
        $OSs = DB::table('os')->orderBy('visits','DESC')->get();
        $footer = $this->footer();
        return view('admin/os')->with(compact('header','OSs','footer'));
    }
    public function browsers(){
        $header = $this->header('Browsers','browsers');
        $browsers = DB::table('browsers')->orderBy('visits','DESC')->get();
        $footer = $this->footer();
        return view('admin/browsers')->with(compact('header','browsers','footer'));
    }
}
