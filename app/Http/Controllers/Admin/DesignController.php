<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
class DesignController extends Admin
{
    public function design_category(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $name1 = $_POST['name'];
            $slug = makeSlugOf($name1, 'design_category');
            $description = $_POST['description'];
            $path1 = $_POST['path'];
            $parent = $_POST['parent'];
            $layout = $_POST['layout'];
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'design/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                $thumb = Image::make($path.$file)->resize(165,110)->save($path.'thumbs/'.$file);
            }
            DB::insert("INSERT INTO design_category (name,slug,description,path,parent,layout,image) VALUE ('$name1','$slug','$description','$path1','$parent','$layout','$file')");
            $data['notices'] .= "<div class='alert mini alert-success'>Design Category has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $name1 = $_POST["name"];
            $description = $_POST['description'];
            $path1 = $_POST["path"];
            $parent = $_POST["parent"];
            $layout = $_POST['layout'];
            DB::update("UPDATE design_category SET name = '$name1',description='$description',path = '$path1',parent = '$parent', layout = '$layout' WHERE id = '" . $_GET['edit'] . "'");
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'design/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                $thumb = Image::make($path.$file)->resize(165,110)->save($path.'thumbs/'.$file);
                DB::update("UPDATE design_category SET image = '$file' WHERE id = '".$_GET['edit']."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Page edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("design_category")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'>Design Category has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Categories','design_category');
        $data['design_category'] = DB::table('design_category')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['design_category'] = DB::table('design_category')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        $data['parents'] =DB::table('design_category')->where('parent',0)->orderBy('id','DESC')->get();
        return view('admin/design_category')->with('data',$data);
    }
    public function DesignCatBanner(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $title = escape($_POST['title']);
            $short_description = escape($_POST['description']);
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'banners/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }
            DB::insert("INSERT INTO design_cat_banner (title,description,image) VALUE ('$title','$short_description','$file')");
            $data['notices'] .= "<div class='alert mini alert-success'> Banner has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $title = escape($_POST['title']);
            $short_description = escape($_POST['description']);
            DB::update("UPDATE design_cat_banner SET title = '$title',description = '$short_description' WHERE id = '".$_GET['edit']."'");
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'banners/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                DB::update("UPDATE design_cat_banner SET image = '$file' WHERE id = '".$_GET['edit']."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Page edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table('design_cat_banner')->where('id','=',$_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Banner has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Design Category Banner','design-cat-banner');
        $data['cbanners'] = DB::table('design_cat_banner')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['cbanner'] = DB::table('design_cat_banner')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/design-cat-banner')->with('data',$data);
    }
    public function BestForLayout(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $datas['link'] = $_POST['link'];
            $datas['title'] = $_POST['title'];
            $datas['content'] = $_POST['content'];
            $post = DB::table('best_for_layout')->insertGetId($datas);
            if (request()->file('image')) {
                $bname = request()->file('image')->getClientOriginalName();
                $bfile = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $bpath = base_path().'/assets/'.'products/';
                request()->file('image')->move($bpath,$bfile);
                $img = Image::make($bpath.$bfile)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($bpath.$bfile);
                DB::update("UPDATE best_for_layout SET image = '".$bfile."' WHERE id = '".$post."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Best For Category has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $link = $_POST['link'];
            $category = $_POST['title'];
            $content = $_POST['content'];
            if (request()->file('image')) {
                $bname = request()->file('image')->getClientOriginalName();
                $bfile = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $bpath = base_path().'/assets/'.'products/';
                request()->file('image')->move($bpath,$bfile);
                $img = Image::make($bpath.$bfile)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($bpath.$bfile);
                DB::update("UPDATE best_for_layout SET image = '".$bfile."' WHERE id = '".$_GET['edit']."'");
            }
            DB::update("UPDATE best_for_layout SET link = '$link', title = '$category', content = '$content' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Data Updated successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table('best_for_layout')->where('id','=',$_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Link has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Best For Category Master','best-for-cat');
        $data['bestforcats'] = DB::table('best_for_layout')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['bestforcat'] = DB::table('best_for_layout')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/best-for-layout')->with('data',$data);
    }
    public function layout_plan(){
        $notices = '';
        if(isset($_POST['add'])){
            $data['title'] = $_POST['title'];
            $data['slug'] = makeSlugOf($data['title'], 'layout_plans');
            $data['plan_code'] = $_POST['plan_code'];
            $data['description'] = $_POST['description'];
            $data['specification'] = $_POST['specification'];
            $data['price'] = $_POST['price'];
            $data['p1_name'] = $_POST['p1_name'];
            $data['p1_price'] = $_POST['p1_price'];
            $data['p1_duration'] = $_POST['p1_duration'];
            $data['p1_description'] = $_POST['p1_description'];
            $data['p2_name'] = $_POST['p2_name'];
            $data['p2_price'] = $_POST['p2_price'];
            $data['p2_duration'] = $_POST['p2_duration'];
            $data['p2_description'] = $_POST['p2_description'];
            $data['p3_name'] = $_POST['p3_name'];
            $data['p3_price'] = $_POST['p3_price'];
            $data['p3_duration'] = $_POST['p3_duration'];
            $data['p3_description'] = $_POST['p3_description'];
            $data['featured'] = $_POST['featured'];
            $ammi = array();
            $x =0;
            foreach ($_POST['ammenities'] as $ammen){
                $ammi[$ammen] = $_POST['interior_ammenities'][$x];
                $x++;
            }
            $data['interior_ammenities'] = json_encode($ammi);
            $data['category'] = (int)$_POST['category'];
            $data['drawing_views'] = json_encode($_POST['drawing_views']);
            $data['file'] = '';
            $data['images'] = '';
            $layout_plan = DB::table('layout_plans')->insertGetId($data);
            if (request()->file('images')) {
                $order = 0;
                $images = array();
                foreach (request()->file('images') as $file) {
                    $name = $file->getClientOriginalName();
                    if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                        $images[] = $image = md5(time()).'-'.$order.'.'.$file->getClientOriginalExtension();
                        $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'layout_plans/';
                        $file->move($path,$image);
                        if($image) {
                            $img = Image::make($path. $image)->resize(800, null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($path. $image);
                            $thumb = Image::make($path. $image)->resize(165, 110)->save($path.'thumbs/'.$image);
                            $order++;
                        }
                    } else {
                        $notices .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                    }
                }
                DB::update("UPDATE layout_plans SET images = '".implode(',',$images)."' WHERE id = '".$layout_plan."'");
            }
            if (request()->file('file') || request()->file('file1')) {
                $name = request()->file('file')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('file')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'layout_plans/';
                request()->file('file')->move($path,$file);
                $img = Image::make($path. $file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path. $file);
                $thumb = Image::make($path. $file)->resize(165, 110)->save($path.'thumbs/'.$file);
                $name = request()->file('file1')->getClientOriginalName();
                $file1 = md5(time()).'.'.request()->file('file1')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'layout_plans/';
                request()->file('file1')->move($path,$file1);
                $img = Image::make($path. $file1)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path. $file1);
                $thumb = Image::make($path. $file1)->resize(165, 110)->save($path.'thumbs/'.$file1);

                $nfile = $file.','.$file1;
                DB::update("UPDATE layout_plans SET file = '".$nfile."' WHERE id = '".$layout_plan."'");
            }
            $notices .= "<div class='alert mini alert-success'> Plan has been added successfully !</div>";
        }
        if(isset($_POST['edit'])){
            $title = escape($_POST["title"]);
            $plan_code = $_POST['plan_code'];
            $description = escape($_POST['description']);
            $specification = escape($_POST['specification']);
            $price = $_POST['price'];
            $tax = $_POST['tax'];
            $p1_name = $_POST['p1_name'];
            $p1_price = $_POST['p1_price'];
            $p1_duration = $_POST['p1_duration'];
            $p1_description = $_POST['p1_description'];
            $p2_name = $_POST['p2_name'];
            $p2_price = $_POST['p2_price'];
            $p2_duration = $_POST['p2_duration'];
            $p2_description = $_POST['p2_description'];
            $p3_name = $_POST['p3_name'];
            $p3_price = $_POST['p3_price'];
            $p3_duration = $_POST['p3_duration'];
            $p3_description = $_POST['p3_description'];
            $featured = $_POST['featured'];
            $ammi = array();
            $x =0;
            foreach ($_POST['ammenities'] as $ammen){
                $ammi[$ammen] = $_POST['interior_ammenities'][$x];
                $x++;
            }
            $interior_ammenities = json_encode($ammi);
            $category = (int)$_POST['category'];
            $drawing_views = json_encode($_POST['drawing_views']);
            $file = '';
            $images = '';
            DB::update("UPDATE layout_plans SET plan_code='$plan_code', title = '$title', price = '$price', 
			tax = '$tax', description = '$description', category = '$category', specification = '$specification', 
			interior_ammenities = '$interior_ammenities', drawing_views = '$drawing_views', p1_name = '$p1_name', p1_price = '$p1_price',
			 p1_duration = '$p1_duration', p1_description = '$p1_description', p2_name = '$p2_name', p2_price = '$p2_price',
			 p2_duration = '$p2_duration', p2_description = '$p2_description', p3_name = '$p3_name', p3_price = '$p3_price',
			 p3_duration = '$p3_duration', p3_description = '$p3_description', featured = '$featured'  WHERE id = '".$_GET['edit']."'");
            if (request()->file('images')) {
                $order = 0;
                $images = array();
                foreach (request()->file('images') as $file) {
                    $name = $file->getClientOriginalName();
                    if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                        $images[] = $image = md5(time()).'-'.$order.'.'.$file->getClientOriginalExtension();
                        $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'layout_plans/';
                        $file->move($path,$image);
                        if($image) {
                            $img = Image::make($path. $image)->resize(800, null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($path. $image);
                            $thumb = Image::make($path. $image)->resize(165, 110)->save($path.'thumbs/'.$image);
                            $order++;
                        }
                    } else {
                        $notices .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                    }
                }
                DB::update("UPDATE layout_plans SET images = '".implode(',',$images)."' WHERE id = '".$_GET['edit']."'");
            }
            if (request()->file('file') || request()->file('file1')) {
                $name = request()->file('file')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('file')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'layout_plans/';
                request()->file('file')->move($path,$file);
                $img = Image::make($path. $file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path. $file);
                $thumb = Image::make($path. $file)->resize(165, 110)->save($path.'thumbs/'.$file);

                $name = request()->file('file1')->getClientOriginalName();
                $file1 = md5(time()).'.'.request()->file('file1')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'layout_plans/';
                request()->file('file1')->move($path,$file1);
                $img = Image::make($path. $file1)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path. $file1);
                $thumb = Image::make($path. $file1)->resize(165, 110)->save($path.'thumbs/'.$file1);

                $nfile = $file.','.$file1;
                DB::update("UPDATE layout_plans SET file = '".$nfile."' WHERE id = '".$_GET['edit']."'");
            }
            $notices .= '<div class="alert mini alert-success">Plan updated successfully !</div>';
        }
        if(isset($_GET['delete'])){
            DB::table("layout_plans")->where('id', '=', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-dismissible alert-success'> Plan has been deleted </div>";
        }
        $plan = [];
        if(isset($_GET['edit'])){
            $plan = DB::table('layout_plans')->where('id','=',$_GET['edit'])->first();
        }
        $header = $this->header('Layout Plans','layout_plans');
        $plans = DB::table('layout_plans')->orderBy('id','DESC')->get();
        $amminities = DB::table('amminities')->orderBy('id','DESC')->get();
        $categories = DB::table('design_category')->where('parent','=',0)->orderBy('id','DESC')->get();
        $footer = $this->footer();
        return view('admin/layout_plans')->with(compact('header','notices','plans','plan','categories','footer', 'amminities'));
    }
    public function layout_addon(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $aname = $_POST['name'];
            $price = $_POST['price'];
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'addon/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(250, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }
            DB::insert("INSERT INTO layout_addon (`name`,`price`,`image`) VALUE ('$aname','$price','$file')");
            $data['notices'] .= "<div class='alert mini alert-success'> Addon has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $name = $_POST["name"];
            $price = $_POST["price"];
            DB::update("UPDATE layout_addon SET name = '$name',price = '$price' WHERE id = '".$_GET['edit']."'");
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'addon/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(250, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                DB::update("UPDATE layout_addon SET image = '$file' WHERE id = '".$_GET['edit']."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'>Addon edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("layout_addon")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'>Addon has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Layout Addons','layout addons');
        $data['addons'] = DB::table('layout_addon')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['addon'] = DB::table('layout_addon')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/layout_addon')->with('data',$data);
    }
    public function photos(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $title = $_POST['title'];
            $slug = makeSlugOf($title, 'photo_gallery');
            $description = $_POST['description'];
            $category = $_POST['category'];
            $featured = $_POST['featured'];
            $file = '';
            if (request()->file('banner_image')) {
                $name = request()->file('banner_image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('banner_image')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'gallery/';
                request()->file('banner_image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                $thumb = Image::make($path.$file)->resize(165,110)->save($path.'thumbs/'.$file);
            }
            DB::insert("INSERT INTO photo_gallery (title,slug,image,description,category,featured) VALUE ('$title','$slug','$file','$description','$category','$featured')");
            $data['notices'] .= "<div class='alert mini alert-success'>Photo Gallery has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $title = $_POST['title'];
            $description = $_POST['description'];
            $category = $_POST['category'];
            $featured = $_POST['featured'];
            $file = '';
            if (request()->file('banner_image')) {
                $name = request()->file('banner_image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('banner_image')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'gallery/';
                request()->file('banner_image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                $thumb = Image::make($path.$file)->resize(165,110)->save($path.'thumbs/'.$file);
            }
            DB::update("UPDATE photo_gallery SET title = '$title',image = '$file', description = '$description',category = '$category',featured = '$featured' WHERE id = '" . $_GET['edit'] . "'");
            $data['notices'] .= "<div class='alert mini alert-success'> Page edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("photo_gallery")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'>Photo Gallery has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Photo Gallery','photos');
        $data['photo'] = DB::table('photo_gallery')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['photo'] = DB::table('photo_gallery')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        $data['categories'] = DB::table('design_category')->where('parent','=',0)->orderBy('id','DESC')->get();
        return view('admin/photo_gallery')->with('data',$data);
    }
    public function BestForPhoto(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $data['link'] = $_POST['link'];
            $data['title'] = $_POST['title'];
            $data['content'] = $_POST['content'];
            $post = DB::table('best_for_photo')->insertGetId($data);
            if (request()->file('image')) {
                $bname = request()->file('image')->getClientOriginalName();
                $bfile = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $bpath = base_path().'/assets/'.'products/';
                request()->file('image')->move($bpath,$bfile);
                $img = Image::make($bpath.$bfile)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($bpath.$bfile);
                DB::update("UPDATE best_for_photo SET image = '".$bfile."' WHERE id = '".$post."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Best For Photo has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $link = $_POST['link'];
            $category = $_POST['title'];
            $content = $_POST['content'];
            if (request()->file('image')) {
                $bname = request()->file('image')->getClientOriginalName();
                $bfile = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $bpath = base_path().'/assets/'.'products/';
                request()->file('image')->move($bpath,$bfile);
                $img = Image::make($bpath.$bfile)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($bpath.$bfile);
                DB::update("UPDATE best_for_photo SET image = '".$bfile."' WHERE id = '".$_GET['edit']."'");
            }
            DB::update("UPDATE best_for_photo SET link = '$link', title = '$category', content = '$content' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Data Updated successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table('best_for_photo')->where('id','=',$_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Link has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Best For Category Master','best-for-cat');
        $data['bestforcats'] = DB::table('best_for_photo')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['bestforcat'] = DB::table('best_for_photo')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/best-for-photo')->with('data',$data);
    }
    public function photoreviews(){
        $notices = '';
        if(isset($_GET['approve']))
        {
            DB::update("UPDATE photo_reviews SET active = '1' WHERE id = ".$_GET['approve']);
            $notices = "<div class='alert alert-success'> Review has been approved !</div>";
        }
        $header = $this->header('Admin','Photo reviews');
        $reviews = DB::table('photo_reviews')->orderBy('id','DESC')->get();
        $footer = $this->footer();
        return view('admin/photo-reviews')->with(compact('header','notices','reviews','footer'));
    }
    public function layout_reviews(){
        $notices = '';
        if(isset($_GET['approve']))
        {
            DB::update("UPDATE layout_reviews SET active = '1' WHERE id = ".$_GET['approve']);
            $notices = "<div class='alert alert-success'> Review has been approved !</div>";
        }
        $header = $this->header('Admin','layout reviews');
        $reviews = DB::table('layout_reviews')->orderBy('id','DESC')->get();
        $footer = $this->footer();
        return view('admin/layout_reviews')->with(compact('header','notices','reviews','footer'));
    }
    public function amminities(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $name = escape($_POST['name']);
            DB::insert("INSERT INTO amminities (name) VALUE ('$name')");
            $data['notices'] .= "<div class='alert mini alert-success'> Amminities has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $name = escape($_POST['name']);
            DB::update("UPDATE amminities SET name = '$name' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Page edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table('amminities')->where('id','=',$_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Amminities has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Amminities','amminities');
        $data['amminities'] = DB::table('amminities')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['amminity'] = DB::table('amminities')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/amminities')->with('data',$data);
    }
    public function layoutFaq(){
        $data['notices'] = '';
        if(isset($_GET['edit'])){
            $data['faq'] = DB::table('layout_faq')->where('id','=',$_GET['edit'])->first();
        }
        if(isset($_POST['edit'])){
            $status = $_POST['status'];
            $answer = $_POST['answer'];
            DB::update("UPDATE layout_faq SET answer = '".$answer."', status = '".$status."' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> FAQ successfully updated !</div>";
        }
        $data['header'] = $this->header('FAQ(s) Master','faq');
        $data['footer'] = $this->footer();
        $data['faqs'] = DB::table('layout_faq')->orderBy('id','DESC')->get();
        return view('admin/layout_faq')->with('data',$data);
    }
    public function modifyPlan(){
        $data['notices'] = '';
        if(isset($_GET['edit'])){
            $data['plan'] = DB::table('modify_plan')->where('id','=',$_GET['edit'])->first();
        }
        if(isset($_POST['edit'])){
            $status = $_POST['status'];
            DB::update("UPDATE modify_plan SET status = '".$status."' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Status successfully updated !</div>";
        }
        $data['header'] = $this->header('Modify Plan','modify plan');
        $data['footer'] = $this->footer();
        $data['plans'] = DB::table('modify_plan')->orderBy('id','DESC')->get();
        return view('admin/modify_plan')->with('data',$data);
    }
    public function image_likes(){
        $notices = '';
        $header = $this->header('Admin','reviews');
        $reviews = DB::table('image_like')->orderBy('id','DESC')->get();
        $footer = $this->footer();
        return view('admin/image_likes')->with(compact('header','notices','reviews','footer'));
    }
    public function image_inspirations(){
        $notices = '';
        $header = $this->header('Admin','reviews');
        $reviews = DB::table('image_inspiration')->orderBy('id','DESC')->get();
        $footer = $this->footer();
        return view('admin/image_inspirations')->with(compact('header','notices','reviews','footer'));
    }
    public function plan_orders(){
        $data['notices'] = '';
        if(isset($_GET['edit'])){
            $data['plan'] = DB::table('layout_order')->where('id','=',$_GET['edit'])->first();
        }
        if(isset($_GET['details'])){
            $data['pland'] = DB::table('layout_order')->where('id','=',$_GET['details'])->first();
        }
        if(isset($_POST['edit'])){
            $status = $_POST['status'];
            DB::update("UPDATE layout_order SET status = '".$status."' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Status successfully updated !</div>";
        }
        $data['header'] = $this->header('Plan Orders','plan orders');
        $data['footer'] = $this->footer();
        $data['plans'] = DB::table('layout_order')->orderBy('id','DESC')->get();
        return view('admin/plan_orders')->with('data',$data);
    }
}
