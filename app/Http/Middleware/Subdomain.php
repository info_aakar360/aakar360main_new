<?php
namespace App\Http\Middleware;

use Closure;

use Exception;

class Subdomain {
    public function handle($request, Closure $next)
    {
        $route = $request->route();
        $domain = $route->parameter('domain');
        $tld = $route->parameter('tld');

        //do something with your params

        $route->forgetParameter('domain');
        $route->forgetParameter('tld');
        return $next($request);
    }
}
